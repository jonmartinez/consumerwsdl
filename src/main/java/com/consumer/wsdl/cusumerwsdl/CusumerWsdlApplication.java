package com.consumer.wsdl.cusumerwsdl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CusumerWsdlApplication {

    public static void main(String[] args) {
        SpringApplication.run(CusumerWsdlApplication.class, args);
    }

}
