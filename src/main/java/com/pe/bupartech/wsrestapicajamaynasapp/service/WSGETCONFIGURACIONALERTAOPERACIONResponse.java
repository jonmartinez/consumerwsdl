
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_CONFIGURACION_ALERTA_OPERACIONResult" type="{http://cajamaynas.pe/}ResponseAlertaOperacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetconfiguracionalertaoperacionResult"
})
@XmlRootElement(name = "WS_GET_CONFIGURACION_ALERTA_OPERACIONResponse")
public class WSGETCONFIGURACIONALERTAOPERACIONResponse {

    @XmlElement(name = "WS_GET_CONFIGURACION_ALERTA_OPERACIONResult")
    protected ResponseAlertaOperacion wsgetconfiguracionalertaoperacionResult;

    /**
     * Obtiene el valor de la propiedad wsgetconfiguracionalertaoperacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAlertaOperacion }
     *     
     */
    public ResponseAlertaOperacion getWSGETCONFIGURACIONALERTAOPERACIONResult() {
        return wsgetconfiguracionalertaoperacionResult;
    }

    /**
     * Define el valor de la propiedad wsgetconfiguracionalertaoperacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAlertaOperacion }
     *     
     */
    public void setWSGETCONFIGURACIONALERTAOPERACIONResult(ResponseAlertaOperacion value) {
        this.wsgetconfiguracionalertaoperacionResult = value;
    }

}
