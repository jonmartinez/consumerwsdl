
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_PAGO_PLANILLA_PROVEEDORES_VERIFICACIONResult" type="{http://cajamaynas.pe/}ResponsePlanillaDeposito" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wspagoplanillaproveedoresverificacionResult"
})
@XmlRootElement(name = "WS_PAGO_PLANILLA_PROVEEDORES_VERIFICACIONResponse")
public class WSPAGOPLANILLAPROVEEDORESVERIFICACIONResponse {

    @XmlElement(name = "WS_PAGO_PLANILLA_PROVEEDORES_VERIFICACIONResult")
    protected ResponsePlanillaDeposito wspagoplanillaproveedoresverificacionResult;

    /**
     * Obtiene el valor de la propiedad wspagoplanillaproveedoresverificacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePlanillaDeposito }
     *     
     */
    public ResponsePlanillaDeposito getWSPAGOPLANILLAPROVEEDORESVERIFICACIONResult() {
        return wspagoplanillaproveedoresverificacionResult;
    }

    /**
     * Define el valor de la propiedad wspagoplanillaproveedoresverificacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePlanillaDeposito }
     *     
     */
    public void setWSPAGOPLANILLAPROVEEDORESVERIFICACIONResult(ResponsePlanillaDeposito value) {
        this.wspagoplanillaproveedoresverificacionResult = value;
    }

}
