
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xn_tasa" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="xn_saldo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="xn_plazo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xnTasa",
    "xnSaldo",
    "xnPlazo"
})
@XmlRootElement(name = "WS_SIMULA_INTERES_GANADO")
public class WSSIMULAINTERESGANADO {

    @XmlElement(name = "xn_tasa")
    protected double xnTasa;
    @XmlElement(name = "xn_saldo")
    protected double xnSaldo;
    @XmlElement(name = "xn_plazo")
    protected int xnPlazo;

    /**
     * Obtiene el valor de la propiedad xnTasa.
     * 
     */
    public double getXnTasa() {
        return xnTasa;
    }

    /**
     * Define el valor de la propiedad xnTasa.
     * 
     */
    public void setXnTasa(double value) {
        this.xnTasa = value;
    }

    /**
     * Obtiene el valor de la propiedad xnSaldo.
     * 
     */
    public double getXnSaldo() {
        return xnSaldo;
    }

    /**
     * Define el valor de la propiedad xnSaldo.
     * 
     */
    public void setXnSaldo(double value) {
        this.xnSaldo = value;
    }

    /**
     * Obtiene el valor de la propiedad xnPlazo.
     * 
     */
    public int getXnPlazo() {
        return xnPlazo;
    }

    /**
     * Define el valor de la propiedad xnPlazo.
     * 
     */
    public void setXnPlazo(int value) {
        this.xnPlazo = value;
    }

}
