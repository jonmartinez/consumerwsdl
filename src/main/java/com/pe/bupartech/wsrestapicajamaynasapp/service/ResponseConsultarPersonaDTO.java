
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseConsultarPersonaDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseConsultarPersonaDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codpersona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_persona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="personeria" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tipo_doi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nro_doi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apellidos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre_completo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apellido_pat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apellido_mat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direc_ciudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direc_domicilio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celular1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celular2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_ing" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_ing_emp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dependencias" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="agencia_emp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fec_sist" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado_emp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fec_nacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado_civil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="salario_promedio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="profesion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cargo_emp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nick_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="razon_social" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_sociedad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="siglas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciiu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="peps" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="residente" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cDNI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cRUC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cCEX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="empleado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="habilitadoParaBanca" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="operador_celular" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="operador_celular2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="desc_resultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor_resultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseConsultarPersonaDTO", propOrder = {
    "codpersona",
    "tipoPersona",
    "personeria",
    "tipoDoi",
    "nroDoi",
    "nombres",
    "apellidos",
    "nombreCompleto",
    "apellidoPat",
    "apellidoMat",
    "tipoCliente",
    "direcCiudad",
    "direcDomicilio",
    "celular1",
    "celular2",
    "telefono1",
    "telefono2",
    "email",
    "fechaIng",
    "fechaIngEmp",
    "dependencias",
    "agenciaEmp",
    "fecSist",
    "estadoEmp",
    "sexo",
    "fecNacimiento",
    "estadoCivil",
    "salarioPromedio",
    "profesion",
    "cargoEmp",
    "nickName",
    "razonSocial",
    "tipoSociedad",
    "siglas",
    "sector",
    "ciiu",
    "peps",
    "nacionalidad",
    "residente",
    "cdni",
    "cruc",
    "ccex",
    "empleado",
    "habilitadoParaBanca",
    "operadorCelular",
    "operadorCelular2",
    "resultado",
    "descResultado",
    "valorResultado"
})
public class ResponseConsultarPersonaDTO {

    protected String codpersona;
    @XmlElement(name = "tipo_persona")
    protected String tipoPersona;
    protected int personeria;
    @XmlElement(name = "tipo_doi")
    protected String tipoDoi;
    @XmlElement(name = "nro_doi")
    protected String nroDoi;
    protected String nombres;
    protected String apellidos;
    @XmlElement(name = "nombre_completo")
    protected String nombreCompleto;
    @XmlElement(name = "apellido_pat")
    protected String apellidoPat;
    @XmlElement(name = "apellido_mat")
    protected String apellidoMat;
    @XmlElement(name = "tipo_cliente")
    protected String tipoCliente;
    @XmlElement(name = "direc_ciudad")
    protected String direcCiudad;
    @XmlElement(name = "direc_domicilio")
    protected String direcDomicilio;
    protected String celular1;
    protected String celular2;
    protected String telefono1;
    protected String telefono2;
    protected String email;
    @XmlElement(name = "fecha_ing")
    protected String fechaIng;
    @XmlElement(name = "fecha_ing_emp")
    protected String fechaIngEmp;
    protected int dependencias;
    @XmlElement(name = "agencia_emp")
    protected String agenciaEmp;
    @XmlElement(name = "fec_sist")
    protected String fecSist;
    @XmlElement(name = "estado_emp")
    protected String estadoEmp;
    protected String sexo;
    @XmlElement(name = "fec_nacimiento")
    protected String fecNacimiento;
    @XmlElement(name = "estado_civil")
    protected String estadoCivil;
    @XmlElement(name = "salario_promedio", required = true)
    protected BigDecimal salarioPromedio;
    protected String profesion;
    @XmlElement(name = "cargo_emp")
    protected String cargoEmp;
    @XmlElement(name = "nick_name")
    protected String nickName;
    @XmlElement(name = "razon_social")
    protected String razonSocial;
    @XmlElement(name = "tipo_sociedad")
    protected String tipoSociedad;
    protected String siglas;
    protected String sector;
    protected String ciiu;
    protected int peps;
    protected String nacionalidad;
    protected int residente;
    @XmlElement(name = "cDNI")
    protected String cdni;
    @XmlElement(name = "cRUC")
    protected String cruc;
    @XmlElement(name = "cCEX")
    protected String ccex;
    protected int empleado;
    protected boolean habilitadoParaBanca;
    @XmlElement(name = "operador_celular")
    protected int operadorCelular;
    @XmlElement(name = "operador_celular2")
    protected int operadorCelular2;
    protected int resultado;
    @XmlElement(name = "desc_resultado")
    protected String descResultado;
    @XmlElement(name = "valor_resultado")
    protected String valorResultado;

    /**
     * Obtiene el valor de la propiedad codpersona.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodpersona() {
        return codpersona;
    }

    /**
     * Define el valor de la propiedad codpersona.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodpersona(String value) {
        this.codpersona = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPersona.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Define el valor de la propiedad tipoPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPersona(String value) {
        this.tipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad personeria.
     * 
     */
    public int getPersoneria() {
        return personeria;
    }

    /**
     * Define el valor de la propiedad personeria.
     * 
     */
    public void setPersoneria(int value) {
        this.personeria = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDoi() {
        return tipoDoi;
    }

    /**
     * Define el valor de la propiedad tipoDoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDoi(String value) {
        this.tipoDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad nroDoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroDoi() {
        return nroDoi;
    }

    /**
     * Define el valor de la propiedad nroDoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroDoi(String value) {
        this.nroDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad nombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * Define el valor de la propiedad nombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombres(String value) {
        this.nombres = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Define el valor de la propiedad apellidos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidos(String value) {
        this.apellidos = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCompleto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCompleto() {
        return nombreCompleto;
    }

    /**
     * Define el valor de la propiedad nombreCompleto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCompleto(String value) {
        this.nombreCompleto = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoPat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoPat() {
        return apellidoPat;
    }

    /**
     * Define el valor de la propiedad apellidoPat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoPat(String value) {
        this.apellidoPat = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoMat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoMat() {
        return apellidoMat;
    }

    /**
     * Define el valor de la propiedad apellidoMat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoMat(String value) {
        this.apellidoMat = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCliente() {
        return tipoCliente;
    }

    /**
     * Define el valor de la propiedad tipoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCliente(String value) {
        this.tipoCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad direcCiudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirecCiudad() {
        return direcCiudad;
    }

    /**
     * Define el valor de la propiedad direcCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirecCiudad(String value) {
        this.direcCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad direcDomicilio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirecDomicilio() {
        return direcDomicilio;
    }

    /**
     * Define el valor de la propiedad direcDomicilio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirecDomicilio(String value) {
        this.direcDomicilio = value;
    }

    /**
     * Obtiene el valor de la propiedad celular1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelular1() {
        return celular1;
    }

    /**
     * Define el valor de la propiedad celular1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelular1(String value) {
        this.celular1 = value;
    }

    /**
     * Obtiene el valor de la propiedad celular2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelular2() {
        return celular2;
    }

    /**
     * Define el valor de la propiedad celular2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelular2(String value) {
        this.celular2 = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono1() {
        return telefono1;
    }

    /**
     * Define el valor de la propiedad telefono1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono1(String value) {
        this.telefono1 = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono2() {
        return telefono2;
    }

    /**
     * Define el valor de la propiedad telefono2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono2(String value) {
        this.telefono2 = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaIng.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaIng() {
        return fechaIng;
    }

    /**
     * Define el valor de la propiedad fechaIng.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaIng(String value) {
        this.fechaIng = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaIngEmp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaIngEmp() {
        return fechaIngEmp;
    }

    /**
     * Define el valor de la propiedad fechaIngEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaIngEmp(String value) {
        this.fechaIngEmp = value;
    }

    /**
     * Obtiene el valor de la propiedad dependencias.
     * 
     */
    public int getDependencias() {
        return dependencias;
    }

    /**
     * Define el valor de la propiedad dependencias.
     * 
     */
    public void setDependencias(int value) {
        this.dependencias = value;
    }

    /**
     * Obtiene el valor de la propiedad agenciaEmp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenciaEmp() {
        return agenciaEmp;
    }

    /**
     * Define el valor de la propiedad agenciaEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenciaEmp(String value) {
        this.agenciaEmp = value;
    }

    /**
     * Obtiene el valor de la propiedad fecSist.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecSist() {
        return fecSist;
    }

    /**
     * Define el valor de la propiedad fecSist.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecSist(String value) {
        this.fecSist = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoEmp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoEmp() {
        return estadoEmp;
    }

    /**
     * Define el valor de la propiedad estadoEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoEmp(String value) {
        this.estadoEmp = value;
    }

    /**
     * Obtiene el valor de la propiedad sexo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * Define el valor de la propiedad sexo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSexo(String value) {
        this.sexo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecNacimiento() {
        return fecNacimiento;
    }

    /**
     * Define el valor de la propiedad fecNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecNacimiento(String value) {
        this.fecNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Define el valor de la propiedad estadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoCivil(String value) {
        this.estadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad salarioPromedio.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSalarioPromedio() {
        return salarioPromedio;
    }

    /**
     * Define el valor de la propiedad salarioPromedio.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSalarioPromedio(BigDecimal value) {
        this.salarioPromedio = value;
    }

    /**
     * Obtiene el valor de la propiedad profesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfesion() {
        return profesion;
    }

    /**
     * Define el valor de la propiedad profesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfesion(String value) {
        this.profesion = value;
    }

    /**
     * Obtiene el valor de la propiedad cargoEmp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargoEmp() {
        return cargoEmp;
    }

    /**
     * Define el valor de la propiedad cargoEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargoEmp(String value) {
        this.cargoEmp = value;
    }

    /**
     * Obtiene el valor de la propiedad nickName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * Define el valor de la propiedad nickName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickName(String value) {
        this.nickName = value;
    }

    /**
     * Obtiene el valor de la propiedad razonSocial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * Define el valor de la propiedad razonSocial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazonSocial(String value) {
        this.razonSocial = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoSociedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSociedad() {
        return tipoSociedad;
    }

    /**
     * Define el valor de la propiedad tipoSociedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSociedad(String value) {
        this.tipoSociedad = value;
    }

    /**
     * Obtiene el valor de la propiedad siglas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiglas() {
        return siglas;
    }

    /**
     * Define el valor de la propiedad siglas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiglas(String value) {
        this.siglas = value;
    }

    /**
     * Obtiene el valor de la propiedad sector.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSector() {
        return sector;
    }

    /**
     * Define el valor de la propiedad sector.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSector(String value) {
        this.sector = value;
    }

    /**
     * Obtiene el valor de la propiedad ciiu.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiiu() {
        return ciiu;
    }

    /**
     * Define el valor de la propiedad ciiu.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiiu(String value) {
        this.ciiu = value;
    }

    /**
     * Obtiene el valor de la propiedad peps.
     * 
     */
    public int getPeps() {
        return peps;
    }

    /**
     * Define el valor de la propiedad peps.
     * 
     */
    public void setPeps(int value) {
        this.peps = value;
    }

    /**
     * Obtiene el valor de la propiedad nacionalidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     * Define el valor de la propiedad nacionalidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNacionalidad(String value) {
        this.nacionalidad = value;
    }

    /**
     * Obtiene el valor de la propiedad residente.
     * 
     */
    public int getResidente() {
        return residente;
    }

    /**
     * Define el valor de la propiedad residente.
     * 
     */
    public void setResidente(int value) {
        this.residente = value;
    }

    /**
     * Obtiene el valor de la propiedad cdni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDNI() {
        return cdni;
    }

    /**
     * Define el valor de la propiedad cdni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDNI(String value) {
        this.cdni = value;
    }

    /**
     * Obtiene el valor de la propiedad cruc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRUC() {
        return cruc;
    }

    /**
     * Define el valor de la propiedad cruc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRUC(String value) {
        this.cruc = value;
    }

    /**
     * Obtiene el valor de la propiedad ccex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCEX() {
        return ccex;
    }

    /**
     * Define el valor de la propiedad ccex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCEX(String value) {
        this.ccex = value;
    }

    /**
     * Obtiene el valor de la propiedad empleado.
     * 
     */
    public int getEmpleado() {
        return empleado;
    }

    /**
     * Define el valor de la propiedad empleado.
     * 
     */
    public void setEmpleado(int value) {
        this.empleado = value;
    }

    /**
     * Obtiene el valor de la propiedad habilitadoParaBanca.
     * 
     */
    public boolean isHabilitadoParaBanca() {
        return habilitadoParaBanca;
    }

    /**
     * Define el valor de la propiedad habilitadoParaBanca.
     * 
     */
    public void setHabilitadoParaBanca(boolean value) {
        this.habilitadoParaBanca = value;
    }

    /**
     * Obtiene el valor de la propiedad operadorCelular.
     * 
     */
    public int getOperadorCelular() {
        return operadorCelular;
    }

    /**
     * Define el valor de la propiedad operadorCelular.
     * 
     */
    public void setOperadorCelular(int value) {
        this.operadorCelular = value;
    }

    /**
     * Obtiene el valor de la propiedad operadorCelular2.
     * 
     */
    public int getOperadorCelular2() {
        return operadorCelular2;
    }

    /**
     * Define el valor de la propiedad operadorCelular2.
     * 
     */
    public void setOperadorCelular2(int value) {
        this.operadorCelular2 = value;
    }

    /**
     * Obtiene el valor de la propiedad resultado.
     * 
     */
    public int getResultado() {
        return resultado;
    }

    /**
     * Define el valor de la propiedad resultado.
     * 
     */
    public void setResultado(int value) {
        this.resultado = value;
    }

    /**
     * Obtiene el valor de la propiedad descResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResultado() {
        return descResultado;
    }

    /**
     * Define el valor de la propiedad descResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResultado(String value) {
        this.descResultado = value;
    }

    /**
     * Obtiene el valor de la propiedad valorResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorResultado() {
        return valorResultado;
    }

    /**
     * Define el valor de la propiedad valorResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorResultado(String value) {
        this.valorResultado = value;
    }

}
