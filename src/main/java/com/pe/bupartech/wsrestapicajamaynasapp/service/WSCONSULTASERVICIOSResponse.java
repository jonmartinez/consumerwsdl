
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTA_SERVICIOSResult" type="{http://cajamaynas.pe/}ArrayOfResponseServiciosDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultaserviciosResult"
})
@XmlRootElement(name = "WS_CONSULTA_SERVICIOSResponse")
public class WSCONSULTASERVICIOSResponse {

    @XmlElement(name = "WS_CONSULTA_SERVICIOSResult")
    protected ArrayOfResponseServiciosDTO wsconsultaserviciosResult;

    /**
     * Obtiene el valor de la propiedad wsconsultaserviciosResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResponseServiciosDTO }
     *     
     */
    public ArrayOfResponseServiciosDTO getWSCONSULTASERVICIOSResult() {
        return wsconsultaserviciosResult;
    }

    /**
     * Define el valor de la propiedad wsconsultaserviciosResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResponseServiciosDTO }
     *     
     */
    public void setWSCONSULTASERVICIOSResult(ArrayOfResponseServiciosDTO value) {
        this.wsconsultaserviciosResult = value;
    }

}
