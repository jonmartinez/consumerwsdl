
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestLimitesOperacionDet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestLimitesOperacionDet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nMoneda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nMontoMinXOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nMontoMaxXOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nMontoMaxAcuXDia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nSubProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cSubProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestLimitesOperacionDet", propOrder = {
    "nMoneda",
    "nMontoMinXOperacion",
    "nMontoMaxXOperacion",
    "nMontoMaxAcuXDia",
    "nProducto",
    "nSubProducto",
    "cProducto",
    "cSubProducto"
})
public class RequestLimitesOperacionDet {

    protected int nMoneda;
    protected String nMontoMinXOperacion;
    protected String nMontoMaxXOperacion;
    protected String nMontoMaxAcuXDia;
    protected int nProducto;
    protected int nSubProducto;
    protected String cProducto;
    protected String cSubProducto;

    /**
     * Obtiene el valor de la propiedad nMoneda.
     * 
     */
    public int getNMoneda() {
        return nMoneda;
    }

    /**
     * Define el valor de la propiedad nMoneda.
     * 
     */
    public void setNMoneda(int value) {
        this.nMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad nMontoMinXOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNMontoMinXOperacion() {
        return nMontoMinXOperacion;
    }

    /**
     * Define el valor de la propiedad nMontoMinXOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNMontoMinXOperacion(String value) {
        this.nMontoMinXOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nMontoMaxXOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNMontoMaxXOperacion() {
        return nMontoMaxXOperacion;
    }

    /**
     * Define el valor de la propiedad nMontoMaxXOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNMontoMaxXOperacion(String value) {
        this.nMontoMaxXOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nMontoMaxAcuXDia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNMontoMaxAcuXDia() {
        return nMontoMaxAcuXDia;
    }

    /**
     * Define el valor de la propiedad nMontoMaxAcuXDia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNMontoMaxAcuXDia(String value) {
        this.nMontoMaxAcuXDia = value;
    }

    /**
     * Obtiene el valor de la propiedad nProducto.
     * 
     */
    public int getNProducto() {
        return nProducto;
    }

    /**
     * Define el valor de la propiedad nProducto.
     * 
     */
    public void setNProducto(int value) {
        this.nProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad nSubProducto.
     * 
     */
    public int getNSubProducto() {
        return nSubProducto;
    }

    /**
     * Define el valor de la propiedad nSubProducto.
     * 
     */
    public void setNSubProducto(int value) {
        this.nSubProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad cProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCProducto() {
        return cProducto;
    }

    /**
     * Define el valor de la propiedad cProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCProducto(String value) {
        this.cProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad cSubProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCSubProducto() {
        return cSubProducto;
    }

    /**
     * Define el valor de la propiedad cSubProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCSubProducto(String value) {
        this.cSubProducto = value;
    }

}
