
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfRequestPlanillaDepositoDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRequestPlanillaDepositoDetalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestPlanillaDepositoDetalle" type="{http://cajamaynas.pe/}RequestPlanillaDepositoDetalle" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRequestPlanillaDepositoDetalle", propOrder = {
    "requestPlanillaDepositoDetalle"
})
public class ArrayOfRequestPlanillaDepositoDetalle {

    @XmlElement(name = "RequestPlanillaDepositoDetalle", nillable = true)
    protected List<RequestPlanillaDepositoDetalle> requestPlanillaDepositoDetalle;

    /**
     * Gets the value of the requestPlanillaDepositoDetalle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestPlanillaDepositoDetalle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestPlanillaDepositoDetalle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestPlanillaDepositoDetalle }
     * 
     * 
     */
    public List<RequestPlanillaDepositoDetalle> getRequestPlanillaDepositoDetalle() {
        if (requestPlanillaDepositoDetalle == null) {
            requestPlanillaDepositoDetalle = new ArrayList<RequestPlanillaDepositoDetalle>();
        }
        return this.requestPlanillaDepositoDetalle;
    }

}
