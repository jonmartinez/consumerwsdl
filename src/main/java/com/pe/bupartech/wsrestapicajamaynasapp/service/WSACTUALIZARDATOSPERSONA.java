
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_telef_fijo_casa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_telef_fijo_trabajo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_movil1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_movil2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_operador1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_operador2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcIdentificacion",
    "xcDireccion",
    "xcEmail",
    "xcTelefFijoCasa",
    "xcTelefFijoTrabajo",
    "xcMovil1",
    "xcMovil2",
    "xnOperador1",
    "xnOperador2"
})
@XmlRootElement(name = "WS_ACTUALIZAR_DATOS_PERSONA")
public class WSACTUALIZARDATOSPERSONA {

    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;
    @XmlElement(name = "xc_direccion")
    protected String xcDireccion;
    @XmlElement(name = "xc_email")
    protected String xcEmail;
    @XmlElement(name = "xc_telef_fijo_casa")
    protected String xcTelefFijoCasa;
    @XmlElement(name = "xc_telef_fijo_trabajo")
    protected String xcTelefFijoTrabajo;
    @XmlElement(name = "xc_movil1")
    protected String xcMovil1;
    @XmlElement(name = "xc_movil2")
    protected String xcMovil2;
    @XmlElement(name = "xn_operador1")
    protected int xnOperador1;
    @XmlElement(name = "xn_operador2")
    protected int xnOperador2;

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad xcDireccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcDireccion() {
        return xcDireccion;
    }

    /**
     * Define el valor de la propiedad xcDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcDireccion(String value) {
        this.xcDireccion = value;
    }

    /**
     * Obtiene el valor de la propiedad xcEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcEmail() {
        return xcEmail;
    }

    /**
     * Define el valor de la propiedad xcEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcEmail(String value) {
        this.xcEmail = value;
    }

    /**
     * Obtiene el valor de la propiedad xcTelefFijoCasa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcTelefFijoCasa() {
        return xcTelefFijoCasa;
    }

    /**
     * Define el valor de la propiedad xcTelefFijoCasa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcTelefFijoCasa(String value) {
        this.xcTelefFijoCasa = value;
    }

    /**
     * Obtiene el valor de la propiedad xcTelefFijoTrabajo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcTelefFijoTrabajo() {
        return xcTelefFijoTrabajo;
    }

    /**
     * Define el valor de la propiedad xcTelefFijoTrabajo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcTelefFijoTrabajo(String value) {
        this.xcTelefFijoTrabajo = value;
    }

    /**
     * Obtiene el valor de la propiedad xcMovil1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcMovil1() {
        return xcMovil1;
    }

    /**
     * Define el valor de la propiedad xcMovil1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcMovil1(String value) {
        this.xcMovil1 = value;
    }

    /**
     * Obtiene el valor de la propiedad xcMovil2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcMovil2() {
        return xcMovil2;
    }

    /**
     * Define el valor de la propiedad xcMovil2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcMovil2(String value) {
        this.xcMovil2 = value;
    }

    /**
     * Obtiene el valor de la propiedad xnOperador1.
     * 
     */
    public int getXnOperador1() {
        return xnOperador1;
    }

    /**
     * Define el valor de la propiedad xnOperador1.
     * 
     */
    public void setXnOperador1(int value) {
        this.xnOperador1 = value;
    }

    /**
     * Obtiene el valor de la propiedad xnOperador2.
     * 
     */
    public int getXnOperador2() {
        return xnOperador2;
    }

    /**
     * Define el valor de la propiedad xnOperador2.
     * 
     */
    public void setXnOperador2(int value) {
        this.xnOperador2 = value;
    }

}
