
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cuenta_ahorro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_monto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_TpoCanal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCuenta",
    "xcCuentaAhorro",
    "xnMonto",
    "xcIdentificacion",
    "xnTpoCanal"
})
@XmlRootElement(name = "WS_RENOVAR_PIGNORATICIO")
public class WSRENOVARPIGNORATICIO {

    @XmlElement(name = "xc_cuenta")
    protected String xcCuenta;
    @XmlElement(name = "xc_cuenta_ahorro")
    protected String xcCuentaAhorro;
    @XmlElement(name = "xn_monto", required = true)
    protected BigDecimal xnMonto;
    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;
    @XmlElement(name = "xn_TpoCanal")
    protected int xnTpoCanal;

    /**
     * Obtiene el valor de la propiedad xcCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuenta() {
        return xcCuenta;
    }

    /**
     * Define el valor de la propiedad xcCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuenta(String value) {
        this.xcCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCuentaAhorro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuentaAhorro() {
        return xcCuentaAhorro;
    }

    /**
     * Define el valor de la propiedad xcCuentaAhorro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuentaAhorro(String value) {
        this.xcCuentaAhorro = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMonto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnMonto() {
        return xnMonto;
    }

    /**
     * Define el valor de la propiedad xnMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnMonto(BigDecimal value) {
        this.xnMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTpoCanal.
     * 
     */
    public int getXnTpoCanal() {
        return xnTpoCanal;
    }

    /**
     * Define el valor de la propiedad xnTpoCanal.
     * 
     */
    public void setXnTpoCanal(int value) {
        this.xnTpoCanal = value;
    }

}
