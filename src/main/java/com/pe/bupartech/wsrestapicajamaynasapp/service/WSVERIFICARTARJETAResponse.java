
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_VERIFICAR_TARJETAResult" type="{http://cajamaynas.pe/}ResponseResulOperacionDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsverificartarjetaResult"
})
@XmlRootElement(name = "WS_VERIFICAR_TARJETAResponse")
public class WSVERIFICARTARJETAResponse {

    @XmlElement(name = "WS_VERIFICAR_TARJETAResult")
    protected ResponseResulOperacionDTO wsverificartarjetaResult;

    /**
     * Obtiene el valor de la propiedad wsverificartarjetaResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public ResponseResulOperacionDTO getWSVERIFICARTARJETAResult() {
        return wsverificartarjetaResult;
    }

    /**
     * Define el valor de la propiedad wsverificartarjetaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public void setWSVERIFICARTARJETAResult(ResponseResulOperacionDTO value) {
        this.wsverificartarjetaResult = value;
    }

}
