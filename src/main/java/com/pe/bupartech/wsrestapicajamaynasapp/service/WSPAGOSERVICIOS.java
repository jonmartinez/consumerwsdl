
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_montocobro" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xn_numref" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_codcliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_codconvenio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_numRecibo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcNumeroCuenta",
    "xnMontocobro",
    "xnNumref",
    "xcCodcliente",
    "xcCodconvenio",
    "xcNumRecibo",
    "xcIdentificacion"
})
@XmlRootElement(name = "WS_PAGO_SERVICIOS")
public class WSPAGOSERVICIOS {

    @XmlElement(name = "xc_numeroCuenta")
    protected String xcNumeroCuenta;
    @XmlElement(name = "xn_montocobro", required = true)
    protected BigDecimal xnMontocobro;
    @XmlElement(name = "xn_numref")
    protected int xnNumref;
    @XmlElement(name = "xc_codcliente")
    protected String xcCodcliente;
    @XmlElement(name = "xc_codconvenio")
    protected String xcCodconvenio;
    @XmlElement(name = "xc_numRecibo")
    protected String xcNumRecibo;
    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;

    /**
     * Obtiene el valor de la propiedad xcNumeroCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNumeroCuenta() {
        return xcNumeroCuenta;
    }

    /**
     * Define el valor de la propiedad xcNumeroCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNumeroCuenta(String value) {
        this.xcNumeroCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMontocobro.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnMontocobro() {
        return xnMontocobro;
    }

    /**
     * Define el valor de la propiedad xnMontocobro.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnMontocobro(BigDecimal value) {
        this.xnMontocobro = value;
    }

    /**
     * Obtiene el valor de la propiedad xnNumref.
     * 
     */
    public int getXnNumref() {
        return xnNumref;
    }

    /**
     * Define el valor de la propiedad xnNumref.
     * 
     */
    public void setXnNumref(int value) {
        this.xnNumref = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCodcliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCodcliente() {
        return xcCodcliente;
    }

    /**
     * Define el valor de la propiedad xcCodcliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCodcliente(String value) {
        this.xcCodcliente = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCodconvenio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCodconvenio() {
        return xcCodconvenio;
    }

    /**
     * Define el valor de la propiedad xcCodconvenio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCodconvenio(String value) {
        this.xcCodconvenio = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNumRecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNumRecibo() {
        return xcNumRecibo;
    }

    /**
     * Define el valor de la propiedad xcNumRecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNumRecibo(String value) {
        this.xcNumRecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

}
