
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_RENOVAR_PIGNORATICIOResult" type="{http://cajamaynas.pe/}ResponseResulOperacionDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsrenovarpignoraticioResult"
})
@XmlRootElement(name = "WS_RENOVAR_PIGNORATICIOResponse")
public class WSRENOVARPIGNORATICIOResponse {

    @XmlElement(name = "WS_RENOVAR_PIGNORATICIOResult")
    protected ResponseResulOperacionDTO wsrenovarpignoraticioResult;

    /**
     * Obtiene el valor de la propiedad wsrenovarpignoraticioResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public ResponseResulOperacionDTO getWSRENOVARPIGNORATICIOResult() {
        return wsrenovarpignoraticioResult;
    }

    /**
     * Define el valor de la propiedad wsrenovarpignoraticioResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public void setWSRENOVARPIGNORATICIOResult(ResponseResulOperacionDTO value) {
        this.wsrenovarpignoraticioResult = value;
    }

}
