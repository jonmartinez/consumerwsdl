
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xn_plazo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_monto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="xn_subproducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_personeria" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xnPlazo",
    "xnMonto",
    "xnSubproducto",
    "xnPersoneria"
})
@XmlRootElement(name = "WS_OBTIENE_TASA_PF")
public class WSOBTIENETASAPF {

    @XmlElement(name = "xn_plazo")
    protected int xnPlazo;
    @XmlElement(name = "xn_monto")
    protected double xnMonto;
    @XmlElement(name = "xn_subproducto")
    protected int xnSubproducto;
    @XmlElement(name = "xn_personeria")
    protected int xnPersoneria;

    /**
     * Obtiene el valor de la propiedad xnPlazo.
     * 
     */
    public int getXnPlazo() {
        return xnPlazo;
    }

    /**
     * Define el valor de la propiedad xnPlazo.
     * 
     */
    public void setXnPlazo(int value) {
        this.xnPlazo = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMonto.
     * 
     */
    public double getXnMonto() {
        return xnMonto;
    }

    /**
     * Define el valor de la propiedad xnMonto.
     * 
     */
    public void setXnMonto(double value) {
        this.xnMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad xnSubproducto.
     * 
     */
    public int getXnSubproducto() {
        return xnSubproducto;
    }

    /**
     * Define el valor de la propiedad xnSubproducto.
     * 
     */
    public void setXnSubproducto(int value) {
        this.xnSubproducto = value;
    }

    /**
     * Obtiene el valor de la propiedad xnPersoneria.
     * 
     */
    public int getXnPersoneria() {
        return xnPersoneria;
    }

    /**
     * Define el valor de la propiedad xnPersoneria.
     * 
     */
    public void setXnPersoneria(int value) {
        this.xnPersoneria = value;
    }

}
