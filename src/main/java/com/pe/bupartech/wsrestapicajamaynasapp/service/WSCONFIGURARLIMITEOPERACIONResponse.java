
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONFIGURAR_LIMITE_OPERACIONResult" type="{http://cajamaynas.pe/}ResponseResulOperacionDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconfigurarlimiteoperacionResult"
})
@XmlRootElement(name = "WS_CONFIGURAR_LIMITE_OPERACIONResponse")
public class WSCONFIGURARLIMITEOPERACIONResponse {

    @XmlElement(name = "WS_CONFIGURAR_LIMITE_OPERACIONResult")
    protected ResponseResulOperacionDTO wsconfigurarlimiteoperacionResult;

    /**
     * Obtiene el valor de la propiedad wsconfigurarlimiteoperacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public ResponseResulOperacionDTO getWSCONFIGURARLIMITEOPERACIONResult() {
        return wsconfigurarlimiteoperacionResult;
    }

    /**
     * Define el valor de la propiedad wsconfigurarlimiteoperacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public void setWSCONFIGURARLIMITEOPERACIONResult(ResponseResulOperacionDTO value) {
        this.wsconfigurarlimiteoperacionResult = value;
    }

}
