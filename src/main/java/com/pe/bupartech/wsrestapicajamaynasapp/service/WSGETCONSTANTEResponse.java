
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_CONSTANTEResult" type="{http://cajamaynas.pe/}ArrayOfResponseConstanteDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetconstanteResult"
})
@XmlRootElement(name = "WS_GET_CONSTANTEResponse")
public class WSGETCONSTANTEResponse {

    @XmlElement(name = "WS_GET_CONSTANTEResult")
    protected ArrayOfResponseConstanteDTO wsgetconstanteResult;

    /**
     * Obtiene el valor de la propiedad wsgetconstanteResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResponseConstanteDTO }
     *     
     */
    public ArrayOfResponseConstanteDTO getWSGETCONSTANTEResult() {
        return wsgetconstanteResult;
    }

    /**
     * Define el valor de la propiedad wsgetconstanteResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResponseConstanteDTO }
     *     
     */
    public void setWSGETCONSTANTEResult(ArrayOfResponseConstanteDTO value) {
        this.wsgetconstanteResult = value;
    }

}
