
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_MISCUENTASResult" type="{http://cajamaynas.pe/}ResponseMisCuentas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultarmiscuentasResult"
})
@XmlRootElement(name = "WS_CONSULTAR_MISCUENTASResponse")
public class WSCONSULTARMISCUENTASResponse {

    @XmlElement(name = "WS_CONSULTAR_MISCUENTASResult")
    protected ResponseMisCuentas wsconsultarmiscuentasResult;

    /**
     * Obtiene el valor de la propiedad wsconsultarmiscuentasResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMisCuentas }
     *     
     */
    public ResponseMisCuentas getWSCONSULTARMISCUENTASResult() {
        return wsconsultarmiscuentasResult;
    }

    /**
     * Define el valor de la propiedad wsconsultarmiscuentasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMisCuentas }
     *     
     */
    public void setWSCONSULTARMISCUENTASResult(ResponseMisCuentas value) {
        this.wsconsultarmiscuentasResult = value;
    }

}
