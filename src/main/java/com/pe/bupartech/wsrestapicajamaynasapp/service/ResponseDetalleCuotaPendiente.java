
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseDetalleCuotaPendiente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseDetalleCuotaPendiente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nro_cuota" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fecha_venc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto_cuota" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="capital" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gastos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_cap" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dias_atraso" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cuotas_pendientes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombres_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigo_consulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseDetalleCuotaPendiente", propOrder = {
    "cuenta",
    "nroCuota",
    "fechaVenc",
    "montoCuota",
    "capital",
    "interes",
    "mora",
    "gastos",
    "saldoCap",
    "diasAtraso",
    "cuotasPendientes",
    "nombresCliente",
    "codigoConsulta"
})
public class ResponseDetalleCuotaPendiente {

    protected String cuenta;
    @XmlElement(name = "nro_cuota")
    protected int nroCuota;
    @XmlElement(name = "fecha_venc")
    protected String fechaVenc;
    @XmlElement(name = "monto_cuota")
    protected String montoCuota;
    protected String capital;
    protected String interes;
    protected String mora;
    protected String gastos;
    @XmlElement(name = "saldo_cap")
    protected String saldoCap;
    @XmlElement(name = "dias_atraso")
    protected int diasAtraso;
    @XmlElement(name = "cuotas_pendientes")
    protected String cuotasPendientes;
    @XmlElement(name = "nombres_cliente")
    protected String nombresCliente;
    @XmlElement(name = "codigo_consulta")
    protected String codigoConsulta;

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCuota.
     * 
     */
    public int getNroCuota() {
        return nroCuota;
    }

    /**
     * Define el valor de la propiedad nroCuota.
     * 
     */
    public void setNroCuota(int value) {
        this.nroCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVenc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVenc() {
        return fechaVenc;
    }

    /**
     * Define el valor de la propiedad fechaVenc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVenc(String value) {
        this.fechaVenc = value;
    }

    /**
     * Obtiene el valor de la propiedad montoCuota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoCuota() {
        return montoCuota;
    }

    /**
     * Define el valor de la propiedad montoCuota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoCuota(String value) {
        this.montoCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad capital.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapital() {
        return capital;
    }

    /**
     * Define el valor de la propiedad capital.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapital(String value) {
        this.capital = value;
    }

    /**
     * Obtiene el valor de la propiedad interes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteres() {
        return interes;
    }

    /**
     * Define el valor de la propiedad interes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteres(String value) {
        this.interes = value;
    }

    /**
     * Obtiene el valor de la propiedad mora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMora() {
        return mora;
    }

    /**
     * Define el valor de la propiedad mora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMora(String value) {
        this.mora = value;
    }

    /**
     * Obtiene el valor de la propiedad gastos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastos() {
        return gastos;
    }

    /**
     * Define el valor de la propiedad gastos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastos(String value) {
        this.gastos = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoCap.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoCap() {
        return saldoCap;
    }

    /**
     * Define el valor de la propiedad saldoCap.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoCap(String value) {
        this.saldoCap = value;
    }

    /**
     * Obtiene el valor de la propiedad diasAtraso.
     * 
     */
    public int getDiasAtraso() {
        return diasAtraso;
    }

    /**
     * Define el valor de la propiedad diasAtraso.
     * 
     */
    public void setDiasAtraso(int value) {
        this.diasAtraso = value;
    }

    /**
     * Obtiene el valor de la propiedad cuotasPendientes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuotasPendientes() {
        return cuotasPendientes;
    }

    /**
     * Define el valor de la propiedad cuotasPendientes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuotasPendientes(String value) {
        this.cuotasPendientes = value;
    }

    /**
     * Obtiene el valor de la propiedad nombresCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombresCliente() {
        return nombresCliente;
    }

    /**
     * Define el valor de la propiedad nombresCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombresCliente(String value) {
        this.nombresCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConsulta() {
        return codigoConsulta;
    }

    /**
     * Define el valor de la propiedad codigoConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConsulta(String value) {
        this.codigoConsulta = value;
    }

}
