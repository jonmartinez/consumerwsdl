
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCodigo"
})
@XmlRootElement(name = "WS_GET_CIFRADO2")
public class WSGETCIFRADO2 {

    @XmlElement(name = "xc_codigo")
    protected String xcCodigo;

    /**
     * Obtiene el valor de la propiedad xcCodigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCodigo() {
        return xcCodigo;
    }

    /**
     * Define el valor de la propiedad xcCodigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCodigo(String value) {
        this.xcCodigo = value;
    }

}
