
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_RESPTSSEGURIDADALEResult" type="{http://cajamaynas.pe/}ArrayOfResponseGetRespuestaSeguridadDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultarresptsseguridadaleResult"
})
@XmlRootElement(name = "WS_CONSULTAR_RESPTSSEGURIDADALEResponse")
public class WSCONSULTARRESPTSSEGURIDADALEResponse {

    @XmlElement(name = "WS_CONSULTAR_RESPTSSEGURIDADALEResult")
    protected ArrayOfResponseGetRespuestaSeguridadDTO wsconsultarresptsseguridadaleResult;

    /**
     * Obtiene el valor de la propiedad wsconsultarresptsseguridadaleResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResponseGetRespuestaSeguridadDTO }
     *     
     */
    public ArrayOfResponseGetRespuestaSeguridadDTO getWSCONSULTARRESPTSSEGURIDADALEResult() {
        return wsconsultarresptsseguridadaleResult;
    }

    /**
     * Define el valor de la propiedad wsconsultarresptsseguridadaleResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResponseGetRespuestaSeguridadDTO }
     *     
     */
    public void setWSCONSULTARRESPTSSEGURIDADALEResult(ArrayOfResponseGetRespuestaSeguridadDTO value) {
        this.wsconsultarresptsseguridadaleResult = value;
    }

}
