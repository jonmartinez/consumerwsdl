
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_PERSONAResult" type="{http://cajamaynas.pe/}ResponseConsultarPersonaDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultarpersonaResult"
})
@XmlRootElement(name = "WS_CONSULTAR_PERSONAResponse")
public class WSCONSULTARPERSONAResponse {

    @XmlElement(name = "WS_CONSULTAR_PERSONAResult")
    protected ResponseConsultarPersonaDTO wsconsultarpersonaResult;

    /**
     * Obtiene el valor de la propiedad wsconsultarpersonaResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseConsultarPersonaDTO }
     *     
     */
    public ResponseConsultarPersonaDTO getWSCONSULTARPERSONAResult() {
        return wsconsultarpersonaResult;
    }

    /**
     * Define el valor de la propiedad wsconsultarpersonaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseConsultarPersonaDTO }
     *     
     */
    public void setWSCONSULTARPERSONAResult(ResponseConsultarPersonaDTO value) {
        this.wsconsultarpersonaResult = value;
    }

}
