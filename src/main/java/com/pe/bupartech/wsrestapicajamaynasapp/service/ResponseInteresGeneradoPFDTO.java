
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseInteresGeneradoPFDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseInteresGeneradoPFDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="interesFinalPlazo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interesAdelantado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interesMensual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseInteresGeneradoPFDTO", propOrder = {
    "interesFinalPlazo",
    "interesAdelantado",
    "interesMensual"
})
public class ResponseInteresGeneradoPFDTO {

    protected String interesFinalPlazo;
    protected String interesAdelantado;
    protected String interesMensual;

    /**
     * Obtiene el valor de la propiedad interesFinalPlazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteresFinalPlazo() {
        return interesFinalPlazo;
    }

    /**
     * Define el valor de la propiedad interesFinalPlazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteresFinalPlazo(String value) {
        this.interesFinalPlazo = value;
    }

    /**
     * Obtiene el valor de la propiedad interesAdelantado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteresAdelantado() {
        return interesAdelantado;
    }

    /**
     * Define el valor de la propiedad interesAdelantado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteresAdelantado(String value) {
        this.interesAdelantado = value;
    }

    /**
     * Obtiene el valor de la propiedad interesMensual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteresMensual() {
        return interesMensual;
    }

    /**
     * Define el valor de la propiedad interesMensual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteresMensual(String value) {
        this.interesMensual = value;
    }

}
