
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xd_fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_tipovalor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xdFecha",
    "xnTipovalor"
})
@XmlRootElement(name = "WS_GET_TIPOCAMBIO")
public class WSGETTIPOCAMBIO {

    @XmlElement(name = "xd_fecha")
    protected String xdFecha;
    @XmlElement(name = "xn_tipovalor")
    protected int xnTipovalor;

    /**
     * Obtiene el valor de la propiedad xdFecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXdFecha() {
        return xdFecha;
    }

    /**
     * Define el valor de la propiedad xdFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXdFecha(String value) {
        this.xdFecha = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipovalor.
     * 
     */
    public int getXnTipovalor() {
        return xnTipovalor;
    }

    /**
     * Define el valor de la propiedad xnTipovalor.
     * 
     */
    public void setXnTipovalor(int value) {
        this.xnTipovalor = value;
    }

}
