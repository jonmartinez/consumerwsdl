
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_CONSTANCIA_OPERACIONResult" type="{http://cajamaynas.pe/}ResponseConstanciaOperacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetconstanciaoperacionResult"
})
@XmlRootElement(name = "WS_GET_CONSTANCIA_OPERACIONResponse")
public class WSGETCONSTANCIAOPERACIONResponse {

    @XmlElement(name = "WS_GET_CONSTANCIA_OPERACIONResult")
    protected ResponseConstanciaOperacion wsgetconstanciaoperacionResult;

    /**
     * Obtiene el valor de la propiedad wsgetconstanciaoperacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseConstanciaOperacion }
     *     
     */
    public ResponseConstanciaOperacion getWSGETCONSTANCIAOPERACIONResult() {
        return wsgetconstanciaoperacionResult;
    }

    /**
     * Define el valor de la propiedad wsgetconstanciaoperacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseConstanciaOperacion }
     *     
     */
    public void setWSGETCONSTANCIAOPERACIONResult(ResponseConstanciaOperacion value) {
        this.wsgetconstanciaoperacionResult = value;
    }

}
