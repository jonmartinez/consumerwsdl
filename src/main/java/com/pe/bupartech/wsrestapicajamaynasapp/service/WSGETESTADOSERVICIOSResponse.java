
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_ESTADOSERVICIOSResult" type="{http://cajamaynas.pe/}ResponseResulOperacionDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetestadoserviciosResult"
})
@XmlRootElement(name = "WS_GET_ESTADOSERVICIOSResponse")
public class WSGETESTADOSERVICIOSResponse {

    @XmlElement(name = "WS_GET_ESTADOSERVICIOSResult")
    protected ResponseResulOperacionDTO wsgetestadoserviciosResult;

    /**
     * Obtiene el valor de la propiedad wsgetestadoserviciosResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public ResponseResulOperacionDTO getWSGETESTADOSERVICIOSResult() {
        return wsgetestadoserviciosResult;
    }

    /**
     * Define el valor de la propiedad wsgetestadoserviciosResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public void setWSGETESTADOSERVICIOSResult(ResponseResulOperacionDTO value) {
        this.wsgetestadoserviciosResult = value;
    }

}
