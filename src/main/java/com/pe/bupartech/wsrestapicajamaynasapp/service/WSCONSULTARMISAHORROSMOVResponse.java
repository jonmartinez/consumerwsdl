
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_MISAHORROS_MOVResult" type="{http://cajamaynas.pe/}ResponseMisAhorros" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultarmisahorrosmovResult"
})
@XmlRootElement(name = "WS_CONSULTAR_MISAHORROS_MOVResponse")
public class WSCONSULTARMISAHORROSMOVResponse {

    @XmlElement(name = "WS_CONSULTAR_MISAHORROS_MOVResult")
    protected ResponseMisAhorros wsconsultarmisahorrosmovResult;

    /**
     * Obtiene el valor de la propiedad wsconsultarmisahorrosmovResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMisAhorros }
     *     
     */
    public ResponseMisAhorros getWSCONSULTARMISAHORROSMOVResult() {
        return wsconsultarmisahorrosmovResult;
    }

    /**
     * Define el valor de la propiedad wsconsultarmisahorrosmovResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMisAhorros }
     *     
     */
    public void setWSCONSULTARMISAHORROSMOVResult(ResponseMisAhorros value) {
        this.wsconsultarmisahorrosmovResult = value;
    }

}
