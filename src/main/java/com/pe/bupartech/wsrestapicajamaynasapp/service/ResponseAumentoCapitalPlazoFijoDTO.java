
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseAumentoCapitalPlazoFijoDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseAumentoCapitalPlazoFijoDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="desc_resultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor_resultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseAumentoCapitalPlazoFijoDTO", propOrder = {
    "resultado",
    "descResultado",
    "valorResultado"
})
public class ResponseAumentoCapitalPlazoFijoDTO {

    protected int resultado;
    @XmlElement(name = "desc_resultado")
    protected String descResultado;
    @XmlElement(name = "valor_resultado")
    protected String valorResultado;

    /**
     * Obtiene el valor de la propiedad resultado.
     * 
     */
    public int getResultado() {
        return resultado;
    }

    /**
     * Define el valor de la propiedad resultado.
     * 
     */
    public void setResultado(int value) {
        this.resultado = value;
    }

    /**
     * Obtiene el valor de la propiedad descResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResultado() {
        return descResultado;
    }

    /**
     * Define el valor de la propiedad descResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResultado(String value) {
        this.descResultado = value;
    }

    /**
     * Obtiene el valor de la propiedad valorResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorResultado() {
        return valorResultado;
    }

    /**
     * Define el valor de la propiedad valorResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorResultado(String value) {
        this.valorResultado = value;
    }

}
