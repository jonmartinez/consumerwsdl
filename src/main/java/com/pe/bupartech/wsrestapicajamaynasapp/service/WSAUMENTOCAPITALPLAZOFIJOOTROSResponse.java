
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_AUMENTO_CAPITAL_PLAZO_FIJO_OTROSResult" type="{http://cajamaynas.pe/}ResponseAumentoCapitalPlazoFijoDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsaumentocapitalplazofijootrosResult"
})
@XmlRootElement(name = "WS_AUMENTO_CAPITAL_PLAZO_FIJO_OTROSResponse")
public class WSAUMENTOCAPITALPLAZOFIJOOTROSResponse {

    @XmlElement(name = "WS_AUMENTO_CAPITAL_PLAZO_FIJO_OTROSResult")
    protected ResponseAumentoCapitalPlazoFijoDTO wsaumentocapitalplazofijootrosResult;

    /**
     * Obtiene el valor de la propiedad wsaumentocapitalplazofijootrosResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAumentoCapitalPlazoFijoDTO }
     *     
     */
    public ResponseAumentoCapitalPlazoFijoDTO getWSAUMENTOCAPITALPLAZOFIJOOTROSResult() {
        return wsaumentocapitalplazofijootrosResult;
    }

    /**
     * Define el valor de la propiedad wsaumentocapitalplazofijootrosResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAumentoCapitalPlazoFijoDTO }
     *     
     */
    public void setWSAUMENTOCAPITALPLAZOFIJOOTROSResult(ResponseAumentoCapitalPlazoFijoDTO value) {
        this.wsaumentocapitalplazofijootrosResult = value;
    }

}
