
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_ALERTA_PREVENSION_FRAUDEResult" type="{http://cajamaynas.pe/}ResponseAlertaHB" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsalertaprevensionfraudeResult"
})
@XmlRootElement(name = "WS_ALERTA_PREVENSION_FRAUDEResponse")
public class WSALERTAPREVENSIONFRAUDEResponse {

    @XmlElement(name = "WS_ALERTA_PREVENSION_FRAUDEResult")
    protected ResponseAlertaHB wsalertaprevensionfraudeResult;

    /**
     * Obtiene el valor de la propiedad wsalertaprevensionfraudeResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAlertaHB }
     *     
     */
    public ResponseAlertaHB getWSALERTAPREVENSIONFRAUDEResult() {
        return wsalertaprevensionfraudeResult;
    }

    /**
     * Define el valor de la propiedad wsalertaprevensionfraudeResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAlertaHB }
     *     
     */
    public void setWSALERTAPREVENSIONFRAUDEResult(ResponseAlertaHB value) {
        this.wsalertaprevensionfraudeResult = value;
    }

}
