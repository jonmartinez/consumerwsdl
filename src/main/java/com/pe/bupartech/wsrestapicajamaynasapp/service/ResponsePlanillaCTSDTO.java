
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponsePlanillaCTSDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponsePlanillaCTSDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bVerificacion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lstObservaciones" type="{http://cajamaynas.pe/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="cMontoCargoCtaOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planilla" type="{http://cajamaynas.pe/}ResponsePlanillaCtsCabDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsePlanillaCTSDTO", propOrder = {
    "bVerificacion",
    "lstObservaciones",
    "cMontoCargoCtaOrigen",
    "planilla"
})
public class ResponsePlanillaCTSDTO {

    protected boolean bVerificacion;
    protected ArrayOfString lstObservaciones;
    protected String cMontoCargoCtaOrigen;
    protected ResponsePlanillaCtsCabDTO planilla;

    /**
     * Obtiene el valor de la propiedad bVerificacion.
     * 
     */
    public boolean isBVerificacion() {
        return bVerificacion;
    }

    /**
     * Define el valor de la propiedad bVerificacion.
     * 
     */
    public void setBVerificacion(boolean value) {
        this.bVerificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad lstObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getLstObservaciones() {
        return lstObservaciones;
    }

    /**
     * Define el valor de la propiedad lstObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setLstObservaciones(ArrayOfString value) {
        this.lstObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad cMontoCargoCtaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMontoCargoCtaOrigen() {
        return cMontoCargoCtaOrigen;
    }

    /**
     * Define el valor de la propiedad cMontoCargoCtaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMontoCargoCtaOrigen(String value) {
        this.cMontoCargoCtaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad planilla.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePlanillaCtsCabDTO }
     *     
     */
    public ResponsePlanillaCtsCabDTO getPlanilla() {
        return planilla;
    }

    /**
     * Define el valor de la propiedad planilla.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePlanillaCtsCabDTO }
     *     
     */
    public void setPlanilla(ResponsePlanillaCtsCabDTO value) {
        this.planilla = value;
    }

}
