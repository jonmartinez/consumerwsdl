
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestLimitesOperacionCab complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestLimitesOperacionCab">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cLimOpeCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nLimOpeXDiaYCliente" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstDetalle" type="{http://cajamaynas.pe/}ArrayOfRequestLimitesOperacionDet" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestLimitesOperacionCab", propOrder = {
    "cLimOpeCod",
    "nLimOpeXDiaYCliente",
    "lstDetalle"
})
public class RequestLimitesOperacionCab {

    protected String cLimOpeCod;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer nLimOpeXDiaYCliente;
    protected ArrayOfRequestLimitesOperacionDet lstDetalle;

    /**
     * Obtiene el valor de la propiedad cLimOpeCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLimOpeCod() {
        return cLimOpeCod;
    }

    /**
     * Define el valor de la propiedad cLimOpeCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLimOpeCod(String value) {
        this.cLimOpeCod = value;
    }

    /**
     * Obtiene el valor de la propiedad nLimOpeXDiaYCliente.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNLimOpeXDiaYCliente() {
        return nLimOpeXDiaYCliente;
    }

    /**
     * Define el valor de la propiedad nLimOpeXDiaYCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNLimOpeXDiaYCliente(Integer value) {
        this.nLimOpeXDiaYCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad lstDetalle.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRequestLimitesOperacionDet }
     *     
     */
    public ArrayOfRequestLimitesOperacionDet getLstDetalle() {
        return lstDetalle;
    }

    /**
     * Define el valor de la propiedad lstDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRequestLimitesOperacionDet }
     *     
     */
    public void setLstDetalle(ArrayOfRequestLimitesOperacionDet value) {
        this.lstDetalle = value;
    }

}
