
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseDetalleCuentaDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseDetalleCuentaDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="age_apertura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_apertura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_cap" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_disp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_retiro_cts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto_desem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nro_totalCuotas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="analista_asig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nro_tel_analista" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prox_cuota" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="prox_montoPendiente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_venc_cuota" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dias_mora" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fecha_venc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="plazo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sub_producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tcea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_persona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseDetalleCuentaDTO", propOrder = {
    "cuenta",
    "titular",
    "cci",
    "tipoProducto",
    "tipoCuenta",
    "ageApertura",
    "fechaApertura",
    "tea",
    "moneda",
    "saldoCap",
    "saldoDisp",
    "saldoRetiroCts",
    "montoDesem",
    "nroTotalCuotas",
    "analistaAsig",
    "nroTelAnalista",
    "proxCuota",
    "proxMontoPendiente",
    "fechaVencCuota",
    "diasMora",
    "fechaVenc",
    "plazo",
    "estado",
    "subProducto",
    "tcea",
    "codPersona"
})
public class ResponseDetalleCuentaDTO {

    protected String cuenta;
    protected String titular;
    protected String cci;
    @XmlElement(name = "tipo_producto")
    protected String tipoProducto;
    @XmlElement(name = "tipo_cuenta")
    protected int tipoCuenta;
    @XmlElement(name = "age_apertura")
    protected String ageApertura;
    @XmlElement(name = "fecha_apertura")
    protected String fechaApertura;
    protected String tea;
    protected String moneda;
    @XmlElement(name = "saldo_cap")
    protected String saldoCap;
    @XmlElement(name = "saldo_disp")
    protected String saldoDisp;
    @XmlElement(name = "saldo_retiro_cts")
    protected String saldoRetiroCts;
    @XmlElement(name = "monto_desem")
    protected String montoDesem;
    @XmlElement(name = "nro_totalCuotas")
    protected int nroTotalCuotas;
    @XmlElement(name = "analista_asig")
    protected String analistaAsig;
    @XmlElement(name = "nro_tel_analista")
    protected String nroTelAnalista;
    @XmlElement(name = "prox_cuota")
    protected int proxCuota;
    @XmlElement(name = "prox_montoPendiente")
    protected String proxMontoPendiente;
    @XmlElement(name = "fecha_venc_cuota")
    protected String fechaVencCuota;
    @XmlElement(name = "dias_mora")
    protected int diasMora;
    @XmlElement(name = "fecha_venc")
    protected String fechaVenc;
    protected int plazo;
    protected String estado;
    @XmlElement(name = "sub_producto")
    protected String subProducto;
    protected String tcea;
    @XmlElement(name = "cod_persona")
    protected String codPersona;

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitular(String value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad cci.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCci() {
        return cci;
    }

    /**
     * Define el valor de la propiedad cci.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCci(String value) {
        this.cci = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProducto() {
        return tipoProducto;
    }

    /**
     * Define el valor de la propiedad tipoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProducto(String value) {
        this.tipoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCuenta.
     * 
     */
    public int getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * Define el valor de la propiedad tipoCuenta.
     * 
     */
    public void setTipoCuenta(int value) {
        this.tipoCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad ageApertura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgeApertura() {
        return ageApertura;
    }

    /**
     * Define el valor de la propiedad ageApertura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgeApertura(String value) {
        this.ageApertura = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaApertura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaApertura() {
        return fechaApertura;
    }

    /**
     * Define el valor de la propiedad fechaApertura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaApertura(String value) {
        this.fechaApertura = value;
    }

    /**
     * Obtiene el valor de la propiedad tea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTea() {
        return tea;
    }

    /**
     * Define el valor de la propiedad tea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTea(String value) {
        this.tea = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoCap.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoCap() {
        return saldoCap;
    }

    /**
     * Define el valor de la propiedad saldoCap.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoCap(String value) {
        this.saldoCap = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoDisp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoDisp() {
        return saldoDisp;
    }

    /**
     * Define el valor de la propiedad saldoDisp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoDisp(String value) {
        this.saldoDisp = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoRetiroCts.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoRetiroCts() {
        return saldoRetiroCts;
    }

    /**
     * Define el valor de la propiedad saldoRetiroCts.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoRetiroCts(String value) {
        this.saldoRetiroCts = value;
    }

    /**
     * Obtiene el valor de la propiedad montoDesem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoDesem() {
        return montoDesem;
    }

    /**
     * Define el valor de la propiedad montoDesem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoDesem(String value) {
        this.montoDesem = value;
    }

    /**
     * Obtiene el valor de la propiedad nroTotalCuotas.
     * 
     */
    public int getNroTotalCuotas() {
        return nroTotalCuotas;
    }

    /**
     * Define el valor de la propiedad nroTotalCuotas.
     * 
     */
    public void setNroTotalCuotas(int value) {
        this.nroTotalCuotas = value;
    }

    /**
     * Obtiene el valor de la propiedad analistaAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalistaAsig() {
        return analistaAsig;
    }

    /**
     * Define el valor de la propiedad analistaAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalistaAsig(String value) {
        this.analistaAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad nroTelAnalista.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroTelAnalista() {
        return nroTelAnalista;
    }

    /**
     * Define el valor de la propiedad nroTelAnalista.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroTelAnalista(String value) {
        this.nroTelAnalista = value;
    }

    /**
     * Obtiene el valor de la propiedad proxCuota.
     * 
     */
    public int getProxCuota() {
        return proxCuota;
    }

    /**
     * Define el valor de la propiedad proxCuota.
     * 
     */
    public void setProxCuota(int value) {
        this.proxCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad proxMontoPendiente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxMontoPendiente() {
        return proxMontoPendiente;
    }

    /**
     * Define el valor de la propiedad proxMontoPendiente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxMontoPendiente(String value) {
        this.proxMontoPendiente = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencCuota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVencCuota() {
        return fechaVencCuota;
    }

    /**
     * Define el valor de la propiedad fechaVencCuota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVencCuota(String value) {
        this.fechaVencCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad diasMora.
     * 
     */
    public int getDiasMora() {
        return diasMora;
    }

    /**
     * Define el valor de la propiedad diasMora.
     * 
     */
    public void setDiasMora(int value) {
        this.diasMora = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVenc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVenc() {
        return fechaVenc;
    }

    /**
     * Define el valor de la propiedad fechaVenc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVenc(String value) {
        this.fechaVenc = value;
    }

    /**
     * Obtiene el valor de la propiedad plazo.
     * 
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * Define el valor de la propiedad plazo.
     * 
     */
    public void setPlazo(int value) {
        this.plazo = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad subProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubProducto() {
        return subProducto;
    }

    /**
     * Define el valor de la propiedad subProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubProducto(String value) {
        this.subProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad tcea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTcea() {
        return tcea;
    }

    /**
     * Define el valor de la propiedad tcea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTcea(String value) {
        this.tcea = value;
    }

    /**
     * Obtiene el valor de la propiedad codPersona.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPersona() {
        return codPersona;
    }

    /**
     * Define el valor de la propiedad codPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPersona(String value) {
        this.codPersona = value;
    }

}
