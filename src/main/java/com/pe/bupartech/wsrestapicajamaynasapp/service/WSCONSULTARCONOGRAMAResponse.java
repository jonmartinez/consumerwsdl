
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_CONOGRAMAResult" type="{http://cajamaynas.pe/}ResponseCronograma" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultarconogramaResult"
})
@XmlRootElement(name = "WS_CONSULTAR_CONOGRAMAResponse")
public class WSCONSULTARCONOGRAMAResponse {

    @XmlElement(name = "WS_CONSULTAR_CONOGRAMAResult")
    protected ResponseCronograma wsconsultarconogramaResult;

    /**
     * Obtiene el valor de la propiedad wsconsultarconogramaResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseCronograma }
     *     
     */
    public ResponseCronograma getWSCONSULTARCONOGRAMAResult() {
        return wsconsultarconogramaResult;
    }

    /**
     * Define el valor de la propiedad wsconsultarconogramaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseCronograma }
     *     
     */
    public void setWSCONSULTARCONOGRAMAResult(ResponseCronograma value) {
        this.wsconsultarconogramaResult = value;
    }

}
