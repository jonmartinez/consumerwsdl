
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfResponseMisCuentasDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfResponseMisCuentasDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseMisCuentasDTO" type="{http://cajamaynas.pe/}ResponseMisCuentasDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfResponseMisCuentasDTO", propOrder = {
    "responseMisCuentasDTO"
})
public class ArrayOfResponseMisCuentasDTO {

    @XmlElement(name = "ResponseMisCuentasDTO", nillable = true)
    protected List<ResponseMisCuentasDTO> responseMisCuentasDTO;

    /**
     * Gets the value of the responseMisCuentasDTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responseMisCuentasDTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponseMisCuentasDTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseMisCuentasDTO }
     * 
     * 
     */
    public List<ResponseMisCuentasDTO> getResponseMisCuentasDTO() {
        if (responseMisCuentasDTO == null) {
            responseMisCuentasDTO = new ArrayList<ResponseMisCuentasDTO>();
        }
        return this.responseMisCuentasDTO;
    }

}
