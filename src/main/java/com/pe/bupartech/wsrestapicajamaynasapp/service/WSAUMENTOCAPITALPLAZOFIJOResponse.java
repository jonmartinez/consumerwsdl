
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_AUMENTO_CAPITAL_PLAZO_FIJOResult" type="{http://cajamaynas.pe/}ResponseAumentoCapitalPlazoFijoDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsaumentocapitalplazofijoResult"
})
@XmlRootElement(name = "WS_AUMENTO_CAPITAL_PLAZO_FIJOResponse")
public class WSAUMENTOCAPITALPLAZOFIJOResponse {

    @XmlElement(name = "WS_AUMENTO_CAPITAL_PLAZO_FIJOResult")
    protected ResponseAumentoCapitalPlazoFijoDTO wsaumentocapitalplazofijoResult;

    /**
     * Obtiene el valor de la propiedad wsaumentocapitalplazofijoResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAumentoCapitalPlazoFijoDTO }
     *     
     */
    public ResponseAumentoCapitalPlazoFijoDTO getWSAUMENTOCAPITALPLAZOFIJOResult() {
        return wsaumentocapitalplazofijoResult;
    }

    /**
     * Define el valor de la propiedad wsaumentocapitalplazofijoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAumentoCapitalPlazoFijoDTO }
     *     
     */
    public void setWSAUMENTOCAPITALPLAZOFIJOResult(ResponseAumentoCapitalPlazoFijoDTO value) {
        this.wsaumentocapitalplazofijoResult = value;
    }

}
