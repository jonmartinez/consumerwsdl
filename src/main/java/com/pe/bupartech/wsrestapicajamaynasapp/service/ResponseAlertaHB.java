
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseAlertaHB complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseAlertaHB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="descripcionresultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_canal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor_canal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desc_operacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor_montooperacion" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="desc_canal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseAlertaHB", propOrder = {
    "resultado",
    "descripcionresultado",
    "tipoCanal",
    "valorCanal",
    "descOperacion",
    "valorMontooperacion",
    "descCanal"
})
public class ResponseAlertaHB {

    protected int resultado;
    protected String descripcionresultado;
    @XmlElement(name = "tipo_canal")
    protected String tipoCanal;
    @XmlElement(name = "valor_canal")
    protected String valorCanal;
    @XmlElement(name = "desc_operacion")
    protected String descOperacion;
    @XmlElement(name = "valor_montooperacion")
    protected double valorMontooperacion;
    @XmlElement(name = "desc_canal")
    protected String descCanal;

    /**
     * Obtiene el valor de la propiedad resultado.
     * 
     */
    public int getResultado() {
        return resultado;
    }

    /**
     * Define el valor de la propiedad resultado.
     * 
     */
    public void setResultado(int value) {
        this.resultado = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionresultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionresultado() {
        return descripcionresultado;
    }

    /**
     * Define el valor de la propiedad descripcionresultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionresultado(String value) {
        this.descripcionresultado = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCanal() {
        return tipoCanal;
    }

    /**
     * Define el valor de la propiedad tipoCanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCanal(String value) {
        this.tipoCanal = value;
    }

    /**
     * Obtiene el valor de la propiedad valorCanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorCanal() {
        return valorCanal;
    }

    /**
     * Define el valor de la propiedad valorCanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorCanal(String value) {
        this.valorCanal = value;
    }

    /**
     * Obtiene el valor de la propiedad descOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescOperacion() {
        return descOperacion;
    }

    /**
     * Define el valor de la propiedad descOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescOperacion(String value) {
        this.descOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valorMontooperacion.
     * 
     */
    public double getValorMontooperacion() {
        return valorMontooperacion;
    }

    /**
     * Define el valor de la propiedad valorMontooperacion.
     * 
     */
    public void setValorMontooperacion(double value) {
        this.valorMontooperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descCanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCanal() {
        return descCanal;
    }

    /**
     * Define el valor de la propiedad descCanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCanal(String value) {
        this.descCanal = value;
    }

}
