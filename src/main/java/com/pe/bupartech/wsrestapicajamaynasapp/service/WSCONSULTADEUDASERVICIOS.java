
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_codcliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_numrecibo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_codconvenio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCodcliente",
    "xcNumrecibo",
    "xcCodconvenio"
})
@XmlRootElement(name = "WS_CONSULTA_DEUDA_SERVICIOS")
public class WSCONSULTADEUDASERVICIOS {

    @XmlElement(name = "xc_codcliente")
    protected String xcCodcliente;
    @XmlElement(name = "xc_numrecibo")
    protected String xcNumrecibo;
    @XmlElement(name = "xc_codconvenio")
    protected String xcCodconvenio;

    /**
     * Obtiene el valor de la propiedad xcCodcliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCodcliente() {
        return xcCodcliente;
    }

    /**
     * Define el valor de la propiedad xcCodcliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCodcliente(String value) {
        this.xcCodcliente = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNumrecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNumrecibo() {
        return xcNumrecibo;
    }

    /**
     * Define el valor de la propiedad xcNumrecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNumrecibo(String value) {
        this.xcNumrecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCodconvenio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCodconvenio() {
        return xcCodconvenio;
    }

    /**
     * Define el valor de la propiedad xcCodconvenio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCodconvenio(String value) {
        this.xcCodconvenio = value;
    }

}
