
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cCtaCodPF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cCtaCodCargo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_nMonto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="xn_itf" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcIdentificacion",
    "xcCCtaCodPF",
    "xcCCtaCodCargo",
    "xnNMonto",
    "xnItf"
})
@XmlRootElement(name = "WS_AUMENTO_CAPITAL_PLAZO_FIJO_OTROS")
public class WSAUMENTOCAPITALPLAZOFIJOOTROS {

    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;
    @XmlElement(name = "xc_cCtaCodPF")
    protected String xcCCtaCodPF;
    @XmlElement(name = "xc_cCtaCodCargo")
    protected String xcCCtaCodCargo;
    @XmlElement(name = "xn_nMonto")
    protected double xnNMonto;
    @XmlElement(name = "xn_itf")
    protected double xnItf;

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCCtaCodPF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCCtaCodPF() {
        return xcCCtaCodPF;
    }

    /**
     * Define el valor de la propiedad xcCCtaCodPF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCCtaCodPF(String value) {
        this.xcCCtaCodPF = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCCtaCodCargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCCtaCodCargo() {
        return xcCCtaCodCargo;
    }

    /**
     * Define el valor de la propiedad xcCCtaCodCargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCCtaCodCargo(String value) {
        this.xcCCtaCodCargo = value;
    }

    /**
     * Obtiene el valor de la propiedad xnNMonto.
     * 
     */
    public double getXnNMonto() {
        return xnNMonto;
    }

    /**
     * Define el valor de la propiedad xnNMonto.
     * 
     */
    public void setXnNMonto(double value) {
        this.xnNMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad xnItf.
     * 
     */
    public double getXnItf() {
        return xnItf;
    }

    /**
     * Define el valor de la propiedad xnItf.
     * 
     */
    public void setXnItf(double value) {
        this.xnItf = value;
    }

}
