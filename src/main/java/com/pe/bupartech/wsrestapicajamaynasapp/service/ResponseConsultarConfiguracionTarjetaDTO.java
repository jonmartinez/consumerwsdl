
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseConsultarConfiguracionTarjetaDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseConsultarConfiguracionTarjetaDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cPersNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nTipoTarjeta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cNumtarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cPersEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bActivoCompInternet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="nTipoActivacionCompInternet" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cFechaInicioCompInternet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cFechaFinCompInternet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bActivoOpExtranjero" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cFechaInicioOpeExtranjero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cFechaFinOpeExtranjero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nValor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nResultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseConsultarConfiguracionTarjetaDTO", propOrder = {
    "cPersNombre",
    "nTipoTarjeta",
    "cNumtarjeta",
    "cPersEmail",
    "bActivoCompInternet",
    "nTipoActivacionCompInternet",
    "cFechaInicioCompInternet",
    "cFechaFinCompInternet",
    "bActivoOpExtranjero",
    "cFechaInicioOpeExtranjero",
    "cFechaFinOpeExtranjero",
    "cDescripcion",
    "nValor",
    "nResultado"
})
public class ResponseConsultarConfiguracionTarjetaDTO {

    protected String cPersNombre;
    protected int nTipoTarjeta;
    protected String cNumtarjeta;
    protected String cPersEmail;
    protected boolean bActivoCompInternet;
    protected int nTipoActivacionCompInternet;
    protected String cFechaInicioCompInternet;
    protected String cFechaFinCompInternet;
    protected boolean bActivoOpExtranjero;
    protected String cFechaInicioOpeExtranjero;
    protected String cFechaFinOpeExtranjero;
    protected String cDescripcion;
    protected int nValor;
    protected int nResultado;

    /**
     * Obtiene el valor de la propiedad cPersNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPersNombre() {
        return cPersNombre;
    }

    /**
     * Define el valor de la propiedad cPersNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPersNombre(String value) {
        this.cPersNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad nTipoTarjeta.
     * 
     */
    public int getNTipoTarjeta() {
        return nTipoTarjeta;
    }

    /**
     * Define el valor de la propiedad nTipoTarjeta.
     * 
     */
    public void setNTipoTarjeta(int value) {
        this.nTipoTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad cNumtarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNumtarjeta() {
        return cNumtarjeta;
    }

    /**
     * Define el valor de la propiedad cNumtarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNumtarjeta(String value) {
        this.cNumtarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad cPersEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPersEmail() {
        return cPersEmail;
    }

    /**
     * Define el valor de la propiedad cPersEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPersEmail(String value) {
        this.cPersEmail = value;
    }

    /**
     * Obtiene el valor de la propiedad bActivoCompInternet.
     * 
     */
    public boolean isBActivoCompInternet() {
        return bActivoCompInternet;
    }

    /**
     * Define el valor de la propiedad bActivoCompInternet.
     * 
     */
    public void setBActivoCompInternet(boolean value) {
        this.bActivoCompInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad nTipoActivacionCompInternet.
     * 
     */
    public int getNTipoActivacionCompInternet() {
        return nTipoActivacionCompInternet;
    }

    /**
     * Define el valor de la propiedad nTipoActivacionCompInternet.
     * 
     */
    public void setNTipoActivacionCompInternet(int value) {
        this.nTipoActivacionCompInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad cFechaInicioCompInternet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFechaInicioCompInternet() {
        return cFechaInicioCompInternet;
    }

    /**
     * Define el valor de la propiedad cFechaInicioCompInternet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFechaInicioCompInternet(String value) {
        this.cFechaInicioCompInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad cFechaFinCompInternet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFechaFinCompInternet() {
        return cFechaFinCompInternet;
    }

    /**
     * Define el valor de la propiedad cFechaFinCompInternet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFechaFinCompInternet(String value) {
        this.cFechaFinCompInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad bActivoOpExtranjero.
     * 
     */
    public boolean isBActivoOpExtranjero() {
        return bActivoOpExtranjero;
    }

    /**
     * Define el valor de la propiedad bActivoOpExtranjero.
     * 
     */
    public void setBActivoOpExtranjero(boolean value) {
        this.bActivoOpExtranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad cFechaInicioOpeExtranjero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFechaInicioOpeExtranjero() {
        return cFechaInicioOpeExtranjero;
    }

    /**
     * Define el valor de la propiedad cFechaInicioOpeExtranjero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFechaInicioOpeExtranjero(String value) {
        this.cFechaInicioOpeExtranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad cFechaFinOpeExtranjero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFechaFinOpeExtranjero() {
        return cFechaFinOpeExtranjero;
    }

    /**
     * Define el valor de la propiedad cFechaFinOpeExtranjero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFechaFinOpeExtranjero(String value) {
        this.cFechaFinOpeExtranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad cDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDescripcion() {
        return cDescripcion;
    }

    /**
     * Define el valor de la propiedad cDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDescripcion(String value) {
        this.cDescripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad nValor.
     * 
     */
    public int getNValor() {
        return nValor;
    }

    /**
     * Define el valor de la propiedad nValor.
     * 
     */
    public void setNValor(int value) {
        this.nValor = value;
    }

    /**
     * Obtiene el valor de la propiedad nResultado.
     * 
     */
    public int getNResultado() {
        return nResultado;
    }

    /**
     * Define el valor de la propiedad nResultado.
     * 
     */
    public void setNResultado(int value) {
        this.nResultado = value;
    }

}
