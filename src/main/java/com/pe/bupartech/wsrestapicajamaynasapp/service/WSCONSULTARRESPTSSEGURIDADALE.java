
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xn_tipodoi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_nrodoi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_pregunta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xnTipodoi",
    "xcNrodoi",
    "xnPregunta"
})
@XmlRootElement(name = "WS_CONSULTAR_RESPTSSEGURIDADALE")
public class WSCONSULTARRESPTSSEGURIDADALE {

    @XmlElement(name = "xn_tipodoi")
    protected int xnTipodoi;
    @XmlElement(name = "xc_nrodoi")
    protected String xcNrodoi;
    @XmlElement(name = "xn_pregunta")
    protected int xnPregunta;

    /**
     * Obtiene el valor de la propiedad xnTipodoi.
     * 
     */
    public int getXnTipodoi() {
        return xnTipodoi;
    }

    /**
     * Define el valor de la propiedad xnTipodoi.
     * 
     */
    public void setXnTipodoi(int value) {
        this.xnTipodoi = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNrodoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNrodoi() {
        return xcNrodoi;
    }

    /**
     * Define el valor de la propiedad xcNrodoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNrodoi(String value) {
        this.xcNrodoi = value;
    }

    /**
     * Obtiene el valor de la propiedad xnPregunta.
     * 
     */
    public int getXnPregunta() {
        return xnPregunta;
    }

    /**
     * Define el valor de la propiedad xnPregunta.
     * 
     */
    public void setXnPregunta(int value) {
        this.xnPregunta = value;
    }

}
