
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_DETALLECUENTAResult" type="{http://cajamaynas.pe/}ResponseDetalleCuenta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultardetallecuentaResult"
})
@XmlRootElement(name = "WS_CONSULTAR_DETALLECUENTAResponse")
public class WSCONSULTARDETALLECUENTAResponse {

    @XmlElement(name = "WS_CONSULTAR_DETALLECUENTAResult")
    protected ResponseDetalleCuenta wsconsultardetallecuentaResult;

    /**
     * Obtiene el valor de la propiedad wsconsultardetallecuentaResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseDetalleCuenta }
     *     
     */
    public ResponseDetalleCuenta getWSCONSULTARDETALLECUENTAResult() {
        return wsconsultardetallecuentaResult;
    }

    /**
     * Define el valor de la propiedad wsconsultardetallecuentaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseDetalleCuenta }
     *     
     */
    public void setWSCONSULTARDETALLECUENTAResult(ResponseDetalleCuenta value) {
        this.wsconsultardetallecuentaResult = value;
    }

}
