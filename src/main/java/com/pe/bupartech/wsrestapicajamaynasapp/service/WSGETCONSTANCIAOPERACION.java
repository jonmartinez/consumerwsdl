
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xn_movimiento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_opecod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xnMovimiento",
    "xcOpecod"
})
@XmlRootElement(name = "WS_GET_CONSTANCIA_OPERACION")
public class WSGETCONSTANCIAOPERACION {

    @XmlElement(name = "xn_movimiento")
    protected int xnMovimiento;
    @XmlElement(name = "xc_opecod")
    protected String xcOpecod;

    /**
     * Obtiene el valor de la propiedad xnMovimiento.
     * 
     */
    public int getXnMovimiento() {
        return xnMovimiento;
    }

    /**
     * Define el valor de la propiedad xnMovimiento.
     * 
     */
    public void setXnMovimiento(int value) {
        this.xnMovimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad xcOpecod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcOpecod() {
        return xcOpecod;
    }

    /**
     * Define el valor de la propiedad xcOpecod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcOpecod(String value) {
        this.xcOpecod = value;
    }

}
