
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_PAGO_PLANILLA_PROVEEDORES_PAGOResult" type="{http://cajamaynas.pe/}ResponsePagoPlanillaDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wspagoplanillaproveedorespagoResult"
})
@XmlRootElement(name = "WS_PAGO_PLANILLA_PROVEEDORES_PAGOResponse")
public class WSPAGOPLANILLAPROVEEDORESPAGOResponse {

    @XmlElement(name = "WS_PAGO_PLANILLA_PROVEEDORES_PAGOResult")
    protected ResponsePagoPlanillaDTO wspagoplanillaproveedorespagoResult;

    /**
     * Obtiene el valor de la propiedad wspagoplanillaproveedorespagoResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagoPlanillaDTO }
     *     
     */
    public ResponsePagoPlanillaDTO getWSPAGOPLANILLAPROVEEDORESPAGOResult() {
        return wspagoplanillaproveedorespagoResult;
    }

    /**
     * Define el valor de la propiedad wspagoplanillaproveedorespagoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagoPlanillaDTO }
     *     
     */
    public void setWSPAGOPLANILLAPROVEEDORESPAGOResult(ResponsePagoPlanillaDTO value) {
        this.wspagoplanillaproveedorespagoResult = value;
    }

}
