
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseMisAhorrosDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseMisAhorrosDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fecha_transaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hora_transaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nro_mov" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_cap" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nro_transaccion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseMisAhorrosDTO", propOrder = {
    "fechaTransaccion",
    "horaTransaccion",
    "nroMov",
    "operacion",
    "importe",
    "saldoCap",
    "agencia",
    "nroTransaccion"
})
public class ResponseMisAhorrosDTO {

    @XmlElement(name = "fecha_transaccion")
    protected String fechaTransaccion;
    @XmlElement(name = "hora_transaccion")
    protected String horaTransaccion;
    @XmlElement(name = "nro_mov")
    protected long nroMov;
    protected String operacion;
    protected String importe;
    @XmlElement(name = "saldo_cap")
    protected String saldoCap;
    protected String agencia;
    @XmlElement(name = "nro_transaccion")
    protected int nroTransaccion;

    /**
     * Obtiene el valor de la propiedad fechaTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * Define el valor de la propiedad fechaTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaTransaccion(String value) {
        this.fechaTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad horaTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraTransaccion() {
        return horaTransaccion;
    }

    /**
     * Define el valor de la propiedad horaTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraTransaccion(String value) {
        this.horaTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad nroMov.
     * 
     */
    public long getNroMov() {
        return nroMov;
    }

    /**
     * Define el valor de la propiedad nroMov.
     * 
     */
    public void setNroMov(long value) {
        this.nroMov = value;
    }

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperacion(String value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad importe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporte() {
        return importe;
    }

    /**
     * Define el valor de la propiedad importe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporte(String value) {
        this.importe = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoCap.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoCap() {
        return saldoCap;
    }

    /**
     * Define el valor de la propiedad saldoCap.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoCap(String value) {
        this.saldoCap = value;
    }

    /**
     * Obtiene el valor de la propiedad agencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Define el valor de la propiedad agencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencia(String value) {
        this.agencia = value;
    }

    /**
     * Obtiene el valor de la propiedad nroTransaccion.
     * 
     */
    public int getNroTransaccion() {
        return nroTransaccion;
    }

    /**
     * Define el valor de la propiedad nroTransaccion.
     * 
     */
    public void setNroTransaccion(int value) {
        this.nroTransaccion = value;
    }

}
