
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_OBTIENE_TASA_PFResult" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsobtienetasapfResult"
})
@XmlRootElement(name = "WS_OBTIENE_TASA_PFResponse")
public class WSOBTIENETASAPFResponse {

    @XmlElement(name = "WS_OBTIENE_TASA_PFResult", required = true)
    protected BigDecimal wsobtienetasapfResult;

    /**
     * Obtiene el valor de la propiedad wsobtienetasapfResult.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWSOBTIENETASAPFResult() {
        return wsobtienetasapfResult;
    }

    /**
     * Define el valor de la propiedad wsobtienetasapfResult.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWSOBTIENETASAPFResult(BigDecimal value) {
        this.wsobtienetasapfResult = value;
    }

}
