
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseCronogramaDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseCronogramaDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nro_cuota" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fecha_venc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto_cuota" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="capital" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gastos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_cap" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_pago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dias_atraso" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseCronogramaDTO", propOrder = {
    "cuenta",
    "nroCuota",
    "fechaVenc",
    "montoCuota",
    "capital",
    "interes",
    "mora",
    "gastos",
    "saldoCap",
    "fechaPago",
    "diasAtraso",
    "estado"
})
public class ResponseCronogramaDTO {

    protected String cuenta;
    @XmlElement(name = "nro_cuota")
    protected int nroCuota;
    @XmlElement(name = "fecha_venc")
    protected String fechaVenc;
    @XmlElement(name = "monto_cuota")
    protected String montoCuota;
    protected String capital;
    protected String interes;
    protected String mora;
    protected String gastos;
    @XmlElement(name = "saldo_cap")
    protected String saldoCap;
    @XmlElement(name = "fecha_pago")
    protected String fechaPago;
    @XmlElement(name = "dias_atraso")
    protected int diasAtraso;
    protected String estado;

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCuota.
     * 
     */
    public int getNroCuota() {
        return nroCuota;
    }

    /**
     * Define el valor de la propiedad nroCuota.
     * 
     */
    public void setNroCuota(int value) {
        this.nroCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVenc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVenc() {
        return fechaVenc;
    }

    /**
     * Define el valor de la propiedad fechaVenc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVenc(String value) {
        this.fechaVenc = value;
    }

    /**
     * Obtiene el valor de la propiedad montoCuota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoCuota() {
        return montoCuota;
    }

    /**
     * Define el valor de la propiedad montoCuota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoCuota(String value) {
        this.montoCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad capital.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapital() {
        return capital;
    }

    /**
     * Define el valor de la propiedad capital.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapital(String value) {
        this.capital = value;
    }

    /**
     * Obtiene el valor de la propiedad interes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteres() {
        return interes;
    }

    /**
     * Define el valor de la propiedad interes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteres(String value) {
        this.interes = value;
    }

    /**
     * Obtiene el valor de la propiedad mora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMora() {
        return mora;
    }

    /**
     * Define el valor de la propiedad mora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMora(String value) {
        this.mora = value;
    }

    /**
     * Obtiene el valor de la propiedad gastos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastos() {
        return gastos;
    }

    /**
     * Define el valor de la propiedad gastos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastos(String value) {
        this.gastos = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoCap.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoCap() {
        return saldoCap;
    }

    /**
     * Define el valor de la propiedad saldoCap.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoCap(String value) {
        this.saldoCap = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * Define el valor de la propiedad fechaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaPago(String value) {
        this.fechaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad diasAtraso.
     * 
     */
    public int getDiasAtraso() {
        return diasAtraso;
    }

    /**
     * Define el valor de la propiedad diasAtraso.
     * 
     */
    public void setDiasAtraso(int value) {
        this.diasAtraso = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

}
