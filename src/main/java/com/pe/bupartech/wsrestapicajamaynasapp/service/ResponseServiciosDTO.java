
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseServiciosDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseServiciosDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cod_convenio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre_convenio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre_institucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseServiciosDTO", propOrder = {
    "codConvenio",
    "nombreConvenio",
    "nombreInstitucion"
})
public class ResponseServiciosDTO {

    @XmlElement(name = "cod_convenio")
    protected String codConvenio;
    @XmlElement(name = "nombre_convenio")
    protected String nombreConvenio;
    @XmlElement(name = "nombre_institucion")
    protected String nombreInstitucion;

    /**
     * Obtiene el valor de la propiedad codConvenio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodConvenio() {
        return codConvenio;
    }

    /**
     * Define el valor de la propiedad codConvenio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodConvenio(String value) {
        this.codConvenio = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreConvenio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreConvenio() {
        return nombreConvenio;
    }

    /**
     * Define el valor de la propiedad nombreConvenio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreConvenio(String value) {
        this.nombreConvenio = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreInstitucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    /**
     * Define el valor de la propiedad nombreInstitucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreInstitucion(String value) {
        this.nombreInstitucion = value;
    }

}
