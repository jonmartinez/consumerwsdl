
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_request" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcRequest"
})
@XmlRootElement(name = "WS_PAGO_PLANILLA_PAGO")
public class WSPAGOPLANILLAPAGO {

    @XmlElement(name = "xc_request")
    protected String xcRequest;

    /**
     * Obtiene el valor de la propiedad xcRequest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcRequest() {
        return xcRequest;
    }

    /**
     * Define el valor de la propiedad xcRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcRequest(String value) {
        this.xcRequest = value;
    }

}
