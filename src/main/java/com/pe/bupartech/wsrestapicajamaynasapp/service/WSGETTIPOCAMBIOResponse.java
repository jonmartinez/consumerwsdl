
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_TIPOCAMBIOResult" type="{http://cajamaynas.pe/}ResponseTipoCambioDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgettipocambioResult"
})
@XmlRootElement(name = "WS_GET_TIPOCAMBIOResponse")
public class WSGETTIPOCAMBIOResponse {

    @XmlElement(name = "WS_GET_TIPOCAMBIOResult")
    protected ResponseTipoCambioDTO wsgettipocambioResult;

    /**
     * Obtiene el valor de la propiedad wsgettipocambioResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseTipoCambioDTO }
     *     
     */
    public ResponseTipoCambioDTO getWSGETTIPOCAMBIOResult() {
        return wsgettipocambioResult;
    }

    /**
     * Define el valor de la propiedad wsgettipocambioResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseTipoCambioDTO }
     *     
     */
    public void setWSGETTIPOCAMBIOResult(ResponseTipoCambioDTO value) {
        this.wsgettipocambioResult = value;
    }

}
