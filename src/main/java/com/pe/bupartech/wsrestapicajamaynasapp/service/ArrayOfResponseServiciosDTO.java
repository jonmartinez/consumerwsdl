
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfResponseServiciosDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfResponseServiciosDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseServiciosDTO" type="{http://cajamaynas.pe/}ResponseServiciosDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfResponseServiciosDTO", propOrder = {
    "responseServiciosDTO"
})
public class ArrayOfResponseServiciosDTO {

    @XmlElement(name = "ResponseServiciosDTO", nillable = true)
    protected List<ResponseServiciosDTO> responseServiciosDTO;

    /**
     * Gets the value of the responseServiciosDTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responseServiciosDTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponseServiciosDTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseServiciosDTO }
     * 
     * 
     */
    public List<ResponseServiciosDTO> getResponseServiciosDTO() {
        if (responseServiciosDTO == null) {
            responseServiciosDTO = new ArrayList<ResponseServiciosDTO>();
        }
        return this.responseServiciosDTO;
    }

}
