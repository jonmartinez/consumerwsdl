
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponsePignoOpeDetalleDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponsePignoOpeDetalleDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaDesembolso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diasAtraso" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="capital" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gastos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="costoCustodia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="costoNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="total" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigo_consulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FecvenceActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsePignoOpeDetalleDTO", propOrder = {
    "cuenta",
    "moneda",
    "titular",
    "fechaDesembolso",
    "fechaVencimiento",
    "diasAtraso",
    "capital",
    "interes",
    "mora",
    "gastos",
    "costoCustodia",
    "costoNotificacion",
    "total",
    "estado",
    "mensaje",
    "codigoConsulta",
    "fecvenceActual"
})
public class ResponsePignoOpeDetalleDTO {

    protected String cuenta;
    protected String moneda;
    protected String titular;
    protected String fechaDesembolso;
    protected String fechaVencimiento;
    protected int diasAtraso;
    protected String capital;
    protected String interes;
    protected String mora;
    protected String gastos;
    protected String costoCustodia;
    protected String costoNotificacion;
    protected String total;
    protected int estado;
    protected String mensaje;
    @XmlElement(name = "codigo_consulta")
    protected String codigoConsulta;
    @XmlElement(name = "FecvenceActual")
    protected String fecvenceActual;

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitular(String value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDesembolso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDesembolso() {
        return fechaDesembolso;
    }

    /**
     * Define el valor de la propiedad fechaDesembolso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDesembolso(String value) {
        this.fechaDesembolso = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Define el valor de la propiedad fechaVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVencimiento(String value) {
        this.fechaVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad diasAtraso.
     * 
     */
    public int getDiasAtraso() {
        return diasAtraso;
    }

    /**
     * Define el valor de la propiedad diasAtraso.
     * 
     */
    public void setDiasAtraso(int value) {
        this.diasAtraso = value;
    }

    /**
     * Obtiene el valor de la propiedad capital.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapital() {
        return capital;
    }

    /**
     * Define el valor de la propiedad capital.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapital(String value) {
        this.capital = value;
    }

    /**
     * Obtiene el valor de la propiedad interes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteres() {
        return interes;
    }

    /**
     * Define el valor de la propiedad interes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteres(String value) {
        this.interes = value;
    }

    /**
     * Obtiene el valor de la propiedad mora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMora() {
        return mora;
    }

    /**
     * Define el valor de la propiedad mora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMora(String value) {
        this.mora = value;
    }

    /**
     * Obtiene el valor de la propiedad gastos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastos() {
        return gastos;
    }

    /**
     * Define el valor de la propiedad gastos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastos(String value) {
        this.gastos = value;
    }

    /**
     * Obtiene el valor de la propiedad costoCustodia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostoCustodia() {
        return costoCustodia;
    }

    /**
     * Define el valor de la propiedad costoCustodia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostoCustodia(String value) {
        this.costoCustodia = value;
    }

    /**
     * Obtiene el valor de la propiedad costoNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostoNotificacion() {
        return costoNotificacion;
    }

    /**
     * Define el valor de la propiedad costoNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostoNotificacion(String value) {
        this.costoNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad total.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotal() {
        return total;
    }

    /**
     * Define el valor de la propiedad total.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotal(String value) {
        this.total = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     */
    public void setEstado(int value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConsulta() {
        return codigoConsulta;
    }

    /**
     * Define el valor de la propiedad codigoConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConsulta(String value) {
        this.codigoConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad fecvenceActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecvenceActual() {
        return fecvenceActual;
    }

    /**
     * Define el valor de la propiedad fecvenceActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecvenceActual(String value) {
        this.fecvenceActual = value;
    }

}
