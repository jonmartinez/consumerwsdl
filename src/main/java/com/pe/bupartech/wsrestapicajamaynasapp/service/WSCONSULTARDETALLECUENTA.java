
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCuenta"
})
@XmlRootElement(name = "WS_CONSULTAR_DETALLECUENTA")
public class WSCONSULTARDETALLECUENTA {

    @XmlElement(name = "xc_cuenta")
    protected String xcCuenta;

    /**
     * Obtiene el valor de la propiedad xcCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuenta() {
        return xcCuenta;
    }

    /**
     * Define el valor de la propiedad xcCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuenta(String value) {
        this.xcCuenta = value;
    }

}
