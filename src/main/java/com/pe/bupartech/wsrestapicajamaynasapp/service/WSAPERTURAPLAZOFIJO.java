
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xn_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_saldo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="xn_plazo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_forma_retiro" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_sub_producto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xnIdentificacion",
    "xnSaldo",
    "xnPlazo",
    "xnFormaRetiro",
    "xcCuenta",
    "xnSubProducto"
})
@XmlRootElement(name = "WS_APERTURA_PLAZO_FIJO")
public class WSAPERTURAPLAZOFIJO {

    @XmlElement(name = "xn_identificacion")
    protected String xnIdentificacion;
    @XmlElement(name = "xn_saldo")
    protected double xnSaldo;
    @XmlElement(name = "xn_plazo")
    protected int xnPlazo;
    @XmlElement(name = "xn_forma_retiro")
    protected int xnFormaRetiro;
    @XmlElement(name = "xc_cuenta")
    protected String xcCuenta;
    @XmlElement(name = "xn_sub_producto")
    protected int xnSubProducto;

    /**
     * Obtiene el valor de la propiedad xnIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXnIdentificacion() {
        return xnIdentificacion;
    }

    /**
     * Define el valor de la propiedad xnIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXnIdentificacion(String value) {
        this.xnIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad xnSaldo.
     * 
     */
    public double getXnSaldo() {
        return xnSaldo;
    }

    /**
     * Define el valor de la propiedad xnSaldo.
     * 
     */
    public void setXnSaldo(double value) {
        this.xnSaldo = value;
    }

    /**
     * Obtiene el valor de la propiedad xnPlazo.
     * 
     */
    public int getXnPlazo() {
        return xnPlazo;
    }

    /**
     * Define el valor de la propiedad xnPlazo.
     * 
     */
    public void setXnPlazo(int value) {
        this.xnPlazo = value;
    }

    /**
     * Obtiene el valor de la propiedad xnFormaRetiro.
     * 
     */
    public int getXnFormaRetiro() {
        return xnFormaRetiro;
    }

    /**
     * Define el valor de la propiedad xnFormaRetiro.
     * 
     */
    public void setXnFormaRetiro(int value) {
        this.xnFormaRetiro = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuenta() {
        return xcCuenta;
    }

    /**
     * Define el valor de la propiedad xcCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuenta(String value) {
        this.xcCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad xnSubProducto.
     * 
     */
    public int getXnSubProducto() {
        return xnSubProducto;
    }

    /**
     * Define el valor de la propiedad xnSubProducto.
     * 
     */
    public void setXnSubProducto(int value) {
        this.xnSubProducto = value;
    }

}
