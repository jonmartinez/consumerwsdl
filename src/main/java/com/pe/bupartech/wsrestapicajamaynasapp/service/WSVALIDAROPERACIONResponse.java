
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_VALIDAR_OPERACIONResult" type="{http://cajamaynas.pe/}ResponseResulOperacionDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsvalidaroperacionResult"
})
@XmlRootElement(name = "WS_VALIDAR_OPERACIONResponse")
public class WSVALIDAROPERACIONResponse {

    @XmlElement(name = "WS_VALIDAR_OPERACIONResult")
    protected ResponseResulOperacionDTO wsvalidaroperacionResult;

    /**
     * Obtiene el valor de la propiedad wsvalidaroperacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public ResponseResulOperacionDTO getWSVALIDAROPERACIONResult() {
        return wsvalidaroperacionResult;
    }

    /**
     * Define el valor de la propiedad wsvalidaroperacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public void setWSVALIDAROPERACIONResult(ResponseResulOperacionDTO value) {
        this.wsvalidaroperacionResult = value;
    }

}
