
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_tipopago" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_montopagar" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xc_cuenta_ahorro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCuenta",
    "xnTipopago",
    "xnMontopagar",
    "xcCuentaAhorro",
    "xcIdentificacion"
})
@XmlRootElement(name = "WS_PAGO_CUOTA_CREDITO_OTROS")
public class WSPAGOCUOTACREDITOOTROS {

    @XmlElement(name = "xc_cuenta")
    protected String xcCuenta;
    @XmlElement(name = "xn_tipopago")
    protected int xnTipopago;
    @XmlElement(name = "xn_montopagar", required = true)
    protected BigDecimal xnMontopagar;
    @XmlElement(name = "xc_cuenta_ahorro")
    protected String xcCuentaAhorro;
    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;

    /**
     * Obtiene el valor de la propiedad xcCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuenta() {
        return xcCuenta;
    }

    /**
     * Define el valor de la propiedad xcCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuenta(String value) {
        this.xcCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipopago.
     * 
     */
    public int getXnTipopago() {
        return xnTipopago;
    }

    /**
     * Define el valor de la propiedad xnTipopago.
     * 
     */
    public void setXnTipopago(int value) {
        this.xnTipopago = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMontopagar.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnMontopagar() {
        return xnMontopagar;
    }

    /**
     * Define el valor de la propiedad xnMontopagar.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnMontopagar(BigDecimal value) {
        this.xnMontopagar = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCuentaAhorro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuentaAhorro() {
        return xcCuentaAhorro;
    }

    /**
     * Define el valor de la propiedad xcCuentaAhorro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuentaAhorro(String value) {
        this.xcCuentaAhorro = value;
    }

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

}
