
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_opecod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cuentaorig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cuentadest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_monto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xn_tipocomision" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_moneda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcOpecod",
    "xcCci",
    "xcCuentaorig",
    "xcCuentadest",
    "xnMonto",
    "xnTipocomision",
    "xnMoneda"
})
@XmlRootElement(name = "WS_GET_COMISION")
public class WSGETCOMISION {

    @XmlElement(name = "xc_opecod")
    protected String xcOpecod;
    @XmlElement(name = "xc_cci")
    protected String xcCci;
    @XmlElement(name = "xc_cuentaorig")
    protected String xcCuentaorig;
    @XmlElement(name = "xc_cuentadest")
    protected String xcCuentadest;
    @XmlElement(name = "xn_monto", required = true)
    protected BigDecimal xnMonto;
    @XmlElement(name = "xn_tipocomision")
    protected int xnTipocomision;
    @XmlElement(name = "xn_moneda")
    protected int xnMoneda;

    /**
     * Obtiene el valor de la propiedad xcOpecod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcOpecod() {
        return xcOpecod;
    }

    /**
     * Define el valor de la propiedad xcOpecod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcOpecod(String value) {
        this.xcOpecod = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCci.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCci() {
        return xcCci;
    }

    /**
     * Define el valor de la propiedad xcCci.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCci(String value) {
        this.xcCci = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCuentaorig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuentaorig() {
        return xcCuentaorig;
    }

    /**
     * Define el valor de la propiedad xcCuentaorig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuentaorig(String value) {
        this.xcCuentaorig = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCuentadest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuentadest() {
        return xcCuentadest;
    }

    /**
     * Define el valor de la propiedad xcCuentadest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuentadest(String value) {
        this.xcCuentadest = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMonto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnMonto() {
        return xnMonto;
    }

    /**
     * Define el valor de la propiedad xnMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnMonto(BigDecimal value) {
        this.xnMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipocomision.
     * 
     */
    public int getXnTipocomision() {
        return xnTipocomision;
    }

    /**
     * Define el valor de la propiedad xnTipocomision.
     * 
     */
    public void setXnTipocomision(int value) {
        this.xnTipocomision = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMoneda.
     * 
     */
    public int getXnMoneda() {
        return xnMoneda;
    }

    /**
     * Define el valor de la propiedad xnMoneda.
     * 
     */
    public void setXnMoneda(int value) {
        this.xnMoneda = value;
    }

}
