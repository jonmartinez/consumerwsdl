
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_cPersCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cCtaCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_nTipo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCPersCod",
    "xcCCtaCod",
    "xnNTipo"
})
@XmlRootElement(name = "WS_VALIDAR_CUENTA_SESION")
public class WSVALIDARCUENTASESION {

    @XmlElement(name = "xc_cPersCod")
    protected String xcCPersCod;
    @XmlElement(name = "xc_cCtaCod")
    protected String xcCCtaCod;
    @XmlElement(name = "xn_nTipo")
    protected int xnNTipo;

    /**
     * Obtiene el valor de la propiedad xcCPersCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCPersCod() {
        return xcCPersCod;
    }

    /**
     * Define el valor de la propiedad xcCPersCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCPersCod(String value) {
        this.xcCPersCod = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCCtaCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCCtaCod() {
        return xcCCtaCod;
    }

    /**
     * Define el valor de la propiedad xcCCtaCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCCtaCod(String value) {
        this.xcCCtaCod = value;
    }

    /**
     * Obtiene el valor de la propiedad xnNTipo.
     * 
     */
    public int getXnNTipo() {
        return xnNTipo;
    }

    /**
     * Define el valor de la propiedad xnNTipo.
     * 
     */
    public void setXnNTipo(int value) {
        this.xnNTipo = value;
    }

}
