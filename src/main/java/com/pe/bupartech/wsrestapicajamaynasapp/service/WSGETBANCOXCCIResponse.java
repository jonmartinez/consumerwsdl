
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_BANCOXCCIResult" type="{http://cajamaynas.pe/}ResponseBancoXCciDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetbancoxcciResult"
})
@XmlRootElement(name = "WS_GET_BANCOXCCIResponse")
public class WSGETBANCOXCCIResponse {

    @XmlElement(name = "WS_GET_BANCOXCCIResult")
    protected ResponseBancoXCciDTO wsgetbancoxcciResult;

    /**
     * Obtiene el valor de la propiedad wsgetbancoxcciResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseBancoXCciDTO }
     *     
     */
    public ResponseBancoXCciDTO getWSGETBANCOXCCIResult() {
        return wsgetbancoxcciResult;
    }

    /**
     * Define el valor de la propiedad wsgetbancoxcciResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseBancoXCciDTO }
     *     
     */
    public void setWSGETBANCOXCCIResult(ResponseBancoXCciDTO value) {
        this.wsgetbancoxcciResult = value;
    }

}
