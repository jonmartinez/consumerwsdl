
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_cCanal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_idcanal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_nmovnro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_moneda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_cperscod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_monto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCCanal",
    "xcIdcanal",
    "xcNmovnro",
    "xcMoneda",
    "xcOperacion",
    "xcCperscod",
    "xcMonto"
})
@XmlRootElement(name = "WS_ALERTA_PREVENSION_FRAUDE")
public class WSALERTAPREVENSIONFRAUDE {

    @XmlElement(name = "xc_cCanal")
    protected String xcCCanal;
    @XmlElement(name = "xc_idcanal")
    protected String xcIdcanal;
    @XmlElement(name = "xc_nmovnro")
    protected String xcNmovnro;
    @XmlElement(name = "xc_moneda")
    protected int xcMoneda;
    @XmlElement(name = "xc_operacion")
    protected int xcOperacion;
    @XmlElement(name = "xc_cperscod")
    protected String xcCperscod;
    @XmlElement(name = "xc_monto")
    protected double xcMonto;

    /**
     * Obtiene el valor de la propiedad xcCCanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCCanal() {
        return xcCCanal;
    }

    /**
     * Define el valor de la propiedad xcCCanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCCanal(String value) {
        this.xcCCanal = value;
    }

    /**
     * Obtiene el valor de la propiedad xcIdcanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdcanal() {
        return xcIdcanal;
    }

    /**
     * Define el valor de la propiedad xcIdcanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdcanal(String value) {
        this.xcIdcanal = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNmovnro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNmovnro() {
        return xcNmovnro;
    }

    /**
     * Define el valor de la propiedad xcNmovnro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNmovnro(String value) {
        this.xcNmovnro = value;
    }

    /**
     * Obtiene el valor de la propiedad xcMoneda.
     * 
     */
    public int getXcMoneda() {
        return xcMoneda;
    }

    /**
     * Define el valor de la propiedad xcMoneda.
     * 
     */
    public void setXcMoneda(int value) {
        this.xcMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad xcOperacion.
     * 
     */
    public int getXcOperacion() {
        return xcOperacion;
    }

    /**
     * Define el valor de la propiedad xcOperacion.
     * 
     */
    public void setXcOperacion(int value) {
        this.xcOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCperscod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCperscod() {
        return xcCperscod;
    }

    /**
     * Define el valor de la propiedad xcCperscod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCperscod(String value) {
        this.xcCperscod = value;
    }

    /**
     * Obtiene el valor de la propiedad xcMonto.
     * 
     */
    public double getXcMonto() {
        return xcMonto;
    }

    /**
     * Define el valor de la propiedad xcMonto.
     * 
     */
    public void setXcMonto(double value) {
        this.xcMonto = value;
    }

}
