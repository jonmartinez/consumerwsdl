
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfRequestLimitesOperacionDet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRequestLimitesOperacionDet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestLimitesOperacionDet" type="{http://cajamaynas.pe/}RequestLimitesOperacionDet" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRequestLimitesOperacionDet", propOrder = {
    "requestLimitesOperacionDet"
})
public class ArrayOfRequestLimitesOperacionDet {

    @XmlElement(name = "RequestLimitesOperacionDet", nillable = true)
    protected List<RequestLimitesOperacionDet> requestLimitesOperacionDet;

    /**
     * Gets the value of the requestLimitesOperacionDet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestLimitesOperacionDet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestLimitesOperacionDet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestLimitesOperacionDet }
     * 
     * 
     */
    public List<RequestLimitesOperacionDet> getRequestLimitesOperacionDet() {
        if (requestLimitesOperacionDet == null) {
            requestLimitesOperacionDet = new ArrayList<RequestLimitesOperacionDet>();
        }
        return this.requestLimitesOperacionDet;
    }

}
