
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xb_ActComprasInternet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="xn_TipoActCompInternet" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_FechaIniCompInternet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_FechaFinCompInternet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xb_ActOpeExtranjero" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="xc_FechaIniOpeExtranjero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_FechaFinOpeExtranjero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_numTarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xbActComprasInternet",
    "xnTipoActCompInternet",
    "xcFechaIniCompInternet",
    "xcFechaFinCompInternet",
    "xbActOpeExtranjero",
    "xcFechaIniOpeExtranjero",
    "xcFechaFinOpeExtranjero",
    "xcNumTarjeta",
    "xcIdentificacion"
})
@XmlRootElement(name = "WS_ACTUALIZAR_CONFIGURACION_TARJETA")
public class WSACTUALIZARCONFIGURACIONTARJETA {

    @XmlElement(name = "xb_ActComprasInternet")
    protected boolean xbActComprasInternet;
    @XmlElement(name = "xn_TipoActCompInternet")
    protected int xnTipoActCompInternet;
    @XmlElement(name = "xc_FechaIniCompInternet")
    protected String xcFechaIniCompInternet;
    @XmlElement(name = "xc_FechaFinCompInternet")
    protected String xcFechaFinCompInternet;
    @XmlElement(name = "xb_ActOpeExtranjero")
    protected boolean xbActOpeExtranjero;
    @XmlElement(name = "xc_FechaIniOpeExtranjero")
    protected String xcFechaIniOpeExtranjero;
    @XmlElement(name = "xc_FechaFinOpeExtranjero")
    protected String xcFechaFinOpeExtranjero;
    @XmlElement(name = "xc_numTarjeta")
    protected String xcNumTarjeta;
    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;

    /**
     * Obtiene el valor de la propiedad xbActComprasInternet.
     * 
     */
    public boolean isXbActComprasInternet() {
        return xbActComprasInternet;
    }

    /**
     * Define el valor de la propiedad xbActComprasInternet.
     * 
     */
    public void setXbActComprasInternet(boolean value) {
        this.xbActComprasInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipoActCompInternet.
     * 
     */
    public int getXnTipoActCompInternet() {
        return xnTipoActCompInternet;
    }

    /**
     * Define el valor de la propiedad xnTipoActCompInternet.
     * 
     */
    public void setXnTipoActCompInternet(int value) {
        this.xnTipoActCompInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad xcFechaIniCompInternet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcFechaIniCompInternet() {
        return xcFechaIniCompInternet;
    }

    /**
     * Define el valor de la propiedad xcFechaIniCompInternet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcFechaIniCompInternet(String value) {
        this.xcFechaIniCompInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad xcFechaFinCompInternet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcFechaFinCompInternet() {
        return xcFechaFinCompInternet;
    }

    /**
     * Define el valor de la propiedad xcFechaFinCompInternet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcFechaFinCompInternet(String value) {
        this.xcFechaFinCompInternet = value;
    }

    /**
     * Obtiene el valor de la propiedad xbActOpeExtranjero.
     * 
     */
    public boolean isXbActOpeExtranjero() {
        return xbActOpeExtranjero;
    }

    /**
     * Define el valor de la propiedad xbActOpeExtranjero.
     * 
     */
    public void setXbActOpeExtranjero(boolean value) {
        this.xbActOpeExtranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad xcFechaIniOpeExtranjero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcFechaIniOpeExtranjero() {
        return xcFechaIniOpeExtranjero;
    }

    /**
     * Define el valor de la propiedad xcFechaIniOpeExtranjero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcFechaIniOpeExtranjero(String value) {
        this.xcFechaIniOpeExtranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad xcFechaFinOpeExtranjero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcFechaFinOpeExtranjero() {
        return xcFechaFinOpeExtranjero;
    }

    /**
     * Define el valor de la propiedad xcFechaFinOpeExtranjero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcFechaFinOpeExtranjero(String value) {
        this.xcFechaFinOpeExtranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNumTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNumTarjeta() {
        return xcNumTarjeta;
    }

    /**
     * Define el valor de la propiedad xcNumTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNumTarjeta(String value) {
        this.xcNumTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

}
