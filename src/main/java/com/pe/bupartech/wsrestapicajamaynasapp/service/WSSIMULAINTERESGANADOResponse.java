
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_SIMULA_INTERES_GANADOResult" type="{http://cajamaynas.pe/}ResponseInteresGeneradoPFDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wssimulainteresganadoResult"
})
@XmlRootElement(name = "WS_SIMULA_INTERES_GANADOResponse")
public class WSSIMULAINTERESGANADOResponse {

    @XmlElement(name = "WS_SIMULA_INTERES_GANADOResult")
    protected ResponseInteresGeneradoPFDTO wssimulainteresganadoResult;

    /**
     * Obtiene el valor de la propiedad wssimulainteresganadoResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseInteresGeneradoPFDTO }
     *     
     */
    public ResponseInteresGeneradoPFDTO getWSSIMULAINTERESGANADOResult() {
        return wssimulainteresganadoResult;
    }

    /**
     * Define el valor de la propiedad wssimulainteresganadoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseInteresGeneradoPFDTO }
     *     
     */
    public void setWSSIMULAINTERESGANADOResult(ResponseInteresGeneradoPFDTO value) {
        this.wssimulainteresganadoResult = value;
    }

}
