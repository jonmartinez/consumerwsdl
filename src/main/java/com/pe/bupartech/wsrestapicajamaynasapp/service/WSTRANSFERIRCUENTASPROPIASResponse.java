
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_TRANSFERIR_CUENTASPROPIASResult" type="{http://cajamaynas.pe/}ResponseTransferenciaDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wstransferircuentaspropiasResult"
})
@XmlRootElement(name = "WS_TRANSFERIR_CUENTASPROPIASResponse")
public class WSTRANSFERIRCUENTASPROPIASResponse {

    @XmlElement(name = "WS_TRANSFERIR_CUENTASPROPIASResult")
    protected ResponseTransferenciaDTO wstransferircuentaspropiasResult;

    /**
     * Obtiene el valor de la propiedad wstransferircuentaspropiasResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseTransferenciaDTO }
     *     
     */
    public ResponseTransferenciaDTO getWSTRANSFERIRCUENTASPROPIASResult() {
        return wstransferircuentaspropiasResult;
    }

    /**
     * Define el valor de la propiedad wstransferircuentaspropiasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseTransferenciaDTO }
     *     
     */
    public void setWSTRANSFERIRCUENTASPROPIASResult(ResponseTransferenciaDTO value) {
        this.wstransferircuentaspropiasResult = value;
    }

}
