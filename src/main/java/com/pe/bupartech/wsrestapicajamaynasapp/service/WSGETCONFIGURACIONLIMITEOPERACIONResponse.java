
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_CONFIGURACION_LIMITE_OPERACIONResult" type="{http://cajamaynas.pe/}ArrayOfRequestLimitesOperacionCab" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetconfiguracionlimiteoperacionResult"
})
@XmlRootElement(name = "WS_GET_CONFIGURACION_LIMITE_OPERACIONResponse")
public class WSGETCONFIGURACIONLIMITEOPERACIONResponse {

    @XmlElement(name = "WS_GET_CONFIGURACION_LIMITE_OPERACIONResult")
    protected ArrayOfRequestLimitesOperacionCab wsgetconfiguracionlimiteoperacionResult;

    /**
     * Obtiene el valor de la propiedad wsgetconfiguracionlimiteoperacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRequestLimitesOperacionCab }
     *     
     */
    public ArrayOfRequestLimitesOperacionCab getWSGETCONFIGURACIONLIMITEOPERACIONResult() {
        return wsgetconfiguracionlimiteoperacionResult;
    }

    /**
     * Define el valor de la propiedad wsgetconfiguracionlimiteoperacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRequestLimitesOperacionCab }
     *     
     */
    public void setWSGETCONFIGURACIONLIMITEOPERACIONResult(ArrayOfRequestLimitesOperacionCab value) {
        this.wsgetconfiguracionlimiteoperacionResult = value;
    }

}
