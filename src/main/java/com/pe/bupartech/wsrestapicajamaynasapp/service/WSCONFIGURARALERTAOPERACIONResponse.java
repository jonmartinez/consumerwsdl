
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONFIGURAR_ALERTA_OPERACIONResult" type="{http://cajamaynas.pe/}ResponseResulOperacionDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconfiguraralertaoperacionResult"
})
@XmlRootElement(name = "WS_CONFIGURAR_ALERTA_OPERACIONResponse")
public class WSCONFIGURARALERTAOPERACIONResponse {

    @XmlElement(name = "WS_CONFIGURAR_ALERTA_OPERACIONResult")
    protected ResponseResulOperacionDTO wsconfiguraralertaoperacionResult;

    /**
     * Obtiene el valor de la propiedad wsconfiguraralertaoperacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public ResponseResulOperacionDTO getWSCONFIGURARALERTAOPERACIONResult() {
        return wsconfiguraralertaoperacionResult;
    }

    /**
     * Define el valor de la propiedad wsconfiguraralertaoperacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseResulOperacionDTO }
     *     
     */
    public void setWSCONFIGURARALERTAOPERACIONResult(ResponseResulOperacionDTO value) {
        this.wsconfiguraralertaoperacionResult = value;
    }

}
