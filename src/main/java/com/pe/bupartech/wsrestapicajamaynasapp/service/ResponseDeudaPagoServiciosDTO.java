
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseDeudaPagoServiciosDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseDeudaPagoServiciosDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cod_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="num_recibo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deuda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numref" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="respuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto_parcial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comision_servicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="concepto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseDeudaPagoServiciosDTO", propOrder = {
    "codCliente",
    "numRecibo",
    "deuda",
    "estado",
    "cuenta",
    "numref",
    "respuesta",
    "montoParcial",
    "mora",
    "comisionServicio",
    "concepto",
    "nombreCliente"
})
public class ResponseDeudaPagoServiciosDTO {

    @XmlElement(name = "cod_cliente")
    protected String codCliente;
    @XmlElement(name = "num_recibo")
    protected String numRecibo;
    protected String deuda;
    protected String estado;
    protected String cuenta;
    protected int numref;
    protected String respuesta;
    @XmlElement(name = "monto_parcial")
    protected String montoParcial;
    protected String mora;
    @XmlElement(name = "comision_servicio")
    protected String comisionServicio;
    protected String concepto;
    @XmlElement(name = "nombre_cliente")
    protected String nombreCliente;

    /**
     * Obtiene el valor de la propiedad codCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCliente() {
        return codCliente;
    }

    /**
     * Define el valor de la propiedad codCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCliente(String value) {
        this.codCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad numRecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRecibo() {
        return numRecibo;
    }

    /**
     * Define el valor de la propiedad numRecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRecibo(String value) {
        this.numRecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad deuda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeuda() {
        return deuda;
    }

    /**
     * Define el valor de la propiedad deuda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeuda(String value) {
        this.deuda = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad numref.
     * 
     */
    public int getNumref() {
        return numref;
    }

    /**
     * Define el valor de la propiedad numref.
     * 
     */
    public void setNumref(int value) {
        this.numref = value;
    }

    /**
     * Obtiene el valor de la propiedad respuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespuesta() {
        return respuesta;
    }

    /**
     * Define el valor de la propiedad respuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespuesta(String value) {
        this.respuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad montoParcial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoParcial() {
        return montoParcial;
    }

    /**
     * Define el valor de la propiedad montoParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoParcial(String value) {
        this.montoParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad mora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMora() {
        return mora;
    }

    /**
     * Define el valor de la propiedad mora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMora(String value) {
        this.mora = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionServicio() {
        return comisionServicio;
    }

    /**
     * Define el valor de la propiedad comisionServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionServicio(String value) {
        this.comisionServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad concepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * Define el valor de la propiedad concepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConcepto(String value) {
        this.concepto = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Define el valor de la propiedad nombreCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCliente(String value) {
        this.nombreCliente = value;
    }

}
