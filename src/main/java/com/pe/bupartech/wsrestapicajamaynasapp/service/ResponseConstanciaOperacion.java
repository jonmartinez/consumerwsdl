
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseConstanciaOperacion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseConstanciaOperacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Movimiento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Hora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TitularOrig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CuentaOrig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoProductoOrig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MontoDebitado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ITFOrig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TitularDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CuentaDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoProductoDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MontoAbonado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComisionTransf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCIBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BancoDestino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MontoTransferido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComisionInter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComisionCMAC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDePago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NroCuota" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FechaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MontoPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CapitalPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InteresPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MoraPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GastoPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GastoComisionPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GastoSegdePagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GastoPolizaIncendioPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GastoPolizaVehiculoPagado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiasMora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SaldoCap" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComisionBI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ITFDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseConstanciaOperacion", propOrder = {
    "movimiento",
    "fecha",
    "hora",
    "titularOrig",
    "cuentaOrig",
    "tipoProductoOrig",
    "montoDebitado",
    "itfOrig",
    "titularDest",
    "cuentaDest",
    "tipoProductoDest",
    "montoAbonado",
    "comisionTransf",
    "cciBeneficiario",
    "bancoDestino",
    "montoTransferido",
    "comisionInter",
    "comisionCMAC",
    "tipoDePago",
    "nroCuota",
    "fechaPago",
    "montoPagado",
    "capitalPagado",
    "interesPagado",
    "moraPagado",
    "gastoPagado",
    "gastoComisionPagado",
    "gastoSegdePagado",
    "gastoPolizaIncendioPagado",
    "gastoPolizaVehiculoPagado",
    "diasMora",
    "saldoCap",
    "comisionBI",
    "itfDest"
})
public class ResponseConstanciaOperacion {

    @XmlElement(name = "Movimiento")
    protected int movimiento;
    @XmlElement(name = "Fecha")
    protected String fecha;
    @XmlElement(name = "Hora")
    protected String hora;
    @XmlElement(name = "TitularOrig")
    protected String titularOrig;
    @XmlElement(name = "CuentaOrig")
    protected String cuentaOrig;
    @XmlElement(name = "TipoProductoOrig")
    protected String tipoProductoOrig;
    @XmlElement(name = "MontoDebitado")
    protected String montoDebitado;
    @XmlElement(name = "ITFOrig")
    protected String itfOrig;
    @XmlElement(name = "TitularDest")
    protected String titularDest;
    @XmlElement(name = "CuentaDest")
    protected String cuentaDest;
    @XmlElement(name = "TipoProductoDest")
    protected String tipoProductoDest;
    @XmlElement(name = "MontoAbonado")
    protected String montoAbonado;
    @XmlElement(name = "ComisionTransf")
    protected String comisionTransf;
    @XmlElement(name = "CCIBeneficiario")
    protected String cciBeneficiario;
    @XmlElement(name = "BancoDestino")
    protected String bancoDestino;
    @XmlElement(name = "MontoTransferido")
    protected String montoTransferido;
    @XmlElement(name = "ComisionInter")
    protected String comisionInter;
    @XmlElement(name = "ComisionCMAC")
    protected String comisionCMAC;
    @XmlElement(name = "TipoDePago")
    protected String tipoDePago;
    @XmlElement(name = "NroCuota")
    protected int nroCuota;
    @XmlElement(name = "FechaPago")
    protected String fechaPago;
    @XmlElement(name = "MontoPagado")
    protected String montoPagado;
    @XmlElement(name = "CapitalPagado")
    protected String capitalPagado;
    @XmlElement(name = "InteresPagado")
    protected String interesPagado;
    @XmlElement(name = "MoraPagado")
    protected String moraPagado;
    @XmlElement(name = "GastoPagado")
    protected String gastoPagado;
    @XmlElement(name = "GastoComisionPagado")
    protected String gastoComisionPagado;
    @XmlElement(name = "GastoSegdePagado")
    protected String gastoSegdePagado;
    @XmlElement(name = "GastoPolizaIncendioPagado")
    protected String gastoPolizaIncendioPagado;
    @XmlElement(name = "GastoPolizaVehiculoPagado")
    protected String gastoPolizaVehiculoPagado;
    @XmlElement(name = "DiasMora")
    protected String diasMora;
    @XmlElement(name = "SaldoCap")
    protected String saldoCap;
    @XmlElement(name = "ComisionBI")
    protected String comisionBI;
    @XmlElement(name = "ITFDest")
    protected String itfDest;

    /**
     * Obtiene el valor de la propiedad movimiento.
     * 
     */
    public int getMovimiento() {
        return movimiento;
    }

    /**
     * Define el valor de la propiedad movimiento.
     * 
     */
    public void setMovimiento(int value) {
        this.movimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad hora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHora() {
        return hora;
    }

    /**
     * Define el valor de la propiedad hora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHora(String value) {
        this.hora = value;
    }

    /**
     * Obtiene el valor de la propiedad titularOrig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitularOrig() {
        return titularOrig;
    }

    /**
     * Define el valor de la propiedad titularOrig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitularOrig(String value) {
        this.titularOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaOrig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaOrig() {
        return cuentaOrig;
    }

    /**
     * Define el valor de la propiedad cuentaOrig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaOrig(String value) {
        this.cuentaOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProductoOrig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProductoOrig() {
        return tipoProductoOrig;
    }

    /**
     * Define el valor de la propiedad tipoProductoOrig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProductoOrig(String value) {
        this.tipoProductoOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad montoDebitado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoDebitado() {
        return montoDebitado;
    }

    /**
     * Define el valor de la propiedad montoDebitado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoDebitado(String value) {
        this.montoDebitado = value;
    }

    /**
     * Obtiene el valor de la propiedad itfOrig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITFOrig() {
        return itfOrig;
    }

    /**
     * Define el valor de la propiedad itfOrig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITFOrig(String value) {
        this.itfOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad titularDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitularDest() {
        return titularDest;
    }

    /**
     * Define el valor de la propiedad titularDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitularDest(String value) {
        this.titularDest = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaDest() {
        return cuentaDest;
    }

    /**
     * Define el valor de la propiedad cuentaDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaDest(String value) {
        this.cuentaDest = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProductoDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProductoDest() {
        return tipoProductoDest;
    }

    /**
     * Define el valor de la propiedad tipoProductoDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProductoDest(String value) {
        this.tipoProductoDest = value;
    }

    /**
     * Obtiene el valor de la propiedad montoAbonado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoAbonado() {
        return montoAbonado;
    }

    /**
     * Define el valor de la propiedad montoAbonado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoAbonado(String value) {
        this.montoAbonado = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionTransf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionTransf() {
        return comisionTransf;
    }

    /**
     * Define el valor de la propiedad comisionTransf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionTransf(String value) {
        this.comisionTransf = value;
    }

    /**
     * Obtiene el valor de la propiedad cciBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCIBeneficiario() {
        return cciBeneficiario;
    }

    /**
     * Define el valor de la propiedad cciBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCIBeneficiario(String value) {
        this.cciBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad bancoDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBancoDestino() {
        return bancoDestino;
    }

    /**
     * Define el valor de la propiedad bancoDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBancoDestino(String value) {
        this.bancoDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad montoTransferido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoTransferido() {
        return montoTransferido;
    }

    /**
     * Define el valor de la propiedad montoTransferido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoTransferido(String value) {
        this.montoTransferido = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionInter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionInter() {
        return comisionInter;
    }

    /**
     * Define el valor de la propiedad comisionInter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionInter(String value) {
        this.comisionInter = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionCMAC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionCMAC() {
        return comisionCMAC;
    }

    /**
     * Define el valor de la propiedad comisionCMAC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionCMAC(String value) {
        this.comisionCMAC = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDePago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDePago() {
        return tipoDePago;
    }

    /**
     * Define el valor de la propiedad tipoDePago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDePago(String value) {
        this.tipoDePago = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCuota.
     * 
     */
    public int getNroCuota() {
        return nroCuota;
    }

    /**
     * Define el valor de la propiedad nroCuota.
     * 
     */
    public void setNroCuota(int value) {
        this.nroCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * Define el valor de la propiedad fechaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaPago(String value) {
        this.fechaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad montoPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoPagado() {
        return montoPagado;
    }

    /**
     * Define el valor de la propiedad montoPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoPagado(String value) {
        this.montoPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad capitalPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapitalPagado() {
        return capitalPagado;
    }

    /**
     * Define el valor de la propiedad capitalPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapitalPagado(String value) {
        this.capitalPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad interesPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteresPagado() {
        return interesPagado;
    }

    /**
     * Define el valor de la propiedad interesPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteresPagado(String value) {
        this.interesPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad moraPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoraPagado() {
        return moraPagado;
    }

    /**
     * Define el valor de la propiedad moraPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoraPagado(String value) {
        this.moraPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad gastoPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastoPagado() {
        return gastoPagado;
    }

    /**
     * Define el valor de la propiedad gastoPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastoPagado(String value) {
        this.gastoPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad gastoComisionPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastoComisionPagado() {
        return gastoComisionPagado;
    }

    /**
     * Define el valor de la propiedad gastoComisionPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastoComisionPagado(String value) {
        this.gastoComisionPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad gastoSegdePagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastoSegdePagado() {
        return gastoSegdePagado;
    }

    /**
     * Define el valor de la propiedad gastoSegdePagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastoSegdePagado(String value) {
        this.gastoSegdePagado = value;
    }

    /**
     * Obtiene el valor de la propiedad gastoPolizaIncendioPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastoPolizaIncendioPagado() {
        return gastoPolizaIncendioPagado;
    }

    /**
     * Define el valor de la propiedad gastoPolizaIncendioPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastoPolizaIncendioPagado(String value) {
        this.gastoPolizaIncendioPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad gastoPolizaVehiculoPagado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGastoPolizaVehiculoPagado() {
        return gastoPolizaVehiculoPagado;
    }

    /**
     * Define el valor de la propiedad gastoPolizaVehiculoPagado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGastoPolizaVehiculoPagado(String value) {
        this.gastoPolizaVehiculoPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad diasMora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiasMora() {
        return diasMora;
    }

    /**
     * Define el valor de la propiedad diasMora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasMora(String value) {
        this.diasMora = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoCap.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoCap() {
        return saldoCap;
    }

    /**
     * Define el valor de la propiedad saldoCap.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoCap(String value) {
        this.saldoCap = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionBI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionBI() {
        return comisionBI;
    }

    /**
     * Define el valor de la propiedad comisionBI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionBI(String value) {
        this.comisionBI = value;
    }

    /**
     * Obtiene el valor de la propiedad itfDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITFDest() {
        return itfDest;
    }

    /**
     * Define el valor de la propiedad itfDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITFDest(String value) {
        this.itfDest = value;
    }

}
