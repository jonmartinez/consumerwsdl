
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cuenta_orig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cci_dest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_tipo_cuenta_dest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_monto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xn_itf" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xn_moneda_dest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcIdentificacion",
    "xcCuentaOrig",
    "xcCciDest",
    "xnTipoCuentaDest",
    "xnMonto",
    "xnItf",
    "xnMonedaDest"
})
@XmlRootElement(name = "WS_TRANSFERIR_TO_OTROBANCO")
public class WSTRANSFERIRTOOTROBANCO {

    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;
    @XmlElement(name = "xc_cuenta_orig")
    protected String xcCuentaOrig;
    @XmlElement(name = "xc_cci_dest")
    protected String xcCciDest;
    @XmlElement(name = "xn_tipo_cuenta_dest")
    protected int xnTipoCuentaDest;
    @XmlElement(name = "xn_monto", required = true)
    protected BigDecimal xnMonto;
    @XmlElement(name = "xn_itf", required = true)
    protected BigDecimal xnItf;
    @XmlElement(name = "xn_moneda_dest")
    protected int xnMonedaDest;

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCuentaOrig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuentaOrig() {
        return xcCuentaOrig;
    }

    /**
     * Define el valor de la propiedad xcCuentaOrig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuentaOrig(String value) {
        this.xcCuentaOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCciDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCciDest() {
        return xcCciDest;
    }

    /**
     * Define el valor de la propiedad xcCciDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCciDest(String value) {
        this.xcCciDest = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipoCuentaDest.
     * 
     */
    public int getXnTipoCuentaDest() {
        return xnTipoCuentaDest;
    }

    /**
     * Define el valor de la propiedad xnTipoCuentaDest.
     * 
     */
    public void setXnTipoCuentaDest(int value) {
        this.xnTipoCuentaDest = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMonto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnMonto() {
        return xnMonto;
    }

    /**
     * Define el valor de la propiedad xnMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnMonto(BigDecimal value) {
        this.xnMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad xnItf.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnItf() {
        return xnItf;
    }

    /**
     * Define el valor de la propiedad xnItf.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnItf(BigDecimal value) {
        this.xnItf = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMonedaDest.
     * 
     */
    public int getXnMonedaDest() {
        return xnMonedaDest;
    }

    /**
     * Define el valor de la propiedad xnMonedaDest.
     * 
     */
    public void setXnMonedaDest(int value) {
        this.xnMonedaDest = value;
    }

}
