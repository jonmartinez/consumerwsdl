
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_DETALLE_CANCELAR_PIGNORATICIOResult" type="{http://cajamaynas.pe/}ResponsePignoOpeDetalleDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetdetallecancelarpignoraticioResult"
})
@XmlRootElement(name = "WS_GET_DETALLE_CANCELAR_PIGNORATICIOResponse")
public class WSGETDETALLECANCELARPIGNORATICIOResponse {

    @XmlElement(name = "WS_GET_DETALLE_CANCELAR_PIGNORATICIOResult")
    protected ResponsePignoOpeDetalleDTO wsgetdetallecancelarpignoraticioResult;

    /**
     * Obtiene el valor de la propiedad wsgetdetallecancelarpignoraticioResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePignoOpeDetalleDTO }
     *     
     */
    public ResponsePignoOpeDetalleDTO getWSGETDETALLECANCELARPIGNORATICIOResult() {
        return wsgetdetallecancelarpignoraticioResult;
    }

    /**
     * Define el valor de la propiedad wsgetdetallecancelarpignoraticioResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePignoOpeDetalleDTO }
     *     
     */
    public void setWSGETDETALLECANCELARPIGNORATICIOResult(ResponsePignoOpeDetalleDTO value) {
        this.wsgetdetallecancelarpignoraticioResult = value;
    }

}
