
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.pe.bupartech.wsrestapicajamaynasapp.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccountHeader_QNAME = new QName("http://cajamaynas.pe/", "AccountHeader");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.pe.bupartech.wsrestapicajamaynasapp.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSGETCOMISIONResponse }
     * 
     */
    public WSGETCOMISIONResponse createWSGETCOMISIONResponse() {
        return new WSGETCOMISIONResponse();
    }

    /**
     * Create an instance of {@link ResponseComisionDTO }
     * 
     */
    public ResponseComisionDTO createResponseComisionDTO() {
        return new ResponseComisionDTO();
    }

    /**
     * Create an instance of {@link WSPAGOCUOTACREDITOResponse }
     * 
     */
    public WSPAGOCUOTACREDITOResponse createWSPAGOCUOTACREDITOResponse() {
        return new WSPAGOCUOTACREDITOResponse();
    }

    /**
     * Create an instance of {@link ResponsePagoCuota }
     * 
     */
    public ResponsePagoCuota createResponsePagoCuota() {
        return new ResponsePagoCuota();
    }

    /**
     * Create an instance of {@link WSPAGOSERVICIOSResponse }
     * 
     */
    public WSPAGOSERVICIOSResponse createWSPAGOSERVICIOSResponse() {
        return new WSPAGOSERVICIOSResponse();
    }

    /**
     * Create an instance of {@link ResponsePagoServiciosDTO }
     * 
     */
    public ResponsePagoServiciosDTO createResponsePagoServiciosDTO() {
        return new ResponsePagoServiciosDTO();
    }

    /**
     * Create an instance of {@link WSSIMULAINTERESGANADO }
     * 
     */
    public WSSIMULAINTERESGANADO createWSSIMULAINTERESGANADO() {
        return new WSSIMULAINTERESGANADO();
    }

    /**
     * Create an instance of {@link WSVERIFICARTARJETAResponse }
     * 
     */
    public WSVERIFICARTARJETAResponse createWSVERIFICARTARJETAResponse() {
        return new WSVERIFICARTARJETAResponse();
    }

    /**
     * Create an instance of {@link ResponseResulOperacionDTO }
     * 
     */
    public ResponseResulOperacionDTO createResponseResulOperacionDTO() {
        return new ResponseResulOperacionDTO();
    }

    /**
     * Create an instance of {@link WSCONFIGURARLIMITEOPERACIONResponse }
     * 
     */
    public WSCONFIGURARLIMITEOPERACIONResponse createWSCONFIGURARLIMITEOPERACIONResponse() {
        return new WSCONFIGURARLIMITEOPERACIONResponse();
    }

    /**
     * Create an instance of {@link WSVALIDAROPERACIONResponse }
     * 
     */
    public WSVALIDAROPERACIONResponse createWSVALIDAROPERACIONResponse() {
        return new WSVALIDAROPERACIONResponse();
    }

    /**
     * Create an instance of {@link WSAUMENTOCAPITALPLAZOFIJO }
     * 
     */
    public WSAUMENTOCAPITALPLAZOFIJO createWSAUMENTOCAPITALPLAZOFIJO() {
        return new WSAUMENTOCAPITALPLAZOFIJO();
    }

    /**
     * Create an instance of {@link WSSIMULAINTERESGANADOResponse }
     * 
     */
    public WSSIMULAINTERESGANADOResponse createWSSIMULAINTERESGANADOResponse() {
        return new WSSIMULAINTERESGANADOResponse();
    }

    /**
     * Create an instance of {@link ResponseInteresGeneradoPFDTO }
     * 
     */
    public ResponseInteresGeneradoPFDTO createResponseInteresGeneradoPFDTO() {
        return new ResponseInteresGeneradoPFDTO();
    }

    /**
     * Create an instance of {@link WSGETITFResponse }
     * 
     */
    public WSGETITFResponse createWSGETITFResponse() {
        return new WSGETITFResponse();
    }

    /**
     * Create an instance of {@link ResponseItfDTO }
     * 
     */
    public ResponseItfDTO createResponseItfDTO() {
        return new ResponseItfDTO();
    }

    /**
     * Create an instance of {@link WSVERIFICARPERSONAResponse }
     * 
     */
    public WSVERIFICARPERSONAResponse createWSVERIFICARPERSONAResponse() {
        return new WSVERIFICARPERSONAResponse();
    }

    /**
     * Create an instance of {@link WSGETCOMISION }
     * 
     */
    public WSGETCOMISION createWSGETCOMISION() {
        return new WSGETCOMISION();
    }

    /**
     * Create an instance of {@link WSACTUALIZARCONFIGURACIONTARJETAResponse }
     * 
     */
    public WSACTUALIZARCONFIGURACIONTARJETAResponse createWSACTUALIZARCONFIGURACIONTARJETAResponse() {
        return new WSACTUALIZARCONFIGURACIONTARJETAResponse();
    }

    /**
     * Create an instance of {@link WSAUMENTOCAPITALPLAZOFIJOResponse }
     * 
     */
    public WSAUMENTOCAPITALPLAZOFIJOResponse createWSAUMENTOCAPITALPLAZOFIJOResponse() {
        return new WSAUMENTOCAPITALPLAZOFIJOResponse();
    }

    /**
     * Create an instance of {@link ResponseAumentoCapitalPlazoFijoDTO }
     * 
     */
    public ResponseAumentoCapitalPlazoFijoDTO createResponseAumentoCapitalPlazoFijoDTO() {
        return new ResponseAumentoCapitalPlazoFijoDTO();
    }

    /**
     * Create an instance of {@link WSTRANSFERIRAOTROSCUENTASResponse }
     * 
     */
    public WSTRANSFERIRAOTROSCUENTASResponse createWSTRANSFERIRAOTROSCUENTASResponse() {
        return new WSTRANSFERIRAOTROSCUENTASResponse();
    }

    /**
     * Create an instance of {@link ResponseTransferenciaDTO }
     * 
     */
    public ResponseTransferenciaDTO createResponseTransferenciaDTO() {
        return new ResponseTransferenciaDTO();
    }

    /**
     * Create an instance of {@link WSCONSULTARMISAHORROSMOV }
     * 
     */
    public WSCONSULTARMISAHORROSMOV createWSCONSULTARMISAHORROSMOV() {
        return new WSCONSULTARMISAHORROSMOV();
    }

    /**
     * Create an instance of {@link WSGETTIPOCAMBIOResponse }
     * 
     */
    public WSGETTIPOCAMBIOResponse createWSGETTIPOCAMBIOResponse() {
        return new WSGETTIPOCAMBIOResponse();
    }

    /**
     * Create an instance of {@link ResponseTipoCambioDTO }
     * 
     */
    public ResponseTipoCambioDTO createResponseTipoCambioDTO() {
        return new ResponseTipoCambioDTO();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAVERIFICACIONResponse }
     * 
     */
    public WSPAGOPLANILLAVERIFICACIONResponse createWSPAGOPLANILLAVERIFICACIONResponse() {
        return new WSPAGOPLANILLAVERIFICACIONResponse();
    }

    /**
     * Create an instance of {@link ResponsePlanillaDeposito }
     * 
     */
    public ResponsePlanillaDeposito createResponsePlanillaDeposito() {
        return new ResponsePlanillaDeposito();
    }

    /**
     * Create an instance of {@link WSGETKEYCARDVALIDOResponse }
     * 
     */
    public WSGETKEYCARDVALIDOResponse createWSGETKEYCARDVALIDOResponse() {
        return new WSGETKEYCARDVALIDOResponse();
    }

    /**
     * Create an instance of {@link WSAUMENTOCAPITALPLAZOFIJOOTROS }
     * 
     */
    public WSAUMENTOCAPITALPLAZOFIJOOTROS createWSAUMENTOCAPITALPLAZOFIJOOTROS() {
        return new WSAUMENTOCAPITALPLAZOFIJOOTROS();
    }

    /**
     * Create an instance of {@link WSGETDETALLERENOVARPIGNORATICIO }
     * 
     */
    public WSGETDETALLERENOVARPIGNORATICIO createWSGETDETALLERENOVARPIGNORATICIO() {
        return new WSGETDETALLERENOVARPIGNORATICIO();
    }

    /**
     * Create an instance of {@link WSGETESTADOSERVICIOS }
     * 
     */
    public WSGETESTADOSERVICIOS createWSGETESTADOSERVICIOS() {
        return new WSGETESTADOSERVICIOS();
    }

    /**
     * Create an instance of {@link WSGETDETALLECANCELARPIGNORATICIOResponse }
     * 
     */
    public WSGETDETALLECANCELARPIGNORATICIOResponse createWSGETDETALLECANCELARPIGNORATICIOResponse() {
        return new WSGETDETALLECANCELARPIGNORATICIOResponse();
    }

    /**
     * Create an instance of {@link ResponsePignoOpeDetalleDTO }
     * 
     */
    public ResponsePignoOpeDetalleDTO createResponsePignoOpeDetalleDTO() {
        return new ResponsePignoOpeDetalleDTO();
    }

    /**
     * Create an instance of {@link WSGETCONFIGURACIONLIMITEOPERACION }
     * 
     */
    public WSGETCONFIGURACIONLIMITEOPERACION createWSGETCONFIGURACIONLIMITEOPERACION() {
        return new WSGETCONFIGURACIONLIMITEOPERACION();
    }

    /**
     * Create an instance of {@link WSCONSULTASERVICIOS }
     * 
     */
    public WSCONSULTASERVICIOS createWSCONSULTASERVICIOS() {
        return new WSCONSULTASERVICIOS();
    }

    /**
     * Create an instance of {@link WSCONFIGURARALERTAOPERACION }
     * 
     */
    public WSCONFIGURARALERTAOPERACION createWSCONFIGURARALERTAOPERACION() {
        return new WSCONFIGURARALERTAOPERACION();
    }

    /**
     * Create an instance of {@link WSGETCONSTANTE }
     * 
     */
    public WSGETCONSTANTE createWSGETCONSTANTE() {
        return new WSGETCONSTANTE();
    }

    /**
     * Create an instance of {@link WSPAGOCUOTACREDITO }
     * 
     */
    public WSPAGOCUOTACREDITO createWSPAGOCUOTACREDITO() {
        return new WSPAGOCUOTACREDITO();
    }

    /**
     * Create an instance of {@link WSCONSULTASERVICIOSResponse }
     * 
     */
    public WSCONSULTASERVICIOSResponse createWSCONSULTASERVICIOSResponse() {
        return new WSCONSULTASERVICIOSResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseServiciosDTO }
     * 
     */
    public ArrayOfResponseServiciosDTO createArrayOfResponseServiciosDTO() {
        return new ArrayOfResponseServiciosDTO();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLACTSVERIFICACION }
     * 
     */
    public WSPAGOPLANILLACTSVERIFICACION createWSPAGOPLANILLACTSVERIFICACION() {
        return new WSPAGOPLANILLACTSVERIFICACION();
    }

    /**
     * Create an instance of {@link WSGETCONFIGURACIONLIMITEOPERACIONResponse }
     * 
     */
    public WSGETCONFIGURACIONLIMITEOPERACIONResponse createWSGETCONFIGURACIONLIMITEOPERACIONResponse() {
        return new WSGETCONFIGURACIONLIMITEOPERACIONResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRequestLimitesOperacionCab }
     * 
     */
    public ArrayOfRequestLimitesOperacionCab createArrayOfRequestLimitesOperacionCab() {
        return new ArrayOfRequestLimitesOperacionCab();
    }

    /**
     * Create an instance of {@link WSVERIFICARPERSONA }
     * 
     */
    public WSVERIFICARPERSONA createWSVERIFICARPERSONA() {
        return new WSVERIFICARPERSONA();
    }

    /**
     * Create an instance of {@link WSCONSULTARMISCUENTAS }
     * 
     */
    public WSCONSULTARMISCUENTAS createWSCONSULTARMISCUENTAS() {
        return new WSCONSULTARMISCUENTAS();
    }

    /**
     * Create an instance of {@link WSOBTIENETASAPFResponse }
     * 
     */
    public WSOBTIENETASAPFResponse createWSOBTIENETASAPFResponse() {
        return new WSOBTIENETASAPFResponse();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAPROVEEDORESPAGOResponse }
     * 
     */
    public WSPAGOPLANILLAPROVEEDORESPAGOResponse createWSPAGOPLANILLAPROVEEDORESPAGOResponse() {
        return new WSPAGOPLANILLAPROVEEDORESPAGOResponse();
    }

    /**
     * Create an instance of {@link ResponsePagoPlanillaDTO }
     * 
     */
    public ResponsePagoPlanillaDTO createResponsePagoPlanillaDTO() {
        return new ResponsePagoPlanillaDTO();
    }

    /**
     * Create an instance of {@link WSCONSULTARDETALLECUENTA }
     * 
     */
    public WSCONSULTARDETALLECUENTA createWSCONSULTARDETALLECUENTA() {
        return new WSCONSULTARDETALLECUENTA();
    }

    /**
     * Create an instance of {@link WSCONSULTARCONOGRAMA }
     * 
     */
    public WSCONSULTARCONOGRAMA createWSCONSULTARCONOGRAMA() {
        return new WSCONSULTARCONOGRAMA();
    }

    /**
     * Create an instance of {@link WSGETCONFIGURACIONALERTAOPERACION }
     * 
     */
    public WSGETCONFIGURACIONALERTAOPERACION createWSGETCONFIGURACIONALERTAOPERACION() {
        return new WSGETCONFIGURACIONALERTAOPERACION();
    }

    /**
     * Create an instance of {@link WSCONSULTARMISAHORROSMOVResponse }
     * 
     */
    public WSCONSULTARMISAHORROSMOVResponse createWSCONSULTARMISAHORROSMOVResponse() {
        return new WSCONSULTARMISAHORROSMOVResponse();
    }

    /**
     * Create an instance of {@link ResponseMisAhorros }
     * 
     */
    public ResponseMisAhorros createResponseMisAhorros() {
        return new ResponseMisAhorros();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAPROVEEDORESVERIFICACIONResponse }
     * 
     */
    public WSPAGOPLANILLAPROVEEDORESVERIFICACIONResponse createWSPAGOPLANILLAPROVEEDORESVERIFICACIONResponse() {
        return new WSPAGOPLANILLAPROVEEDORESVERIFICACIONResponse();
    }

    /**
     * Create an instance of {@link WSVERIFICARTARJETA }
     * 
     */
    public WSVERIFICARTARJETA createWSVERIFICARTARJETA() {
        return new WSVERIFICARTARJETA();
    }

    /**
     * Create an instance of {@link WSCONFIGURARLIMITEOPERACION }
     * 
     */
    public WSCONFIGURARLIMITEOPERACION createWSCONFIGURARLIMITEOPERACION() {
        return new WSCONFIGURARLIMITEOPERACION();
    }

    /**
     * Create an instance of {@link WSGETCIFRADO2 }
     * 
     */
    public WSGETCIFRADO2 createWSGETCIFRADO2() {
        return new WSGETCIFRADO2();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAVERIFICACION }
     * 
     */
    public WSPAGOPLANILLAVERIFICACION createWSPAGOPLANILLAVERIFICACION() {
        return new WSPAGOPLANILLAVERIFICACION();
    }

    /**
     * Create an instance of {@link WSRENOVARPIGNORATICIO }
     * 
     */
    public WSRENOVARPIGNORATICIO createWSRENOVARPIGNORATICIO() {
        return new WSRENOVARPIGNORATICIO();
    }

    /**
     * Create an instance of {@link WSPAGOSERVICIOS }
     * 
     */
    public WSPAGOSERVICIOS createWSPAGOSERVICIOS() {
        return new WSPAGOSERVICIOS();
    }

    /**
     * Create an instance of {@link WSRENOVARPIGNORATICIOResponse }
     * 
     */
    public WSRENOVARPIGNORATICIOResponse createWSRENOVARPIGNORATICIOResponse() {
        return new WSRENOVARPIGNORATICIOResponse();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLACTSPAGOResponse }
     * 
     */
    public WSPAGOPLANILLACTSPAGOResponse createWSPAGOPLANILLACTSPAGOResponse() {
        return new WSPAGOPLANILLACTSPAGOResponse();
    }

    /**
     * Create an instance of {@link WSTRANSFERIRTOOTROBANCO }
     * 
     */
    public WSTRANSFERIRTOOTROBANCO createWSTRANSFERIRTOOTROBANCO() {
        return new WSTRANSFERIRTOOTROBANCO();
    }

    /**
     * Create an instance of {@link WSGETCONSTANCIAOPERACION }
     * 
     */
    public WSGETCONSTANCIAOPERACION createWSGETCONSTANCIAOPERACION() {
        return new WSGETCONSTANCIAOPERACION();
    }

    /**
     * Create an instance of {@link WSVALIDARCUENTASESIONResponse }
     * 
     */
    public WSVALIDARCUENTASESIONResponse createWSVALIDARCUENTASESIONResponse() {
        return new WSVALIDARCUENTASESIONResponse();
    }

    /**
     * Create an instance of {@link WSGETDETALLECUOTAPENDIENTEResponse }
     * 
     */
    public WSGETDETALLECUOTAPENDIENTEResponse createWSGETDETALLECUOTAPENDIENTEResponse() {
        return new WSGETDETALLECUOTAPENDIENTEResponse();
    }

    /**
     * Create an instance of {@link ResponseDetalleCuotaPendienteDTO }
     * 
     */
    public ResponseDetalleCuotaPendienteDTO createResponseDetalleCuotaPendienteDTO() {
        return new ResponseDetalleCuotaPendienteDTO();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAPROVEEDORESVERIFICACION }
     * 
     */
    public WSPAGOPLANILLAPROVEEDORESVERIFICACION createWSPAGOPLANILLAPROVEEDORESVERIFICACION() {
        return new WSPAGOPLANILLAPROVEEDORESVERIFICACION();
    }

    /**
     * Create an instance of {@link WSTRANSFERIRCUENTASPROPIAS }
     * 
     */
    public WSTRANSFERIRCUENTASPROPIAS createWSTRANSFERIRCUENTASPROPIAS() {
        return new WSTRANSFERIRCUENTASPROPIAS();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLACTSVERIFICACIONResponse }
     * 
     */
    public WSPAGOPLANILLACTSVERIFICACIONResponse createWSPAGOPLANILLACTSVERIFICACIONResponse() {
        return new WSPAGOPLANILLACTSVERIFICACIONResponse();
    }

    /**
     * Create an instance of {@link ResponsePlanillaCTSDTO }
     * 
     */
    public ResponsePlanillaCTSDTO createResponsePlanillaCTSDTO() {
        return new ResponsePlanillaCTSDTO();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLACTSPAGO }
     * 
     */
    public WSPAGOPLANILLACTSPAGO createWSPAGOPLANILLACTSPAGO() {
        return new WSPAGOPLANILLACTSPAGO();
    }

    /**
     * Create an instance of {@link WSGETCONFIGURACIONALERTAOPERACIONResponse }
     * 
     */
    public WSGETCONFIGURACIONALERTAOPERACIONResponse createWSGETCONFIGURACIONALERTAOPERACIONResponse() {
        return new WSGETCONFIGURACIONALERTAOPERACIONResponse();
    }

    /**
     * Create an instance of {@link ResponseAlertaOperacion }
     * 
     */
    public ResponseAlertaOperacion createResponseAlertaOperacion() {
        return new ResponseAlertaOperacion();
    }

    /**
     * Create an instance of {@link WSCONSULTARCONOGRAMAResponse }
     * 
     */
    public WSCONSULTARCONOGRAMAResponse createWSCONSULTARCONOGRAMAResponse() {
        return new WSCONSULTARCONOGRAMAResponse();
    }

    /**
     * Create an instance of {@link ResponseCronograma }
     * 
     */
    public ResponseCronograma createResponseCronograma() {
        return new ResponseCronograma();
    }

    /**
     * Create an instance of {@link WSGETBANCOXCCIResponse }
     * 
     */
    public WSGETBANCOXCCIResponse createWSGETBANCOXCCIResponse() {
        return new WSGETBANCOXCCIResponse();
    }

    /**
     * Create an instance of {@link ResponseBancoXCciDTO }
     * 
     */
    public ResponseBancoXCciDTO createResponseBancoXCciDTO() {
        return new ResponseBancoXCciDTO();
    }

    /**
     * Create an instance of {@link WSGETBANCOXCCI }
     * 
     */
    public WSGETBANCOXCCI createWSGETBANCOXCCI() {
        return new WSGETBANCOXCCI();
    }

    /**
     * Create an instance of {@link WSGETITF }
     * 
     */
    public WSGETITF createWSGETITF() {
        return new WSGETITF();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAPAGOResponse }
     * 
     */
    public WSPAGOPLANILLAPAGOResponse createWSPAGOPLANILLAPAGOResponse() {
        return new WSPAGOPLANILLAPAGOResponse();
    }

    /**
     * Create an instance of {@link WSGETESTADOSERVICIOSResponse }
     * 
     */
    public WSGETESTADOSERVICIOSResponse createWSGETESTADOSERVICIOSResponse() {
        return new WSGETESTADOSERVICIOSResponse();
    }

    /**
     * Create an instance of {@link WSVALIDARCUENTASESION }
     * 
     */
    public WSVALIDARCUENTASESION createWSVALIDARCUENTASESION() {
        return new WSVALIDARCUENTASESION();
    }

    /**
     * Create an instance of {@link WSCONSULTARCONFIGURACIONTARJETAResponse }
     * 
     */
    public WSCONSULTARCONFIGURACIONTARJETAResponse createWSCONSULTARCONFIGURACIONTARJETAResponse() {
        return new WSCONSULTARCONFIGURACIONTARJETAResponse();
    }

    /**
     * Create an instance of {@link ResponseConsultarConfiguracionTarjetaDTO }
     * 
     */
    public ResponseConsultarConfiguracionTarjetaDTO createResponseConsultarConfiguracionTarjetaDTO() {
        return new ResponseConsultarConfiguracionTarjetaDTO();
    }

    /**
     * Create an instance of {@link WSAPERTURAPLAZOFIJOResponse }
     * 
     */
    public WSAPERTURAPLAZOFIJOResponse createWSAPERTURAPLAZOFIJOResponse() {
        return new WSAPERTURAPLAZOFIJOResponse();
    }

    /**
     * Create an instance of {@link ResponseAperturaPlazoFijoDTO }
     * 
     */
    public ResponseAperturaPlazoFijoDTO createResponseAperturaPlazoFijoDTO() {
        return new ResponseAperturaPlazoFijoDTO();
    }

    /**
     * Create an instance of {@link WSCONSULTADEUDASERVICIOSResponse }
     * 
     */
    public WSCONSULTADEUDASERVICIOSResponse createWSCONSULTADEUDASERVICIOSResponse() {
        return new WSCONSULTADEUDASERVICIOSResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseDeudaPagoServiciosDTO }
     * 
     */
    public ArrayOfResponseDeudaPagoServiciosDTO createArrayOfResponseDeudaPagoServiciosDTO() {
        return new ArrayOfResponseDeudaPagoServiciosDTO();
    }

    /**
     * Create an instance of {@link WSPAGOCUOTACREDITOOTROSResponse }
     * 
     */
    public WSPAGOCUOTACREDITOOTROSResponse createWSPAGOCUOTACREDITOOTROSResponse() {
        return new WSPAGOCUOTACREDITOOTROSResponse();
    }

    /**
     * Create an instance of {@link ConnexionUsingCertificado }
     * 
     */
    public ConnexionUsingCertificado createConnexionUsingCertificado() {
        return new ConnexionUsingCertificado();
    }

    /**
     * Create an instance of {@link WSCONSULTARPERSONAResponse }
     * 
     */
    public WSCONSULTARPERSONAResponse createWSCONSULTARPERSONAResponse() {
        return new WSCONSULTARPERSONAResponse();
    }

    /**
     * Create an instance of {@link ResponseConsultarPersonaDTO }
     * 
     */
    public ResponseConsultarPersonaDTO createResponseConsultarPersonaDTO() {
        return new ResponseConsultarPersonaDTO();
    }

    /**
     * Create an instance of {@link WSGETKEYCARDVALIDO }
     * 
     */
    public WSGETKEYCARDVALIDO createWSGETKEYCARDVALIDO() {
        return new WSGETKEYCARDVALIDO();
    }

    /**
     * Create an instance of {@link WSALERTAPREVENSIONFRAUDE }
     * 
     */
    public WSALERTAPREVENSIONFRAUDE createWSALERTAPREVENSIONFRAUDE() {
        return new WSALERTAPREVENSIONFRAUDE();
    }

    /**
     * Create an instance of {@link WSTRANSFERIRTOOTROBANCOResponse }
     * 
     */
    public WSTRANSFERIRTOOTROBANCOResponse createWSTRANSFERIRTOOTROBANCOResponse() {
        return new WSTRANSFERIRTOOTROBANCOResponse();
    }

    /**
     * Create an instance of {@link WSCONSULTADEUDASERVICIOS }
     * 
     */
    public WSCONSULTADEUDASERVICIOS createWSCONSULTADEUDASERVICIOS() {
        return new WSCONSULTADEUDASERVICIOS();
    }

    /**
     * Create an instance of {@link ConnexionUsingCertificadoResponse }
     * 
     */
    public ConnexionUsingCertificadoResponse createConnexionUsingCertificadoResponse() {
        return new ConnexionUsingCertificadoResponse();
    }

    /**
     * Create an instance of {@link WSTRANSFERIRAOTROSCUENTAS }
     * 
     */
    public WSTRANSFERIRAOTROSCUENTAS createWSTRANSFERIRAOTROSCUENTAS() {
        return new WSTRANSFERIRAOTROSCUENTAS();
    }

    /**
     * Create an instance of {@link WSOBTIENETASAPF }
     * 
     */
    public WSOBTIENETASAPF createWSOBTIENETASAPF() {
        return new WSOBTIENETASAPF();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAPAGO }
     * 
     */
    public WSPAGOPLANILLAPAGO createWSPAGOPLANILLAPAGO() {
        return new WSPAGOPLANILLAPAGO();
    }

    /**
     * Create an instance of {@link WSCONSULTARMISCUENTASResponse }
     * 
     */
    public WSCONSULTARMISCUENTASResponse createWSCONSULTARMISCUENTASResponse() {
        return new WSCONSULTARMISCUENTASResponse();
    }

    /**
     * Create an instance of {@link ResponseMisCuentas }
     * 
     */
    public ResponseMisCuentas createResponseMisCuentas() {
        return new ResponseMisCuentas();
    }

    /**
     * Create an instance of {@link WSPAGOPLANILLAPROVEEDORESPAGO }
     * 
     */
    public WSPAGOPLANILLAPROVEEDORESPAGO createWSPAGOPLANILLAPROVEEDORESPAGO() {
        return new WSPAGOPLANILLAPROVEEDORESPAGO();
    }

    /**
     * Create an instance of {@link WSCANCELARPIGNORATICIOResponse }
     * 
     */
    public WSCANCELARPIGNORATICIOResponse createWSCANCELARPIGNORATICIOResponse() {
        return new WSCANCELARPIGNORATICIOResponse();
    }

    /**
     * Create an instance of {@link ResponseCancelacionPignoDTO }
     * 
     */
    public ResponseCancelacionPignoDTO createResponseCancelacionPignoDTO() {
        return new ResponseCancelacionPignoDTO();
    }

    /**
     * Create an instance of {@link WSGETCONSTANCIAOPERACIONResponse }
     * 
     */
    public WSGETCONSTANCIAOPERACIONResponse createWSGETCONSTANCIAOPERACIONResponse() {
        return new WSGETCONSTANCIAOPERACIONResponse();
    }

    /**
     * Create an instance of {@link ResponseConstanciaOperacion }
     * 
     */
    public ResponseConstanciaOperacion createResponseConstanciaOperacion() {
        return new ResponseConstanciaOperacion();
    }

    /**
     * Create an instance of {@link WSCONSULTARPERSONA }
     * 
     */
    public WSCONSULTARPERSONA createWSCONSULTARPERSONA() {
        return new WSCONSULTARPERSONA();
    }

    /**
     * Create an instance of {@link WSCONSULTARMISAHORROSMOVPENDIENTES }
     * 
     */
    public WSCONSULTARMISAHORROSMOVPENDIENTES createWSCONSULTARMISAHORROSMOVPENDIENTES() {
        return new WSCONSULTARMISAHORROSMOVPENDIENTES();
    }

    /**
     * Create an instance of {@link WSPAGOCUOTACREDITOOTROS }
     * 
     */
    public WSPAGOCUOTACREDITOOTROS createWSPAGOCUOTACREDITOOTROS() {
        return new WSPAGOCUOTACREDITOOTROS();
    }

    /**
     * Create an instance of {@link WSAPERTURAPLAZOFIJO }
     * 
     */
    public WSAPERTURAPLAZOFIJO createWSAPERTURAPLAZOFIJO() {
        return new WSAPERTURAPLAZOFIJO();
    }

    /**
     * Create an instance of {@link WSAUMENTOCAPITALPLAZOFIJOOTROSResponse }
     * 
     */
    public WSAUMENTOCAPITALPLAZOFIJOOTROSResponse createWSAUMENTOCAPITALPLAZOFIJOOTROSResponse() {
        return new WSAUMENTOCAPITALPLAZOFIJOOTROSResponse();
    }

    /**
     * Create an instance of {@link WSAPERTURAGIROResponse }
     * 
     */
    public WSAPERTURAGIROResponse createWSAPERTURAGIROResponse() {
        return new WSAPERTURAGIROResponse();
    }

    /**
     * Create an instance of {@link AccountHeader }
     * 
     */
    public AccountHeader createAccountHeader() {
        return new AccountHeader();
    }

    /**
     * Create an instance of {@link WSCONSULTARMISAHORROSMOVPENDIENTESResponse }
     * 
     */
    public WSCONSULTARMISAHORROSMOVPENDIENTESResponse createWSCONSULTARMISAHORROSMOVPENDIENTESResponse() {
        return new WSCONSULTARMISAHORROSMOVPENDIENTESResponse();
    }

    /**
     * Create an instance of {@link WSVALIDAROPERACION }
     * 
     */
    public WSVALIDAROPERACION createWSVALIDAROPERACION() {
        return new WSVALIDAROPERACION();
    }

    /**
     * Create an instance of {@link WSACTUALIZARDATOSPERSONA }
     * 
     */
    public WSACTUALIZARDATOSPERSONA createWSACTUALIZARDATOSPERSONA() {
        return new WSACTUALIZARDATOSPERSONA();
    }

    /**
     * Create an instance of {@link WSALERTAPREVENSIONFRAUDEResponse }
     * 
     */
    public WSALERTAPREVENSIONFRAUDEResponse createWSALERTAPREVENSIONFRAUDEResponse() {
        return new WSALERTAPREVENSIONFRAUDEResponse();
    }

    /**
     * Create an instance of {@link ResponseAlertaHB }
     * 
     */
    public ResponseAlertaHB createResponseAlertaHB() {
        return new ResponseAlertaHB();
    }

    /**
     * Create an instance of {@link WSGETCIFRADO2Response }
     * 
     */
    public WSGETCIFRADO2Response createWSGETCIFRADO2Response() {
        return new WSGETCIFRADO2Response();
    }

    /**
     * Create an instance of {@link WSCANCELARPIGNORATICIO }
     * 
     */
    public WSCANCELARPIGNORATICIO createWSCANCELARPIGNORATICIO() {
        return new WSCANCELARPIGNORATICIO();
    }

    /**
     * Create an instance of {@link WSGETCONSTANTEResponse }
     * 
     */
    public WSGETCONSTANTEResponse createWSGETCONSTANTEResponse() {
        return new WSGETCONSTANTEResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseConstanteDTO }
     * 
     */
    public ArrayOfResponseConstanteDTO createArrayOfResponseConstanteDTO() {
        return new ArrayOfResponseConstanteDTO();
    }

    /**
     * Create an instance of {@link WSCONFIGURARALERTAOPERACIONResponse }
     * 
     */
    public WSCONFIGURARALERTAOPERACIONResponse createWSCONFIGURARALERTAOPERACIONResponse() {
        return new WSCONFIGURARALERTAOPERACIONResponse();
    }

    /**
     * Create an instance of {@link WSCONSULTARRESPTSSEGURIDADALE }
     * 
     */
    public WSCONSULTARRESPTSSEGURIDADALE createWSCONSULTARRESPTSSEGURIDADALE() {
        return new WSCONSULTARRESPTSSEGURIDADALE();
    }

    /**
     * Create an instance of {@link WSCONSULTARRESPTSSEGURIDADALEResponse }
     * 
     */
    public WSCONSULTARRESPTSSEGURIDADALEResponse createWSCONSULTARRESPTSSEGURIDADALEResponse() {
        return new WSCONSULTARRESPTSSEGURIDADALEResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseGetRespuestaSeguridadDTO }
     * 
     */
    public ArrayOfResponseGetRespuestaSeguridadDTO createArrayOfResponseGetRespuestaSeguridadDTO() {
        return new ArrayOfResponseGetRespuestaSeguridadDTO();
    }

    /**
     * Create an instance of {@link WSTRANSFERIRCUENTASPROPIASResponse }
     * 
     */
    public WSTRANSFERIRCUENTASPROPIASResponse createWSTRANSFERIRCUENTASPROPIASResponse() {
        return new WSTRANSFERIRCUENTASPROPIASResponse();
    }

    /**
     * Create an instance of {@link WSGETDETALLERENOVARPIGNORATICIOResponse }
     * 
     */
    public WSGETDETALLERENOVARPIGNORATICIOResponse createWSGETDETALLERENOVARPIGNORATICIOResponse() {
        return new WSGETDETALLERENOVARPIGNORATICIOResponse();
    }

    /**
     * Create an instance of {@link WSACTUALIZARCONFIGURACIONTARJETA }
     * 
     */
    public WSACTUALIZARCONFIGURACIONTARJETA createWSACTUALIZARCONFIGURACIONTARJETA() {
        return new WSACTUALIZARCONFIGURACIONTARJETA();
    }

    /**
     * Create an instance of {@link WSGETDETALLECANCELARPIGNORATICIO }
     * 
     */
    public WSGETDETALLECANCELARPIGNORATICIO createWSGETDETALLECANCELARPIGNORATICIO() {
        return new WSGETDETALLECANCELARPIGNORATICIO();
    }

    /**
     * Create an instance of {@link WSCONSULTARCONFIGURACIONTARJETA }
     * 
     */
    public WSCONSULTARCONFIGURACIONTARJETA createWSCONSULTARCONFIGURACIONTARJETA() {
        return new WSCONSULTARCONFIGURACIONTARJETA();
    }

    /**
     * Create an instance of {@link WSACTUALIZARDATOSPERSONAResponse }
     * 
     */
    public WSACTUALIZARDATOSPERSONAResponse createWSACTUALIZARDATOSPERSONAResponse() {
        return new WSACTUALIZARDATOSPERSONAResponse();
    }

    /**
     * Create an instance of {@link WSAPERTURAGIRO }
     * 
     */
    public WSAPERTURAGIRO createWSAPERTURAGIRO() {
        return new WSAPERTURAGIRO();
    }

    /**
     * Create an instance of {@link WSGETTIPOCAMBIO }
     * 
     */
    public WSGETTIPOCAMBIO createWSGETTIPOCAMBIO() {
        return new WSGETTIPOCAMBIO();
    }

    /**
     * Create an instance of {@link WSCONSULTARDETALLECUENTAResponse }
     * 
     */
    public WSCONSULTARDETALLECUENTAResponse createWSCONSULTARDETALLECUENTAResponse() {
        return new WSCONSULTARDETALLECUENTAResponse();
    }

    /**
     * Create an instance of {@link ResponseDetalleCuenta }
     * 
     */
    public ResponseDetalleCuenta createResponseDetalleCuenta() {
        return new ResponseDetalleCuenta();
    }

    /**
     * Create an instance of {@link WSGETDETALLECUOTAPENDIENTE }
     * 
     */
    public WSGETDETALLECUOTAPENDIENTE createWSGETDETALLECUOTAPENDIENTE() {
        return new WSGETDETALLECUOTAPENDIENTE();
    }

    /**
     * Create an instance of {@link ResponsePlanillaDepositoCab }
     * 
     */
    public ResponsePlanillaDepositoCab createResponsePlanillaDepositoCab() {
        return new ResponsePlanillaDepositoCab();
    }

    /**
     * Create an instance of {@link ArrayOfResponsePlanillaDepositoDet }
     * 
     */
    public ArrayOfResponsePlanillaDepositoDet createArrayOfResponsePlanillaDepositoDet() {
        return new ArrayOfResponsePlanillaDepositoDet();
    }

    /**
     * Create an instance of {@link ResponseDetalleCuentaDTO }
     * 
     */
    public ResponseDetalleCuentaDTO createResponseDetalleCuentaDTO() {
        return new ResponseDetalleCuentaDTO();
    }

    /**
     * Create an instance of {@link ResponseGetRespuestaSeguridadDTO }
     * 
     */
    public ResponseGetRespuestaSeguridadDTO createResponseGetRespuestaSeguridadDTO() {
        return new ResponseGetRespuestaSeguridadDTO();
    }

    /**
     * Create an instance of {@link ArrayOfResponseDetalleCuentaDTO }
     * 
     */
    public ArrayOfResponseDetalleCuentaDTO createArrayOfResponseDetalleCuentaDTO() {
        return new ArrayOfResponseDetalleCuentaDTO();
    }

    /**
     * Create an instance of {@link ResponseConstanteDTO }
     * 
     */
    public ResponseConstanteDTO createResponseConstanteDTO() {
        return new ResponseConstanteDTO();
    }

    /**
     * Create an instance of {@link ResponsePlanillaCtsDetDTO }
     * 
     */
    public ResponsePlanillaCtsDetDTO createResponsePlanillaCtsDetDTO() {
        return new ResponsePlanillaCtsDetDTO();
    }

    /**
     * Create an instance of {@link ArrayOfResponseCronogramaDTO }
     * 
     */
    public ArrayOfResponseCronogramaDTO createArrayOfResponseCronogramaDTO() {
        return new ArrayOfResponseCronogramaDTO();
    }

    /**
     * Create an instance of {@link RequestPlanillaDepositoDetalle }
     * 
     */
    public RequestPlanillaDepositoDetalle createRequestPlanillaDepositoDetalle() {
        return new RequestPlanillaDepositoDetalle();
    }

    /**
     * Create an instance of {@link ArrayOfResponsePlanillaCtsDetDTO }
     * 
     */
    public ArrayOfResponsePlanillaCtsDetDTO createArrayOfResponsePlanillaCtsDetDTO() {
        return new ArrayOfResponsePlanillaCtsDetDTO();
    }

    /**
     * Create an instance of {@link RequestLimitesOperacionCab }
     * 
     */
    public RequestLimitesOperacionCab createRequestLimitesOperacionCab() {
        return new RequestLimitesOperacionCab();
    }

    /**
     * Create an instance of {@link ResponsePlanillaCtsCabDTO }
     * 
     */
    public ResponsePlanillaCtsCabDTO createResponsePlanillaCtsCabDTO() {
        return new ResponsePlanillaCtsCabDTO();
    }

    /**
     * Create an instance of {@link ArrayOfResponseAlertaOperacionDetalle }
     * 
     */
    public ArrayOfResponseAlertaOperacionDetalle createArrayOfResponseAlertaOperacionDetalle() {
        return new ArrayOfResponseAlertaOperacionDetalle();
    }

    /**
     * Create an instance of {@link ResponseMisCuentasDTO }
     * 
     */
    public ResponseMisCuentasDTO createResponseMisCuentasDTO() {
        return new ResponseMisCuentasDTO();
    }

    /**
     * Create an instance of {@link ArrayOfRequestPlanillaDepositoDetalle }
     * 
     */
    public ArrayOfRequestPlanillaDepositoDetalle createArrayOfRequestPlanillaDepositoDetalle() {
        return new ArrayOfRequestPlanillaDepositoDetalle();
    }

    /**
     * Create an instance of {@link ResponseMisAhorrosDTO }
     * 
     */
    public ResponseMisAhorrosDTO createResponseMisAhorrosDTO() {
        return new ResponseMisAhorrosDTO();
    }

    /**
     * Create an instance of {@link ArrayOfRequestLimitesOperacionDet }
     * 
     */
    public ArrayOfRequestLimitesOperacionDet createArrayOfRequestLimitesOperacionDet() {
        return new ArrayOfRequestLimitesOperacionDet();
    }

    /**
     * Create an instance of {@link ArrayOfResponseMisCuentasDTO }
     * 
     */
    public ArrayOfResponseMisCuentasDTO createArrayOfResponseMisCuentasDTO() {
        return new ArrayOfResponseMisCuentasDTO();
    }

    /**
     * Create an instance of {@link ResponseDeudaPagoServiciosDTO }
     * 
     */
    public ResponseDeudaPagoServiciosDTO createResponseDeudaPagoServiciosDTO() {
        return new ResponseDeudaPagoServiciosDTO();
    }

    /**
     * Create an instance of {@link RequestLimitesOperacionDet }
     * 
     */
    public RequestLimitesOperacionDet createRequestLimitesOperacionDet() {
        return new RequestLimitesOperacionDet();
    }

    /**
     * Create an instance of {@link ResponseCronogramaDTO }
     * 
     */
    public ResponseCronogramaDTO createResponseCronogramaDTO() {
        return new ResponseCronogramaDTO();
    }

    /**
     * Create an instance of {@link ResponsePlanillaDepositoDet }
     * 
     */
    public ResponsePlanillaDepositoDet createResponsePlanillaDepositoDet() {
        return new ResponsePlanillaDepositoDet();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link ResponseServiciosDTO }
     * 
     */
    public ResponseServiciosDTO createResponseServiciosDTO() {
        return new ResponseServiciosDTO();
    }

    /**
     * Create an instance of {@link ResponseAlertaOperacionDetalle }
     * 
     */
    public ResponseAlertaOperacionDetalle createResponseAlertaOperacionDetalle() {
        return new ResponseAlertaOperacionDetalle();
    }

    /**
     * Create an instance of {@link ArrayOfResponseMisAhorrosDTO }
     * 
     */
    public ArrayOfResponseMisAhorrosDTO createArrayOfResponseMisAhorrosDTO() {
        return new ArrayOfResponseMisAhorrosDTO();
    }

    /**
     * Create an instance of {@link ResponseDetalleCuotaPendiente }
     * 
     */
    public ResponseDetalleCuotaPendiente createResponseDetalleCuotaPendiente() {
        return new ResponseDetalleCuotaPendiente();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cajamaynas.pe/", name = "AccountHeader")
    public JAXBElement<AccountHeader> createAccountHeader(AccountHeader value) {
        return new JAXBElement<AccountHeader>(_AccountHeader_QNAME, AccountHeader.class, null, value);
    }

}
