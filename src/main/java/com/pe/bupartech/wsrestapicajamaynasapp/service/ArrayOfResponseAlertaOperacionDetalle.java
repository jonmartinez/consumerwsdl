
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfResponseAlertaOperacionDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfResponseAlertaOperacionDetalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseAlertaOperacionDetalle" type="{http://cajamaynas.pe/}ResponseAlertaOperacionDetalle" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfResponseAlertaOperacionDetalle", propOrder = {
    "responseAlertaOperacionDetalle"
})
public class ArrayOfResponseAlertaOperacionDetalle {

    @XmlElement(name = "ResponseAlertaOperacionDetalle", nillable = true)
    protected List<ResponseAlertaOperacionDetalle> responseAlertaOperacionDetalle;

    /**
     * Gets the value of the responseAlertaOperacionDetalle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responseAlertaOperacionDetalle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponseAlertaOperacionDetalle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseAlertaOperacionDetalle }
     * 
     * 
     */
    public List<ResponseAlertaOperacionDetalle> getResponseAlertaOperacionDetalle() {
        if (responseAlertaOperacionDetalle == null) {
            responseAlertaOperacionDetalle = new ArrayList<ResponseAlertaOperacionDetalle>();
        }
        return this.responseAlertaOperacionDetalle;
    }

}
