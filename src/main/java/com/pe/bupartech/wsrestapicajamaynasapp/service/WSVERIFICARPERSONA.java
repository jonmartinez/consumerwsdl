
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xn_tipoDOI" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_numdoi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xnTipoDOI",
    "xcNumdoi"
})
@XmlRootElement(name = "WS_VERIFICAR_PERSONA")
public class WSVERIFICARPERSONA {

    @XmlElement(name = "xn_tipoDOI")
    protected int xnTipoDOI;
    @XmlElement(name = "xc_numdoi")
    protected String xcNumdoi;

    /**
     * Obtiene el valor de la propiedad xnTipoDOI.
     * 
     */
    public int getXnTipoDOI() {
        return xnTipoDOI;
    }

    /**
     * Define el valor de la propiedad xnTipoDOI.
     * 
     */
    public void setXnTipoDOI(int value) {
        this.xnTipoDOI = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNumdoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNumdoi() {
        return xcNumdoi;
    }

    /**
     * Define el valor de la propiedad xcNumdoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNumdoi(String value) {
        this.xcNumdoi = value;
    }

}
