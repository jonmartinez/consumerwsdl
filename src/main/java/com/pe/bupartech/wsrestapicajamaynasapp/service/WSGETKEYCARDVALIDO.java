
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_pin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_tarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcPin",
    "xcTarjeta"
})
@XmlRootElement(name = "WS_GET_KEYCARD_VALIDO")
public class WSGETKEYCARDVALIDO {

    @XmlElement(name = "xc_pin")
    protected String xcPin;
    @XmlElement(name = "xc_tarjeta")
    protected String xcTarjeta;

    /**
     * Obtiene el valor de la propiedad xcPin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcPin() {
        return xcPin;
    }

    /**
     * Define el valor de la propiedad xcPin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcPin(String value) {
        this.xcPin = value;
    }

    /**
     * Obtiene el valor de la propiedad xcTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcTarjeta() {
        return xcTarjeta;
    }

    /**
     * Define el valor de la propiedad xcTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcTarjeta(String value) {
        this.xcTarjeta = value;
    }

}
