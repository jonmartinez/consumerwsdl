
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseAlertaOperacion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseAlertaOperacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstDetalle" type="{http://cajamaynas.pe/}ArrayOfResponseAlertaOperacionDetalle" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseAlertaOperacion", propOrder = {
    "lstDetalle"
})
public class ResponseAlertaOperacion {

    protected ArrayOfResponseAlertaOperacionDetalle lstDetalle;

    /**
     * Obtiene el valor de la propiedad lstDetalle.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResponseAlertaOperacionDetalle }
     *     
     */
    public ArrayOfResponseAlertaOperacionDetalle getLstDetalle() {
        return lstDetalle;
    }

    /**
     * Define el valor de la propiedad lstDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResponseAlertaOperacionDetalle }
     *     
     */
    public void setLstDetalle(ArrayOfResponseAlertaOperacionDetalle value) {
        this.lstDetalle = value;
    }

}
