
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_COMISIONResult" type="{http://cajamaynas.pe/}ResponseComisionDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetcomisionResult"
})
@XmlRootElement(name = "WS_GET_COMISIONResponse")
public class WSGETCOMISIONResponse {

    @XmlElement(name = "WS_GET_COMISIONResult")
    protected ResponseComisionDTO wsgetcomisionResult;

    /**
     * Obtiene el valor de la propiedad wsgetcomisionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseComisionDTO }
     *     
     */
    public ResponseComisionDTO getWSGETCOMISIONResult() {
        return wsgetcomisionResult;
    }

    /**
     * Define el valor de la propiedad wsgetcomisionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseComisionDTO }
     *     
     */
    public void setWSGETCOMISIONResult(ResponseComisionDTO value) {
        this.wsgetcomisionResult = value;
    }

}
