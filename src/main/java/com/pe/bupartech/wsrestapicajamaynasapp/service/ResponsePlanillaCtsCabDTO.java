
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponsePlanillaCtsCabDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponsePlanillaCtsCabDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cMontoSoles" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cMontoDolares" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nNumPagosSoles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nNumPagosDolares" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cITF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cComision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cPersCodTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstDetalle" type="{http://cajamaynas.pe/}ArrayOfResponsePlanillaCtsDetDTO" minOccurs="0"/>
 *         &lt;element name="cDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsePlanillaCtsCabDTO", propOrder = {
    "cMontoSoles",
    "cMontoDolares",
    "nNumPagosSoles",
    "nNumPagosDolares",
    "cCuenta",
    "citf",
    "cComision",
    "cPersCodTitular",
    "lstDetalle",
    "cDescripcion"
})
public class ResponsePlanillaCtsCabDTO {

    protected String cMontoSoles;
    protected String cMontoDolares;
    protected int nNumPagosSoles;
    protected int nNumPagosDolares;
    protected String cCuenta;
    @XmlElement(name = "cITF")
    protected String citf;
    protected String cComision;
    protected String cPersCodTitular;
    protected ArrayOfResponsePlanillaCtsDetDTO lstDetalle;
    protected String cDescripcion;

    /**
     * Obtiene el valor de la propiedad cMontoSoles.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMontoSoles() {
        return cMontoSoles;
    }

    /**
     * Define el valor de la propiedad cMontoSoles.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMontoSoles(String value) {
        this.cMontoSoles = value;
    }

    /**
     * Obtiene el valor de la propiedad cMontoDolares.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMontoDolares() {
        return cMontoDolares;
    }

    /**
     * Define el valor de la propiedad cMontoDolares.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMontoDolares(String value) {
        this.cMontoDolares = value;
    }

    /**
     * Obtiene el valor de la propiedad nNumPagosSoles.
     * 
     */
    public int getNNumPagosSoles() {
        return nNumPagosSoles;
    }

    /**
     * Define el valor de la propiedad nNumPagosSoles.
     * 
     */
    public void setNNumPagosSoles(int value) {
        this.nNumPagosSoles = value;
    }

    /**
     * Obtiene el valor de la propiedad nNumPagosDolares.
     * 
     */
    public int getNNumPagosDolares() {
        return nNumPagosDolares;
    }

    /**
     * Define el valor de la propiedad nNumPagosDolares.
     * 
     */
    public void setNNumPagosDolares(int value) {
        this.nNumPagosDolares = value;
    }

    /**
     * Obtiene el valor de la propiedad cCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCuenta() {
        return cCuenta;
    }

    /**
     * Define el valor de la propiedad cCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCuenta(String value) {
        this.cCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad citf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCITF() {
        return citf;
    }

    /**
     * Define el valor de la propiedad citf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCITF(String value) {
        this.citf = value;
    }

    /**
     * Obtiene el valor de la propiedad cComision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCComision() {
        return cComision;
    }

    /**
     * Define el valor de la propiedad cComision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCComision(String value) {
        this.cComision = value;
    }

    /**
     * Obtiene el valor de la propiedad cPersCodTitular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPersCodTitular() {
        return cPersCodTitular;
    }

    /**
     * Define el valor de la propiedad cPersCodTitular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPersCodTitular(String value) {
        this.cPersCodTitular = value;
    }

    /**
     * Obtiene el valor de la propiedad lstDetalle.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResponsePlanillaCtsDetDTO }
     *     
     */
    public ArrayOfResponsePlanillaCtsDetDTO getLstDetalle() {
        return lstDetalle;
    }

    /**
     * Define el valor de la propiedad lstDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResponsePlanillaCtsDetDTO }
     *     
     */
    public void setLstDetalle(ArrayOfResponsePlanillaCtsDetDTO value) {
        this.lstDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad cDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDescripcion() {
        return cDescripcion;
    }

    /**
     * Define el valor de la propiedad cDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDescripcion(String value) {
        this.cDescripcion = value;
    }

}
