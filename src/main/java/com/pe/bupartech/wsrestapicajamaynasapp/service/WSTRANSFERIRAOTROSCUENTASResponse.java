
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_TRANSFERIR_AOTROSCUENTASResult" type="{http://cajamaynas.pe/}ResponseTransferenciaDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wstransferiraotroscuentasResult"
})
@XmlRootElement(name = "WS_TRANSFERIR_AOTROSCUENTASResponse")
public class WSTRANSFERIRAOTROSCUENTASResponse {

    @XmlElement(name = "WS_TRANSFERIR_AOTROSCUENTASResult")
    protected ResponseTransferenciaDTO wstransferiraotroscuentasResult;

    /**
     * Obtiene el valor de la propiedad wstransferiraotroscuentasResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseTransferenciaDTO }
     *     
     */
    public ResponseTransferenciaDTO getWSTRANSFERIRAOTROSCUENTASResult() {
        return wstransferiraotroscuentasResult;
    }

    /**
     * Define el valor de la propiedad wstransferiraotroscuentasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseTransferenciaDTO }
     *     
     */
    public void setWSTRANSFERIRAOTROSCUENTASResult(ResponseTransferenciaDTO value) {
        this.wstransferiraotroscuentasResult = value;
    }

}
