
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_VALIDAR_CUENTA_SESIONResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsvalidarcuentasesionResult"
})
@XmlRootElement(name = "WS_VALIDAR_CUENTA_SESIONResponse")
public class WSVALIDARCUENTASESIONResponse {

    @XmlElement(name = "WS_VALIDAR_CUENTA_SESIONResult")
    protected boolean wsvalidarcuentasesionResult;

    /**
     * Obtiene el valor de la propiedad wsvalidarcuentasesionResult.
     * 
     */
    public boolean isWSVALIDARCUENTASESIONResult() {
        return wsvalidarcuentasesionResult;
    }

    /**
     * Define el valor de la propiedad wsvalidarcuentasesionResult.
     * 
     */
    public void setWSVALIDARCUENTASESIONResult(boolean value) {
        this.wsvalidarcuentasesionResult = value;
    }

}
