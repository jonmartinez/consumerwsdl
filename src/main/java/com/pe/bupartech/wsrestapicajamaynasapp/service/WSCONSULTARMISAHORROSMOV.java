
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_tipo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_desde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_hasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcCuenta",
    "xnTipo",
    "xcDesde",
    "xcHasta"
})
@XmlRootElement(name = "WS_CONSULTAR_MISAHORROS_MOV")
public class WSCONSULTARMISAHORROSMOV {

    @XmlElement(name = "xc_cuenta")
    protected String xcCuenta;
    @XmlElement(name = "xn_tipo")
    protected int xnTipo;
    @XmlElement(name = "xc_desde")
    protected String xcDesde;
    @XmlElement(name = "xc_hasta")
    protected String xcHasta;

    /**
     * Obtiene el valor de la propiedad xcCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuenta() {
        return xcCuenta;
    }

    /**
     * Define el valor de la propiedad xcCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuenta(String value) {
        this.xcCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipo.
     * 
     */
    public int getXnTipo() {
        return xnTipo;
    }

    /**
     * Define el valor de la propiedad xnTipo.
     * 
     */
    public void setXnTipo(int value) {
        this.xnTipo = value;
    }

    /**
     * Obtiene el valor de la propiedad xcDesde.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcDesde() {
        return xcDesde;
    }

    /**
     * Define el valor de la propiedad xcDesde.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcDesde(String value) {
        this.xcDesde = value;
    }

    /**
     * Obtiene el valor de la propiedad xcHasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcHasta() {
        return xcHasta;
    }

    /**
     * Define el valor de la propiedad xcHasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcHasta(String value) {
        this.xcHasta = value;
    }

}
