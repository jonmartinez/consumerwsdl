
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponsePlanillaDepositoDet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponsePlanillaDepositoDet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cTipoDoi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cDoi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cPersCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cPersNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cCtaCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cMonto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cITF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cMoneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cCuatroSueldos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsePlanillaDepositoDet", propOrder = {
    "cTipoDoi",
    "cDoi",
    "cPersCod",
    "cPersNombre",
    "cCtaCod",
    "cMonto",
    "citf",
    "cMoneda",
    "cCuatroSueldos",
    "cEstado"
})
public class ResponsePlanillaDepositoDet {

    protected String cTipoDoi;
    protected String cDoi;
    protected String cPersCod;
    protected String cPersNombre;
    protected String cCtaCod;
    protected String cMonto;
    @XmlElement(name = "cITF")
    protected String citf;
    protected String cMoneda;
    protected String cCuatroSueldos;
    protected String cEstado;

    /**
     * Obtiene el valor de la propiedad cTipoDoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTipoDoi() {
        return cTipoDoi;
    }

    /**
     * Define el valor de la propiedad cTipoDoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTipoDoi(String value) {
        this.cTipoDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad cDoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDoi() {
        return cDoi;
    }

    /**
     * Define el valor de la propiedad cDoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDoi(String value) {
        this.cDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad cPersCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPersCod() {
        return cPersCod;
    }

    /**
     * Define el valor de la propiedad cPersCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPersCod(String value) {
        this.cPersCod = value;
    }

    /**
     * Obtiene el valor de la propiedad cPersNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPersNombre() {
        return cPersNombre;
    }

    /**
     * Define el valor de la propiedad cPersNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPersNombre(String value) {
        this.cPersNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad cCtaCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCtaCod() {
        return cCtaCod;
    }

    /**
     * Define el valor de la propiedad cCtaCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCtaCod(String value) {
        this.cCtaCod = value;
    }

    /**
     * Obtiene el valor de la propiedad cMonto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMonto() {
        return cMonto;
    }

    /**
     * Define el valor de la propiedad cMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMonto(String value) {
        this.cMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad citf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCITF() {
        return citf;
    }

    /**
     * Define el valor de la propiedad citf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCITF(String value) {
        this.citf = value;
    }

    /**
     * Obtiene el valor de la propiedad cMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMoneda() {
        return cMoneda;
    }

    /**
     * Define el valor de la propiedad cMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMoneda(String value) {
        this.cMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad cCuatroSueldos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCuatroSueldos() {
        return cCuatroSueldos;
    }

    /**
     * Define el valor de la propiedad cCuatroSueldos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCuatroSueldos(String value) {
        this.cCuatroSueldos = value;
    }

    /**
     * Obtiene el valor de la propiedad cEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCEstado() {
        return cEstado;
    }

    /**
     * Define el valor de la propiedad cEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCEstado(String value) {
        this.cEstado = value;
    }

}
