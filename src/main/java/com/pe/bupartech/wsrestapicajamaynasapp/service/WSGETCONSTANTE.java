
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xn_conscod" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xn_consvalor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xnConscod",
    "xnConsvalor"
})
@XmlRootElement(name = "WS_GET_CONSTANTE")
public class WSGETCONSTANTE {

    @XmlElement(name = "xn_conscod")
    protected int xnConscod;
    @XmlElement(name = "xn_consvalor")
    protected String xnConsvalor;

    /**
     * Obtiene el valor de la propiedad xnConscod.
     * 
     */
    public int getXnConscod() {
        return xnConscod;
    }

    /**
     * Define el valor de la propiedad xnConscod.
     * 
     */
    public void setXnConscod(int value) {
        this.xnConscod = value;
    }

    /**
     * Obtiene el valor de la propiedad xnConsvalor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXnConsvalor() {
        return xnConsvalor;
    }

    /**
     * Define el valor de la propiedad xnConsvalor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXnConsvalor(String value) {
        this.xnConsvalor = value;
    }

}
