
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_DETALLE_RENOVAR_PIGNORATICIOResult" type="{http://cajamaynas.pe/}ResponsePignoOpeDetalleDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetdetallerenovarpignoraticioResult"
})
@XmlRootElement(name = "WS_GET_DETALLE_RENOVAR_PIGNORATICIOResponse")
public class WSGETDETALLERENOVARPIGNORATICIOResponse {

    @XmlElement(name = "WS_GET_DETALLE_RENOVAR_PIGNORATICIOResult")
    protected ResponsePignoOpeDetalleDTO wsgetdetallerenovarpignoraticioResult;

    /**
     * Obtiene el valor de la propiedad wsgetdetallerenovarpignoraticioResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePignoOpeDetalleDTO }
     *     
     */
    public ResponsePignoOpeDetalleDTO getWSGETDETALLERENOVARPIGNORATICIOResult() {
        return wsgetdetallerenovarpignoraticioResult;
    }

    /**
     * Define el valor de la propiedad wsgetdetallerenovarpignoraticioResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePignoOpeDetalleDTO }
     *     
     */
    public void setWSGETDETALLERENOVARPIGNORATICIOResult(ResponsePignoOpeDetalleDTO value) {
        this.wsgetdetallerenovarpignoraticioResult = value;
    }

}
