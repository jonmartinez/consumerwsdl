
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_PAGO_PLANILLA_CTS_PAGOResult" type="{http://cajamaynas.pe/}ResponsePagoPlanillaDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wspagoplanillactspagoResult"
})
@XmlRootElement(name = "WS_PAGO_PLANILLA_CTS_PAGOResponse")
public class WSPAGOPLANILLACTSPAGOResponse {

    @XmlElement(name = "WS_PAGO_PLANILLA_CTS_PAGOResult")
    protected ResponsePagoPlanillaDTO wspagoplanillactspagoResult;

    /**
     * Obtiene el valor de la propiedad wspagoplanillactspagoResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagoPlanillaDTO }
     *     
     */
    public ResponsePagoPlanillaDTO getWSPAGOPLANILLACTSPAGOResult() {
        return wspagoplanillactspagoResult;
    }

    /**
     * Define el valor de la propiedad wspagoplanillactspagoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagoPlanillaDTO }
     *     
     */
    public void setWSPAGOPLANILLACTSPAGOResult(ResponsePagoPlanillaDTO value) {
        this.wspagoplanillactspagoResult = value;
    }

}
