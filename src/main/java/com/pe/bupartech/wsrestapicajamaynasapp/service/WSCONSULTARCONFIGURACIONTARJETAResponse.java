
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_CONFIGURACION_TARJETAResult" type="{http://cajamaynas.pe/}ResponseConsultarConfiguracionTarjetaDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultarconfiguraciontarjetaResult"
})
@XmlRootElement(name = "WS_CONSULTAR_CONFIGURACION_TARJETAResponse")
public class WSCONSULTARCONFIGURACIONTARJETAResponse {

    @XmlElement(name = "WS_CONSULTAR_CONFIGURACION_TARJETAResult")
    protected ResponseConsultarConfiguracionTarjetaDTO wsconsultarconfiguraciontarjetaResult;

    /**
     * Obtiene el valor de la propiedad wsconsultarconfiguraciontarjetaResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseConsultarConfiguracionTarjetaDTO }
     *     
     */
    public ResponseConsultarConfiguracionTarjetaDTO getWSCONSULTARCONFIGURACIONTARJETAResult() {
        return wsconsultarconfiguraciontarjetaResult;
    }

    /**
     * Define el valor de la propiedad wsconsultarconfiguraciontarjetaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseConsultarConfiguracionTarjetaDTO }
     *     
     */
    public void setWSCONSULTARCONFIGURACIONTARJETAResult(ResponseConsultarConfiguracionTarjetaDTO value) {
        this.wsconsultarconfiguraciontarjetaResult = value;
    }

}
