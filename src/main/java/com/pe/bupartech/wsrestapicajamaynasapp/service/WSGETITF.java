
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_opecod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_monto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xn_tipo_ope" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_mismo_titular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcOpecod",
    "xnMonto",
    "xnTipoOpe",
    "xcMismoTitular"
})
@XmlRootElement(name = "WS_GET_ITF")
public class WSGETITF {

    @XmlElement(name = "xc_opecod")
    protected String xcOpecod;
    @XmlElement(name = "xn_monto", required = true)
    protected BigDecimal xnMonto;
    @XmlElement(name = "xn_tipo_ope")
    protected int xnTipoOpe;
    @XmlElement(name = "xc_mismo_titular")
    protected String xcMismoTitular;

    /**
     * Obtiene el valor de la propiedad xcOpecod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcOpecod() {
        return xcOpecod;
    }

    /**
     * Define el valor de la propiedad xcOpecod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcOpecod(String value) {
        this.xcOpecod = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMonto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnMonto() {
        return xnMonto;
    }

    /**
     * Define el valor de la propiedad xnMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnMonto(BigDecimal value) {
        this.xnMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipoOpe.
     * 
     */
    public int getXnTipoOpe() {
        return xnTipoOpe;
    }

    /**
     * Define el valor de la propiedad xnTipoOpe.
     * 
     */
    public void setXnTipoOpe(int value) {
        this.xnTipoOpe = value;
    }

    /**
     * Obtiene el valor de la propiedad xcMismoTitular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcMismoTitular() {
        return xcMismoTitular;
    }

    /**
     * Define el valor de la propiedad xcMismoTitular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcMismoTitular(String value) {
        this.xcMismoTitular = value;
    }

}
