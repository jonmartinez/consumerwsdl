
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_CONSULTAR_MISAHORROS_MOVPENDIENTESResult" type="{http://cajamaynas.pe/}ResponseMisAhorros" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsconsultarmisahorrosmovpendientesResult"
})
@XmlRootElement(name = "WS_CONSULTAR_MISAHORROS_MOVPENDIENTESResponse")
public class WSCONSULTARMISAHORROSMOVPENDIENTESResponse {

    @XmlElement(name = "WS_CONSULTAR_MISAHORROS_MOVPENDIENTESResult")
    protected ResponseMisAhorros wsconsultarmisahorrosmovpendientesResult;

    /**
     * Obtiene el valor de la propiedad wsconsultarmisahorrosmovpendientesResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMisAhorros }
     *     
     */
    public ResponseMisAhorros getWSCONSULTARMISAHORROSMOVPENDIENTESResult() {
        return wsconsultarmisahorrosmovpendientesResult;
    }

    /**
     * Define el valor de la propiedad wsconsultarmisahorrosmovpendientesResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMisAhorros }
     *     
     */
    public void setWSCONSULTARMISAHORROSMOVPENDIENTESResult(ResponseMisAhorros value) {
        this.wsconsultarmisahorrosmovpendientesResult = value;
    }

}
