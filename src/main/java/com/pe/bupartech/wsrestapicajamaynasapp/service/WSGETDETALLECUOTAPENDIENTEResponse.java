
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_DETALLE_CUOTA_PENDIENTEResult" type="{http://cajamaynas.pe/}ResponseDetalleCuotaPendienteDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetdetallecuotapendienteResult"
})
@XmlRootElement(name = "WS_GET_DETALLE_CUOTA_PENDIENTEResponse")
public class WSGETDETALLECUOTAPENDIENTEResponse {

    @XmlElement(name = "WS_GET_DETALLE_CUOTA_PENDIENTEResult")
    protected ResponseDetalleCuotaPendienteDTO wsgetdetallecuotapendienteResult;

    /**
     * Obtiene el valor de la propiedad wsgetdetallecuotapendienteResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseDetalleCuotaPendienteDTO }
     *     
     */
    public ResponseDetalleCuotaPendienteDTO getWSGETDETALLECUOTAPENDIENTEResult() {
        return wsgetdetallecuotapendienteResult;
    }

    /**
     * Define el valor de la propiedad wsgetdetallecuotapendienteResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseDetalleCuotaPendienteDTO }
     *     
     */
    public void setWSGETDETALLECUOTAPENDIENTEResult(ResponseDetalleCuotaPendienteDTO value) {
        this.wsgetdetallecuotapendienteResult = value;
    }

}
