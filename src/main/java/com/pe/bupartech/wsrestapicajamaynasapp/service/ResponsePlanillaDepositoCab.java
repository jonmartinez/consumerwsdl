
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponsePlanillaDepositoCab complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponsePlanillaDepositoCab">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nAnio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cMontoPlanilla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cITF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cComision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nNumTrabajadores" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cPersCodTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstDetalle" type="{http://cajamaynas.pe/}ArrayOfResponsePlanillaDepositoDet" minOccurs="0"/>
 *         &lt;element name="bProveedores" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsePlanillaDepositoCab", propOrder = {
    "nAnio",
    "nMes",
    "cMontoPlanilla",
    "citf",
    "cComision",
    "nNumTrabajadores",
    "cDescripcion",
    "cPersCodTitular",
    "cCuenta",
    "lstDetalle",
    "bProveedores"
})
public class ResponsePlanillaDepositoCab {

    protected int nAnio;
    protected int nMes;
    protected String cMontoPlanilla;
    @XmlElement(name = "cITF")
    protected String citf;
    protected String cComision;
    protected int nNumTrabajadores;
    protected String cDescripcion;
    protected String cPersCodTitular;
    protected String cCuenta;
    protected ArrayOfResponsePlanillaDepositoDet lstDetalle;
    protected boolean bProveedores;

    /**
     * Obtiene el valor de la propiedad nAnio.
     * 
     */
    public int getNAnio() {
        return nAnio;
    }

    /**
     * Define el valor de la propiedad nAnio.
     * 
     */
    public void setNAnio(int value) {
        this.nAnio = value;
    }

    /**
     * Obtiene el valor de la propiedad nMes.
     * 
     */
    public int getNMes() {
        return nMes;
    }

    /**
     * Define el valor de la propiedad nMes.
     * 
     */
    public void setNMes(int value) {
        this.nMes = value;
    }

    /**
     * Obtiene el valor de la propiedad cMontoPlanilla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMontoPlanilla() {
        return cMontoPlanilla;
    }

    /**
     * Define el valor de la propiedad cMontoPlanilla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMontoPlanilla(String value) {
        this.cMontoPlanilla = value;
    }

    /**
     * Obtiene el valor de la propiedad citf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCITF() {
        return citf;
    }

    /**
     * Define el valor de la propiedad citf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCITF(String value) {
        this.citf = value;
    }

    /**
     * Obtiene el valor de la propiedad cComision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCComision() {
        return cComision;
    }

    /**
     * Define el valor de la propiedad cComision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCComision(String value) {
        this.cComision = value;
    }

    /**
     * Obtiene el valor de la propiedad nNumTrabajadores.
     * 
     */
    public int getNNumTrabajadores() {
        return nNumTrabajadores;
    }

    /**
     * Define el valor de la propiedad nNumTrabajadores.
     * 
     */
    public void setNNumTrabajadores(int value) {
        this.nNumTrabajadores = value;
    }

    /**
     * Obtiene el valor de la propiedad cDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDescripcion() {
        return cDescripcion;
    }

    /**
     * Define el valor de la propiedad cDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDescripcion(String value) {
        this.cDescripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad cPersCodTitular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPersCodTitular() {
        return cPersCodTitular;
    }

    /**
     * Define el valor de la propiedad cPersCodTitular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPersCodTitular(String value) {
        this.cPersCodTitular = value;
    }

    /**
     * Obtiene el valor de la propiedad cCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCuenta() {
        return cCuenta;
    }

    /**
     * Define el valor de la propiedad cCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCuenta(String value) {
        this.cCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad lstDetalle.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResponsePlanillaDepositoDet }
     *     
     */
    public ArrayOfResponsePlanillaDepositoDet getLstDetalle() {
        return lstDetalle;
    }

    /**
     * Define el valor de la propiedad lstDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResponsePlanillaDepositoDet }
     *     
     */
    public void setLstDetalle(ArrayOfResponsePlanillaDepositoDet value) {
        this.lstDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad bProveedores.
     * 
     */
    public boolean isBProveedores() {
        return bProveedores;
    }

    /**
     * Define el valor de la propiedad bProveedores.
     * 
     */
    public void setBProveedores(boolean value) {
        this.bProveedores = value;
    }

}
