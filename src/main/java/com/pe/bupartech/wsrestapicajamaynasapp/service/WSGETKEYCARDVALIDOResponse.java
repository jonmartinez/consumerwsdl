
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_GET_KEYCARD_VALIDOResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsgetkeycardvalidoResult"
})
@XmlRootElement(name = "WS_GET_KEYCARD_VALIDOResponse")
public class WSGETKEYCARDVALIDOResponse {

    @XmlElement(name = "WS_GET_KEYCARD_VALIDOResult")
    protected boolean wsgetkeycardvalidoResult;

    /**
     * Obtiene el valor de la propiedad wsgetkeycardvalidoResult.
     * 
     */
    public boolean isWSGETKEYCARDVALIDOResult() {
        return wsgetkeycardvalidoResult;
    }

    /**
     * Define el valor de la propiedad wsgetkeycardvalidoResult.
     * 
     */
    public void setWSGETKEYCARDVALIDOResult(boolean value) {
        this.wsgetkeycardvalidoResult = value;
    }

}
