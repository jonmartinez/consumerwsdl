
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_tipo_doi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_doi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_tarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcTipoDoi",
    "xcDoi",
    "xcTarjeta"
})
@XmlRootElement(name = "WS_CONSULTAR_PERSONA")
public class WSCONSULTARPERSONA {

    @XmlElement(name = "xc_tipo_doi")
    protected String xcTipoDoi;
    @XmlElement(name = "xc_doi")
    protected String xcDoi;
    @XmlElement(name = "xc_tarjeta")
    protected String xcTarjeta;

    /**
     * Obtiene el valor de la propiedad xcTipoDoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcTipoDoi() {
        return xcTipoDoi;
    }

    /**
     * Define el valor de la propiedad xcTipoDoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcTipoDoi(String value) {
        this.xcTipoDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad xcDoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcDoi() {
        return xcDoi;
    }

    /**
     * Define el valor de la propiedad xcDoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcDoi(String value) {
        this.xcDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad xcTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcTarjeta() {
        return xcTarjeta;
    }

    /**
     * Define el valor de la propiedad xcTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcTarjeta(String value) {
        this.xcTarjeta = value;
    }

}
