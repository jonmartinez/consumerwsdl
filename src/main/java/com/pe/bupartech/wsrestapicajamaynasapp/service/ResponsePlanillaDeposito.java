
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponsePlanillaDeposito complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponsePlanillaDeposito">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bVerificacion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lstObservaciones" type="{http://cajamaynas.pe/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="planilla" type="{http://cajamaynas.pe/}ResponsePlanillaDepositoCab" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsePlanillaDeposito", propOrder = {
    "bVerificacion",
    "lstObservaciones",
    "planilla"
})
public class ResponsePlanillaDeposito {

    protected boolean bVerificacion;
    protected ArrayOfString lstObservaciones;
    protected ResponsePlanillaDepositoCab planilla;

    /**
     * Obtiene el valor de la propiedad bVerificacion.
     * 
     */
    public boolean isBVerificacion() {
        return bVerificacion;
    }

    /**
     * Define el valor de la propiedad bVerificacion.
     * 
     */
    public void setBVerificacion(boolean value) {
        this.bVerificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad lstObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getLstObservaciones() {
        return lstObservaciones;
    }

    /**
     * Define el valor de la propiedad lstObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setLstObservaciones(ArrayOfString value) {
        this.lstObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad planilla.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePlanillaDepositoCab }
     *     
     */
    public ResponsePlanillaDepositoCab getPlanilla() {
        return planilla;
    }

    /**
     * Define el valor de la propiedad planilla.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePlanillaDepositoCab }
     *     
     */
    public void setPlanilla(ResponsePlanillaDepositoCab value) {
        this.planilla = value;
    }

}
