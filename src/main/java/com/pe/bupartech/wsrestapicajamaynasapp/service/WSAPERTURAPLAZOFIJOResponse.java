
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_APERTURA_PLAZO_FIJOResult" type="{http://cajamaynas.pe/}ResponseAperturaPlazoFijoDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsaperturaplazofijoResult"
})
@XmlRootElement(name = "WS_APERTURA_PLAZO_FIJOResponse")
public class WSAPERTURAPLAZOFIJOResponse {

    @XmlElement(name = "WS_APERTURA_PLAZO_FIJOResult")
    protected ResponseAperturaPlazoFijoDTO wsaperturaplazofijoResult;

    /**
     * Obtiene el valor de la propiedad wsaperturaplazofijoResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAperturaPlazoFijoDTO }
     *     
     */
    public ResponseAperturaPlazoFijoDTO getWSAPERTURAPLAZOFIJOResult() {
        return wsaperturaplazofijoResult;
    }

    /**
     * Define el valor de la propiedad wsaperturaplazofijoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAperturaPlazoFijoDTO }
     *     
     */
    public void setWSAPERTURAPLAZOFIJOResult(ResponseAperturaPlazoFijoDTO value) {
        this.wsaperturaplazofijoResult = value;
    }

}
