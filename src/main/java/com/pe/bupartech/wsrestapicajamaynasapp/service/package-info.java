/**
 * Caja Maynas pone a disponición los siguientes servicios.
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://cajamaynas.pe/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.pe.bupartech.wsrestapicajamaynasapp.service;
