
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseMisCuentasDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseMisCuentasDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_cap" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_subpro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cuota_vig" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="saldo_disp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="int_ganado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo_actual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseMisCuentasDTO", propOrder = {
    "grupo",
    "cuenta",
    "saldoCap",
    "tipoProducto",
    "tipoSubpro",
    "cuotaVig",
    "saldoDisp",
    "moneda",
    "cci",
    "intGanado",
    "estado",
    "telefono",
    "saldoActual"
})
public class ResponseMisCuentasDTO {

    protected String grupo;
    protected String cuenta;
    @XmlElement(name = "saldo_cap")
    protected String saldoCap;
    @XmlElement(name = "tipo_producto")
    protected String tipoProducto;
    @XmlElement(name = "tipo_subpro")
    protected String tipoSubpro;
    @XmlElement(name = "cuota_vig")
    protected int cuotaVig;
    @XmlElement(name = "saldo_disp")
    protected String saldoDisp;
    protected String moneda;
    protected String cci;
    @XmlElement(name = "int_ganado")
    protected String intGanado;
    protected String estado;
    protected String telefono;
    @XmlElement(name = "saldo_actual")
    protected String saldoActual;

    /**
     * Obtiene el valor de la propiedad grupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Define el valor de la propiedad grupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupo(String value) {
        this.grupo = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoCap.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoCap() {
        return saldoCap;
    }

    /**
     * Define el valor de la propiedad saldoCap.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoCap(String value) {
        this.saldoCap = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProducto() {
        return tipoProducto;
    }

    /**
     * Define el valor de la propiedad tipoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProducto(String value) {
        this.tipoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoSubpro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSubpro() {
        return tipoSubpro;
    }

    /**
     * Define el valor de la propiedad tipoSubpro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSubpro(String value) {
        this.tipoSubpro = value;
    }

    /**
     * Obtiene el valor de la propiedad cuotaVig.
     * 
     */
    public int getCuotaVig() {
        return cuotaVig;
    }

    /**
     * Define el valor de la propiedad cuotaVig.
     * 
     */
    public void setCuotaVig(int value) {
        this.cuotaVig = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoDisp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoDisp() {
        return saldoDisp;
    }

    /**
     * Define el valor de la propiedad saldoDisp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoDisp(String value) {
        this.saldoDisp = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad cci.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCci() {
        return cci;
    }

    /**
     * Define el valor de la propiedad cci.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCci(String value) {
        this.cci = value;
    }

    /**
     * Obtiene el valor de la propiedad intGanado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntGanado() {
        return intGanado;
    }

    /**
     * Define el valor de la propiedad intGanado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntGanado(String value) {
        this.intGanado = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Define el valor de la propiedad telefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoActual() {
        return saldoActual;
    }

    /**
     * Define el valor de la propiedad saldoActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoActual(String value) {
        this.saldoActual = value;
    }

}
