
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_PAGO_PLANILLA_VERIFICACIONResult" type="{http://cajamaynas.pe/}ResponsePlanillaDeposito" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wspagoplanillaverificacionResult"
})
@XmlRootElement(name = "WS_PAGO_PLANILLA_VERIFICACIONResponse")
public class WSPAGOPLANILLAVERIFICACIONResponse {

    @XmlElement(name = "WS_PAGO_PLANILLA_VERIFICACIONResult")
    protected ResponsePlanillaDeposito wspagoplanillaverificacionResult;

    /**
     * Obtiene el valor de la propiedad wspagoplanillaverificacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePlanillaDeposito }
     *     
     */
    public ResponsePlanillaDeposito getWSPAGOPLANILLAVERIFICACIONResult() {
        return wspagoplanillaverificacionResult;
    }

    /**
     * Define el valor de la propiedad wspagoplanillaverificacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePlanillaDeposito }
     *     
     */
    public void setWSPAGOPLANILLAVERIFICACIONResult(ResponsePlanillaDeposito value) {
        this.wspagoplanillaverificacionResult = value;
    }

}
