
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xc_identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_tipo_doi_orig" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_nrodoi_orig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_cuenta_orig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_tipo_doi_dest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="xc_nrodoi_dest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_apell_pater" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_apell_mater" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xc_nombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xn_monto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="xc_cod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xcIdentificacion",
    "xnTipoDoiOrig",
    "xcNrodoiOrig",
    "xcCuentaOrig",
    "xnTipoDoiDest",
    "xcNrodoiDest",
    "xcApellPater",
    "xcApellMater",
    "xcNombres",
    "xnMonto",
    "xcCod"
})
@XmlRootElement(name = "WS_APERTURAGIRO")
public class WSAPERTURAGIRO {

    @XmlElement(name = "xc_identificacion")
    protected String xcIdentificacion;
    @XmlElement(name = "xn_tipo_doi_orig")
    protected int xnTipoDoiOrig;
    @XmlElement(name = "xc_nrodoi_orig")
    protected String xcNrodoiOrig;
    @XmlElement(name = "xc_cuenta_orig")
    protected String xcCuentaOrig;
    @XmlElement(name = "xn_tipo_doi_dest")
    protected int xnTipoDoiDest;
    @XmlElement(name = "xc_nrodoi_dest")
    protected String xcNrodoiDest;
    @XmlElement(name = "xc_apell_pater")
    protected String xcApellPater;
    @XmlElement(name = "xc_apell_mater")
    protected String xcApellMater;
    @XmlElement(name = "xc_nombres")
    protected String xcNombres;
    @XmlElement(name = "xn_monto", required = true)
    protected BigDecimal xnMonto;
    @XmlElement(name = "xc_cod")
    protected String xcCod;

    /**
     * Obtiene el valor de la propiedad xcIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcIdentificacion() {
        return xcIdentificacion;
    }

    /**
     * Define el valor de la propiedad xcIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcIdentificacion(String value) {
        this.xcIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipoDoiOrig.
     * 
     */
    public int getXnTipoDoiOrig() {
        return xnTipoDoiOrig;
    }

    /**
     * Define el valor de la propiedad xnTipoDoiOrig.
     * 
     */
    public void setXnTipoDoiOrig(int value) {
        this.xnTipoDoiOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNrodoiOrig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNrodoiOrig() {
        return xcNrodoiOrig;
    }

    /**
     * Define el valor de la propiedad xcNrodoiOrig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNrodoiOrig(String value) {
        this.xcNrodoiOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCuentaOrig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCuentaOrig() {
        return xcCuentaOrig;
    }

    /**
     * Define el valor de la propiedad xcCuentaOrig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCuentaOrig(String value) {
        this.xcCuentaOrig = value;
    }

    /**
     * Obtiene el valor de la propiedad xnTipoDoiDest.
     * 
     */
    public int getXnTipoDoiDest() {
        return xnTipoDoiDest;
    }

    /**
     * Define el valor de la propiedad xnTipoDoiDest.
     * 
     */
    public void setXnTipoDoiDest(int value) {
        this.xnTipoDoiDest = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNrodoiDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNrodoiDest() {
        return xcNrodoiDest;
    }

    /**
     * Define el valor de la propiedad xcNrodoiDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNrodoiDest(String value) {
        this.xcNrodoiDest = value;
    }

    /**
     * Obtiene el valor de la propiedad xcApellPater.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcApellPater() {
        return xcApellPater;
    }

    /**
     * Define el valor de la propiedad xcApellPater.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcApellPater(String value) {
        this.xcApellPater = value;
    }

    /**
     * Obtiene el valor de la propiedad xcApellMater.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcApellMater() {
        return xcApellMater;
    }

    /**
     * Define el valor de la propiedad xcApellMater.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcApellMater(String value) {
        this.xcApellMater = value;
    }

    /**
     * Obtiene el valor de la propiedad xcNombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcNombres() {
        return xcNombres;
    }

    /**
     * Define el valor de la propiedad xcNombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcNombres(String value) {
        this.xcNombres = value;
    }

    /**
     * Obtiene el valor de la propiedad xnMonto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXnMonto() {
        return xnMonto;
    }

    /**
     * Define el valor de la propiedad xnMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXnMonto(BigDecimal value) {
        this.xnMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad xcCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXcCod() {
        return xcCod;
    }

    /**
     * Define el valor de la propiedad xcCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXcCod(String value) {
        this.xcCod = value;
    }

}
