
package com.pe.bupartech.wsrestapicajamaynasapp.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestPlanillaDepositoDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestPlanillaDepositoDetalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nTipoDoi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cDoi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cCtaCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Monto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="cMoneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nMoneda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nCuatroSueldos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="cEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestPlanillaDepositoDetalle", propOrder = {
    "nTipoDoi",
    "cDoi",
    "cCtaCod",
    "monto",
    "cMoneda",
    "nMoneda",
    "nCuatroSueldos",
    "cEstado"
})
public class RequestPlanillaDepositoDetalle {

    protected int nTipoDoi;
    protected String cDoi;
    protected String cCtaCod;
    @XmlElement(name = "Monto", required = true)
    protected BigDecimal monto;
    protected String cMoneda;
    protected int nMoneda;
    @XmlElement(required = true)
    protected BigDecimal nCuatroSueldos;
    protected String cEstado;

    /**
     * Obtiene el valor de la propiedad nTipoDoi.
     * 
     */
    public int getNTipoDoi() {
        return nTipoDoi;
    }

    /**
     * Define el valor de la propiedad nTipoDoi.
     * 
     */
    public void setNTipoDoi(int value) {
        this.nTipoDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad cDoi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDoi() {
        return cDoi;
    }

    /**
     * Define el valor de la propiedad cDoi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDoi(String value) {
        this.cDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad cCtaCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCtaCod() {
        return cCtaCod;
    }

    /**
     * Define el valor de la propiedad cCtaCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCtaCod(String value) {
        this.cCtaCod = value;
    }

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMonto(BigDecimal value) {
        this.monto = value;
    }

    /**
     * Obtiene el valor de la propiedad cMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMoneda() {
        return cMoneda;
    }

    /**
     * Define el valor de la propiedad cMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMoneda(String value) {
        this.cMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad nMoneda.
     * 
     */
    public int getNMoneda() {
        return nMoneda;
    }

    /**
     * Define el valor de la propiedad nMoneda.
     * 
     */
    public void setNMoneda(int value) {
        this.nMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad nCuatroSueldos.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNCuatroSueldos() {
        return nCuatroSueldos;
    }

    /**
     * Define el valor de la propiedad nCuatroSueldos.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNCuatroSueldos(BigDecimal value) {
        this.nCuatroSueldos = value;
    }

    /**
     * Obtiene el valor de la propiedad cEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCEstado() {
        return cEstado;
    }

    /**
     * Define el valor de la propiedad cEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCEstado(String value) {
        this.cEstado = value;
    }

}
